package escal.hpc;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import escal.hpc.panels.ModeSelectPanel;
import escal.hpc.utilities.Miscellaneous;

public class FrontendMain {
	
	static Miscellaneous misc = new Miscellaneous();

	public static void main(String[] args) {
		
		setUserExperience();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				introShow();
			}
		});
	}
	
	private static void introShow() {
		
		final JFrame introFrame = new JFrame("HPCVisualizer - Select Mode");
		introFrame.setPreferredSize(new Dimension(600, 250));
		introFrame.setSize(new Dimension(600, 250));
				
		ModeSelectPanel msp = new ModeSelectPanel(introFrame);
		
		introFrame.setResizable(false);
		misc.alignWindow(introFrame);
		introFrame.getContentPane().add(msp);
		introFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		introFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				System.exit(0);
			}
		});
		introFrame.pack();
		introFrame.setVisible(true);
		
		System.out.println("\n\nHPC-Visualizer is running...\n");
	}

	private static void setUserExperience() {
		
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// if Nimbus Look and Feel is not installed, basic Look and Feel is applied
		}
	}
}
