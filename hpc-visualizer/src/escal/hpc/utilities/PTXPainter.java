package escal.hpc.utilities;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

public class PTXPainter
implements Highlighter.HighlightPainter, CaretListener, MouseListener, MouseMotionListener {
	
	private JTextComponent codeComponent;
	private JTextComponent ptxComponent;
	
	private int[][] ptxCodeArray;
	
	private Color color;
	
	private Rectangle codeLastView;
	private Rectangle ptxLastView;
	
	public PTXPainter(JTextComponent codeComponent, JTextComponent ptxComponent, int[][] ptxCodeArray)
	{
		this(codeComponent, ptxComponent, ptxCodeArray, null);
				
		setLighter(codeComponent.getSelectionColor());
		setLighter(ptxComponent.getSelectionColor());
	}
	
	public PTXPainter(JTextComponent codeComponent, JTextComponent ptxComponent, int[][] ptxCodeArray, Color color)
	{
		this.codeComponent = codeComponent;
		this.ptxComponent = ptxComponent;
		this.ptxCodeArray = ptxCodeArray;
				
		setColor( color );

		//  Add listeners so we know when to change highlighting

		codeComponent.addCaretListener( this );
		codeComponent.addMouseListener( this );
		codeComponent.addMouseMotionListener( this );
		
		ptxComponent.addCaretListener( this );
		ptxComponent.addMouseListener( this );
		ptxComponent.addMouseMotionListener( this );

		//  Turn highlighting on by adding a dummy highlight

		try
		{
			codeComponent.getHighlighter().addHighlight(0, 0, this);
			ptxComponent.getHighlighter().addHighlight(0, 0, this);
		}
		catch(BadLocationException ble) {}
	}
	
	public void setLighter(Color color)
	{
		int red   = Math.min(255, (int)(color.getRed() * 1.2));
		int green = Math.min(255, (int)(color.getGreen() * 1.2));
		int blue  = Math.min(255, (int)(color.getBlue() * 1.2));
		setColor(new Color(red, green, blue));
	}
	
	public void setColor(Color color)
	{
		this.color = color;
	}


	public void paint(Graphics g, int p0, int p1, Shape bounds, JTextComponent c)
	{
		try
		{
			Rectangle r = c.modelToView(c.getCaretPosition());
			g.setColor( color );
			g.fillRect(0, r.y, c.getWidth(), r.height);
			
		}
		catch(BadLocationException ble) {System.out.println(ble);}
	}
		
	private void resetHighlight()
	{
		//  Use invokeLater to make sure updates to the Document are completed,
		//  otherwise Undo processing causes the modelToView method to loop.

		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					int offset = codeComponent.getCaretPosition();
					Rectangle currentView = codeComponent.modelToView(offset);
					
					if (codeLastView == null) {
						codeLastView = currentView;
						int codeCurrentLine = (currentView.y - codeComponent.modelToView(0).y) / currentView.height;
						
						Graphics ptxG = ptxComponent.getGraphics();
						ptxG.setColor(color.green);
						
						for(int ptxLine = 1; ptxLine < ptxCodeArray[0].length - 1; ptxLine++) {
							if(ptxCodeArray[0][ptxLine] == codeCurrentLine) {
								ptxG.fillRect(0, currentView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y, ptxComponent.getWidth(), currentView.height);								
							}
						}
					}
					//  Remove the highlighting from the previously highlighted line
					
					if (codeLastView.y != currentView.y)
					{
						int codeCurrentLine = (currentView.y - codeComponent.modelToView(0).y) / currentView.height;
						int codeLastLine = (codeLastView.y - codeComponent.modelToView(0).y) / codeLastView.height;
						
						Graphics ptxG = ptxComponent.getGraphics();
						ptxG.setColor(color.green);
						
						codeComponent.repaint(0, codeLastView.y, codeComponent.getWidth(), codeLastView.height);
												
						for(int ptxLine = 1; ptxLine < ptxCodeArray[0].length - 1; ++ptxLine) {
							if(ptxCodeArray[0][ptxLine] == codeLastLine) {
								ptxComponent.repaint(0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y, ptxComponent.getWidth(), codeLastView.height);
								//ptxG.clearRect(0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y, ptxComponent.getWidth(), codeLastView.height);
							}
						}
						
						for(int ptxLine = 1; ptxLine < ptxCodeArray[0].length - 1; ptxLine++) {
							if(ptxCodeArray[0][ptxLine] == codeCurrentLine) {
								//ptxComponent.paintImmediately(0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y, ptxComponent.getWidth(), codeLastView.height);
								ptxG.fillRect(0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y, ptxComponent.getWidth(), codeLastView.height);
								//ptxG.drawString(ptxComponent.getComponent(ptxLine), 0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y);
								//ptxG.drawString(ptxComponent.getComponent(ptxLine).toString(), 0, codeLastView.height * (ptxLine - 1) + ptxComponent.modelToView(0).y);
							}
						}
						codeLastView = currentView;
					}
					
					int ptxOffset = ptxComponent.getCaretPosition();
					Rectangle currentPTXView = ptxComponent.modelToView(ptxOffset);
					
					if (ptxLastView == null) {
						ptxLastView = currentPTXView;
						int ptxCurrentLine = (currentPTXView.y - ptxComponent.modelToView(0).y) / currentPTXView.height;
						int codeLine = ptxCodeArray[0][ptxCurrentLine+1];
						
						Graphics codeG = codeComponent.getGraphics();
						codeG.setColor(color.green);
						codeG.fillRect(0, codeLastView.height * (codeLine) + codeComponent.modelToView(0).y, codeComponent.getWidth(), codeLastView.height);				
					}
					
					//  Remove the highlighting from the previously highlighted line
					if (ptxLastView.y != currentPTXView.y)
					{
						int ptxCurrentLine = (currentPTXView.y - ptxComponent.modelToView(0).y) / currentPTXView.height;
						int ptxLastLine = (ptxLastView.y - ptxComponent.modelToView(0).y) / ptxLastView.height;
						
						int codeLine = ptxCodeArray[0][ptxCurrentLine+1];
						int codeLastLine = ptxCodeArray[0][ptxLastLine+1];

						ptxComponent.repaint(0, ptxLastView.y, ptxComponent.getWidth(), ptxLastView.height);
						
						Graphics codeG = codeComponent.getGraphics();
						codeG.setColor(color.green);

						if(codeLastLine != -1) {
							if(codeLastLine != codeLine) {
								codeComponent.repaint(0, codeLastView.height * (codeLastLine) + codeComponent.modelToView(0).y, codeComponent.getWidth(), codeLastView.height);
							}
						}
						if(codeLine != -1) {
							if(codeLastLine != codeLine) {
								codeG.fillRect(0, codeLastView.height * (codeLine) + codeComponent.modelToView(0).y, codeComponent.getWidth(), codeLastView.height);
							}
						}
						
						ptxLastView = currentPTXView;
					}
				}
				catch(BadLocationException ble) {}
			}
		});
	}
	
	//  Implement CaretListener

	public void caretUpdate(CaretEvent e)
	{
		resetHighlight();
	}

	//  Implement MouseListener

	public void mousePressed(MouseEvent e)
	{
		resetHighlight();
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	//  Implement MouseMotionListener

	public void mouseDragged(MouseEvent e)
	{
		resetHighlight();
	}

	public void mouseMoved(MouseEvent e) {}

}
