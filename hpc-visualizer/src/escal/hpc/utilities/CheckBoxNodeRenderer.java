package escal.hpc.utilities;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;


public class CheckBoxNodeRenderer implements TreeCellRenderer {
	
	private JCheckBox headRenderer = new JCheckBox();
	
	private DefaultTreeCellRenderer leafRenderer = new DefaultTreeCellRenderer();
	
	Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;
	
	private JEditorPane codeArea = null;
	
	protected JCheckBox headRenderer() {
		return headRenderer;
	}
	
	public CheckBoxNodeRenderer(JEditorPane inCodeArea) {
		
		Font fontValue;
		fontValue = UIManager.getFont("Tree.font");
		
		if (fontValue != null) {
			headRenderer.setFont(fontValue);
		}
		
		Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");
		headRenderer.setFocusPainted((booleanValue != null)&& (booleanValue.booleanValue()));

		selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
		selectionForeground = UIManager.getColor("Tree.selectionForeground");
		selectionBackground = UIManager.getColor("Tree.selectionBackground");
		textForeground = UIManager.getColor("Tree.textForeground");
		textBackground = UIManager.getColor("Tree.textBackground");
		
		codeArea = inCodeArea;
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		
		Component returnValue;
		if (!leaf) {
			
			String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, false);
			headRenderer.setText(stringValue);
			headRenderer.setSelected(false);
			headRenderer.setEnabled(tree.isEnabled());
			
			if (selected) {
				headRenderer.setForeground(selectionForeground);
				headRenderer.setBackground(selectionBackground);
			} 
			else {
				headRenderer.setForeground(textForeground);
				headRenderer.setBackground(textBackground);
			}

			if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
				Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
				
				if (userObject instanceof CheckBoxNode) {
					CheckBoxNode node = (CheckBoxNode) userObject;
					headRenderer.setText(node.getText());
					headRenderer.setSelected(node.isSelected());
				}
			}
			
			headRenderer.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					Document doc = codeArea.getDocument();
					
					Highlighter codeHL = codeArea.getHighlighter();
					
					
					try {
						
						String strTemp = headRenderer.getText();
						String[] str = null;
						str = strTemp.split("\\(");
						
						Pattern pat = Pattern.compile(str[0]);
						Matcher mat = pat.matcher(doc.getText(0, doc.getLength()));
						int pos = 0;
						int start, end;
						boolean isChecked = headRenderer.isSelected();
						
						while (mat.find(pos)) {
							
							start = mat.start();
							end = mat.end();
							
							if (isChecked) {
								codeHL.addHighlight(start, end, new DefaultHighlighter.DefaultHighlightPainter(Color.RED));
								pos = end;
							}
							else {
								pos = end;
								codeHL.removeAllHighlights();
							}
						}
						
						//highlight kernel and thread
						String grid = "dimGrid";
								
						Pattern patgrid = Pattern.compile(grid);
						Matcher matgrid = patgrid.matcher(doc.getText(0, doc.getLength()));
						int posgrid = 0;
						boolean isCheckedgrid = headRenderer.isSelected();
						
						while (matgrid.find(posgrid)) {
							
							start = matgrid.start();
							end = matgrid.end();
							
							if (isCheckedgrid) {
								codeHL.addHighlight(start, end, new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW));
								posgrid = end;
								
							}
							else {
								posgrid = end;
								codeHL.removeAllHighlights();
	
							}
							
						}
												
						String block = "dimBlock";
	
						Pattern patblock = Pattern.compile(block);
						Matcher matblock = patblock.matcher(doc.getText(0, doc.getLength()));
						int posblock = 0;
						boolean isCheckedblock = headRenderer.isSelected();
						
						while (matblock.find(posblock)) {
							
							start = matblock.start();
							end = matblock.end();
							if (isCheckedblock) {
								codeHL.addHighlight(start, end, new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN));
								posblock = end;
							
							}
							else {
								posblock = end;
								codeHL.removeAllHighlights();
							}
						}
						
					} catch (BadLocationException e1) {
						e1.printStackTrace();
					}
				}
				
				
				
			});
			returnValue = headRenderer;
		} 
		else {
			returnValue = leafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
		}
	    return returnValue;
	}
}