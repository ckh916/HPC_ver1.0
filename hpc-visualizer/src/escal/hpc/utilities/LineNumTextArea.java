package escal.hpc.utilities;

import javax.swing.JTextArea;
import javax.swing.text.Element;
import java.awt.*;

@SuppressWarnings("serial")
public class LineNumTextArea extends JTextArea {
	private JTextArea textArea;
	
	public LineNumTextArea(JTextArea textArea) {
		this.textArea = textArea;
		setBackground(Color.LIGHT_GRAY);
		setEditable(false);
	}
	
	public void updateLineNumbers() {
		String lineNumbersText = getLineNumbersText();
		setText(lineNumbersText);
	}
	
	private String getLineNumbersText() {
		int carePosition = textArea.getDocument().getLength();
		Element root = textArea.getDocument().getDefaultRootElement();
		StringBuilder lineNumbersTextBuilder = new StringBuilder();
		lineNumbersTextBuilder.append("1").append(System.lineSeparator());
		
		for (int elementIndex = 2; elementIndex < root.getElementIndex(carePosition) + 2; elementIndex++) {
			lineNumbersTextBuilder.append(elementIndex).append(System.lineSeparator());
		}
		return lineNumbersTextBuilder.toString();
	}	
}
