package escal.hpc.panels;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.text.Highlighter.HighlightPainter;

import escal.hpc.utilities.LineNumTextArea;
import escal.hpc.utilities.PTXPainter;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

@SuppressWarnings("serial")
public class GPUPTXPanel extends JPanel{
	
	//private JFrame cpuFrame;
	//private CPUMasterPanel cpuPanel;
	
	private JFrame parentFrame;
	
	int[][] ptxCodeArray;
	int numOfFiles = 1;
	int numPTXLine = 1;
	
	String pathCode;
	String pathPTX;
	String pathLineInfo;

	JTextArea codeArea = new JTextArea("", 0, 0);
	LineNumTextArea codeNumArea = new LineNumTextArea(codeArea);
	JPanel codePanel = new JPanel();
	JScrollPane codeScrollPane = new JScrollPane(codeArea);
	
	JTextArea ptxArea = new JTextArea("", 0, 0);
	LineNumTextArea ptxNumArea = new LineNumTextArea(ptxArea);
	JPanel ptxPanel = new JPanel();
	JScrollPane ptxScrollPane = new JScrollPane(ptxArea);

	String version = "Version v0.1.0 (20161027)";
	
	public GPUPTXPanel(JFrame _parentFrame, String _pathCode, String _pathPTX, String _pathLineInfo) {

		parentFrame = _parentFrame;
		pathCode = _pathCode;
		pathPTX = _pathPTX;
		pathLineInfo = _pathLineInfo;
		parentFrame.getContentPane().add(this);

		initGPUPanel();
		actionOpen();
		ptxCodeArray = new int[numOfFiles][numPTXLine];
		actionMap();
		
		PTXPainter ptxPainter = new PTXPainter(codeArea, ptxArea, ptxCodeArray);
	}

	private void initGPUPanel() {

		this.setLayout(new GridLayout(1, 2));
		
		GPUPTXPanel.this.codeArea.setEditable(false);
		GPUPTXPanel.this.ptxArea.setEditable(false);
		codeScrollPane.setRowHeaderView(codeNumArea);
		ptxScrollPane.setRowHeaderView(ptxNumArea);
		
		codePanel.setLayout(new BorderLayout());
		ptxPanel.setLayout(new BorderLayout());
		
		JLabel codeLabel = new JLabel("CUDA code");
		JLabel ptxLabel = new JLabel("PTX code");
		
		codePanel.add(codeLabel, BorderLayout.PAGE_START);
		codePanel.add(codeScrollPane, BorderLayout.CENTER);
		
		ptxPanel.add(ptxLabel, BorderLayout.PAGE_START);
		ptxPanel.add(ptxScrollPane, BorderLayout.CENTER);

		this.add(codePanel);
		this.add(ptxPanel);
	}

	public void destroy() {
		
		System.exit(0);
	}

	private void actionOpen() {
		//FileDialog load = new FileDialog(parentFrame, "Load source file");
		JTextArea codeTemp = new JTextArea("", 0, 0);
		JTextArea ptxTemp = new JTextArea("", 0, 0);
		codeTemp = GPUPTXPanel.this.codeArea;
		ptxTemp = GPUPTXPanel.this.ptxArea;

		FileReader fr = null;
		BufferedReader br = null;
		
		codeTemp.setText(null);
		codeTemp.setTabSize(4);
		ptxTemp.setText(null);
		ptxTemp.setTabSize(4);
		
		try {
			fr = new FileReader(pathCode);
			br = new BufferedReader(fr);
						
			String string = new String();
		
			do {
				string = br.readLine();
				
				if(string != null) {
					codeTemp.append(string + "\n");
				}
			} while(string != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
		try {
			fr = new FileReader(pathPTX);
			br = new BufferedReader(fr);

			String string = new String();
		
			do {
				string = br.readLine();
				
				if(string != null) {
					ptxTemp.append(string + "\n");
					numPTXLine += 1;
				}
			} while(string != null);
			
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
		
		codeNumArea.updateLineNumbers();
		ptxNumArea.updateLineNumbers();
		parentFrame.setTitle("HPCVisualizer - GPU Analyzer");
		codeTemp.setCaretPosition(0);
		codeTemp.setTabSize(4);
		ptxTemp.setCaretPosition(0);
		ptxTemp.setTabSize(4);

	}
	
	private void actionMap() {
		FileReader fr = null;
		BufferedReader br = null;
		int ptxLineCount = 0;
		int fileCount = -1;
		int cuLineCount = -1;
		
		try {
			fr = new FileReader(pathLineInfo);
			br = new BufferedReader(fr);
			
			String string = new String();
		
			do {
				string = br.readLine();
				
				if(string != null) {
					if(string.contains(".file")) {
					}
					else if(string.contains(".loc")) {
						ptxLineCount -= 2;
						fileCount = Integer.parseInt(string.split(" ")[1]) - 1;
						cuLineCount = Integer.parseInt(string.split(" ")[2]) - 1;
					}
					else if(string.equals("")) {
						ptxLineCount += 1;
						fileCount = -1;
						cuLineCount = -1;
						ptxCodeArray[0][ptxLineCount] = -1;
					}
					else if(fileCount != -1 && cuLineCount != -1) {
						ptxLineCount += 1;
						if(fileCount == 0) {
							ptxCodeArray[fileCount][ptxLineCount] = cuLineCount;
						}
						else {
							ptxCodeArray[0][ptxLineCount] = -1;
						}
					}
					else {
						ptxLineCount += 1;
						ptxCodeArray[0][ptxLineCount] = -1;
					}
				}
			} while(string != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
	}
	
}
