package escal.hpc.panels;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

public class MemoryDiagramPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2709727191120505260L;
	/**
	 * 
	 */
	
	
	ArrayList<String[]> aryCPU;
	ArrayList<String[]> aryMemory;
	//ArrayList<String> kernelList;
	DrawingMemoryPanel drawingPanel;
	
	//JComboBox<String> kernelSelect;
	
	public MemoryDiagramPanel(ArrayList<String[]> _aryCPU, ArrayList<String[]> _aryMemory) {
		
		aryCPU = _aryCPU;
		aryMemory = _aryMemory;
		
		/* Get the list of kernels and the number of kernels */
		/*
		kernelList = new ArrayList<String>();
		
		for (int i=1; i<aryMetrics.size(); i++) {
			kernelList.add(aryMetrics.get(i)[1]);
		}
		
		kernelList = new ArrayList<String>(new HashSet<String>(kernelList));
		*/
		setLayout(new BorderLayout());
		/*
		kernelSelect = new JComboBox<String>();
		
		for(int i=0; i<kernelList.size(); i++) {
			kernelSelect.addItem(kernelList.get(i));
		}
		*/
		drawingPanel = new DrawingMemoryPanel(aryCPU, aryMemory);//, (String) kernelSelect.getSelectedItem());
		
		//this.add(kernelSelect, BorderLayout.PAGE_START);
		this.add(drawingPanel, BorderLayout.CENTER);
		/*
		kernelSelect.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				drawingPanel.updatePaint((String) kernelSelect.getSelectedItem());
				drawingPanel.repaint();
				drawingPanel.revalidate();
			}
		});
		*/
	}
}

class DrawingMemoryPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5910640712900279614L;

	/**
	 * 
	 */
	
	
	private final int ARR_SIZE = 4;
	
	ArrayList<String[]> aryCPU;
	ArrayList<String[]> aryMemory;
	//String selectedKernel;
	
	String skt0ch0Read, skt0ch0Write;
	String skt0ch1Read, skt0ch1Write;
	String skt0ch2Read, skt0ch2Write;
	String skt0ch3Read, skt0ch3Write;
	
	String skt1ch0Read, skt1ch0Write;
	String skt1ch1Read, skt1ch1Write;
	String skt1ch2Read, skt1ch2Write;
	String skt1ch3Read, skt1ch3Write;
	
	String skt0Read, skt0Write;
	String skt1Read, skt1Write;
	
	String skt0QPI0, skt0QPI1;
	String skt1QPI0, skt1QPI1;
	
	
	public DrawingMemoryPanel(ArrayList<String[]> _aryCPU,	ArrayList<String[]> _aryMemory) { //,String _selectedKernel) {
		
		aryCPU = _aryCPU;
		aryMemory = _aryMemory;
		//selectedKernel = _selectedKernel;
		
		parseData();
	}
	
	public void updatePaint() {
		
		//selectedKernel = _selectedKernel;
		parseData();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		/* Some variables */
		Color lightBlue = new Color(91, 155, 213);
		Color skyBlue = new Color(91, 155, 213);
		Color Grey = new Color(190, 193, 198);
		Color Green = new Color(75, 158, 64);
		int panelWidth = this.getWidth();
		int panelHeight = this.getHeight();
		int margin = 10;
		int round = 25;
		int gap = 20;
		int effLength = panelWidth - 2*margin;
		int effHeight = panelHeight - 2*margin;
		int effStart = margin;
		
		
		RoundRectangle2D skt0Rect,skt1Rect;	
		RoundRectangle2D membus0Rect,membus1Rect;
		RoundRectangle2D skt0ch0Rect,skt0ch1Rect,skt0ch2Rect,skt0ch3Rect;
		RoundRectangle2D skt1ch0Rect,skt1ch1Rect,skt1ch2Rect,skt1ch3Rect;
		RoundRectangle2D skt0memRect,skt1memRect;
		
		/* Configuring graphic parameters */
		super.setBackground(Color.WHITE);
		Graphics2D g2 = (Graphics2D)g;
		g2.setBackground(Color.WHITE);
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 14));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(lightBlue);
		/*
		skt0memRect = new RoundRectangle2D.Float (margin, margin, effLength*3/17+2*margin, panelHeight - 2*margin, round, round);
		skt1memRect = new RoundRectangle2D.Float (effStart + effLength*14/17 - 2*margin, margin, effLength*3/17+2*margin, panelHeight - 2*margin, round, round);
				
		skt0ch0Rect = new RoundRectangle2D.Float (2*margin, margin + 10, effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt0ch1Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + (effHeight-3*gap)/4 + gap - 5, effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt0ch2Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + 2*((effHeight-3*gap)/4 + gap - 5), effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt0ch3Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + 3*((effHeight-3*gap)/4 + gap - 5), effLength*3/17, (effHeight-3*gap)/4, round, round);
		
		membus0Rect = new RoundRectangle2D.Float (effStart + effLength*4/17, margin, effLength/17, panelHeight - 2*margin, round, round);
				
		skt0Rect = new RoundRectangle2D.Float    (effStart + effLength*6/17, margin + (panelHeight - 2*margin)/4, effLength*2/17, (panelHeight - 2*margin)*1/2, round, round);
		skt1Rect = new RoundRectangle2D.Float    (effStart + effLength*9/17, margin + (panelHeight - 2*margin)/4, effLength*2/17, (panelHeight - 2*margin)*1/2, round, round);
		
		membus1Rect = new RoundRectangle2D.Float (effStart + effLength*12/17, margin, effLength/17, panelHeight - 2*margin, round, round);
		
		skt1ch0Rect = new RoundRectangle2D.Float (effStart + effLength*14/17 - margin, margin + 10, effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt1ch1Rect = new RoundRectangle2D.Float (effStart + effLength*14/17 - margin, margin + 10 + (effHeight-3*gap)/4 + gap -5    , effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt1ch2Rect = new RoundRectangle2D.Float (effStart + effLength*14/17 - margin, margin + 10 + 2*((effHeight-3*gap)/4 + gap -5), effLength*3/17, (effHeight-3*gap)/4, round, round);
		skt1ch3Rect = new RoundRectangle2D.Float (effStart + effLength*14/17 - margin, margin + 10 + 3*((effHeight-3*gap)/4 + gap -5), effLength*3/17, (effHeight-3*gap)/4, round, round);
		*/
		skt0memRect = new RoundRectangle2D.Float (margin, margin, effLength*2/17+2*margin, panelHeight - 2*margin, round, round);
		skt1memRect = new RoundRectangle2D.Float (effStart + effLength*15/17 - 2*margin, margin, effLength*2/17+2*margin, panelHeight - 2*margin, round, round);
				
		skt0ch0Rect = new RoundRectangle2D.Float (2*margin, margin + 10, effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt0ch1Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + (effHeight-3*gap)/4 + gap - 5, effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt0ch2Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + 2*((effHeight-3*gap)/4 + gap - 5), effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt0ch3Rect = new RoundRectangle2D.Float (2*margin, margin + 10 + 3*((effHeight-3*gap)/4 + gap - 5), effLength*2/17, (effHeight-3*gap)/4, round, round);
		
		membus0Rect = new RoundRectangle2D.Float (effStart + effLength*4/17, margin, effLength/17, panelHeight - 2*margin, round, round);
				
		skt0Rect = new RoundRectangle2D.Float    (effStart + effLength*6/17, margin + (panelHeight - 2*margin)/4, effLength*2/17, (panelHeight - 2*margin)*1/2, round, round);
		skt1Rect = new RoundRectangle2D.Float    (effStart + effLength*9/17, margin + (panelHeight - 2*margin)/4, effLength*2/17, (panelHeight - 2*margin)*1/2, round, round);
		
		membus1Rect = new RoundRectangle2D.Float (effStart + effLength*12/17, margin, effLength/17, panelHeight - 2*margin, round, round);
		
		skt1ch0Rect = new RoundRectangle2D.Float (effStart + effLength*15/17 - margin, margin + 10, effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt1ch1Rect = new RoundRectangle2D.Float (effStart + effLength*15/17 - margin, margin + 10 + (effHeight-3*gap)/4 + gap -5    , effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt1ch2Rect = new RoundRectangle2D.Float (effStart + effLength*15/17 - margin, margin + 10 + 2*((effHeight-3*gap)/4 + gap -5), effLength*2/17, (effHeight-3*gap)/4, round, round);
		skt1ch3Rect = new RoundRectangle2D.Float (effStart + effLength*15/17 - margin, margin + 10 + 3*((effHeight-3*gap)/4 + gap -5), effLength*2/17, (effHeight-3*gap)/4, round, round);
		
		/*
		g2.fill(kernelRect);
		
		g2.fill(textureRect);
		g2.fill(localRect);
		g2.fill(globalRect);
		g2.fill(sharedRect);
		
		g2.fill(textureCacheRect);
		g2.fill(l1CacheRect);
		g2.fill(sharedMemoryRect);
		
		g2.fill(l2CacheRect);
		
		g2.fill(systemMemoryRect);
		g2.fill(deviceMemoryRect);
		*/
		
		g2.fill(skt0memRect);
		g2.fill(skt1memRect);
		
		g2.setColor(Grey);
		g2.fill(skt0Rect);
		g2.fill(skt1Rect);
		
		g2.setColor(lightBlue);
		g2.fill(membus0Rect);
		g2.fill(membus1Rect);
		
		g2.setColor(Green);
		g2.fill(skt0ch0Rect);
		g2.fill(skt0ch1Rect);
		g2.fill(skt0ch2Rect);
		g2.fill(skt0ch3Rect);

		g2.fill(skt1ch0Rect);
		g2.fill(skt1ch1Rect);
		g2.fill(skt1ch2Rect);
		g2.fill(skt1ch3Rect);
		
		/* Guide Lines */
		/*
		g2.setColor(Color.RED);
		g2.drawLine(10, 0, 10, panelHeight);
		g2.drawLine(panelWidth/20+margin, 0, panelWidth/20+10, panelHeight);
		g2.drawLine(panelWidth-10, 0, panelWidth-10, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength/8, 0, panelWidth/20+margin + effLength/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*2/8, 0, panelWidth/20+margin + effLength*2/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*3/8, 0, panelWidth/20+margin + effLength*3/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*4/8, 0, panelWidth/20+margin + effLength*4/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*5/8, 0, panelWidth/20+margin + effLength*5/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*6/8, 0, panelWidth/20+margin + effLength*6/8, panelHeight);
		g2.drawLine(panelWidth/20+margin + effLength*7/8, 0, panelWidth/20+margin + effLength*7/8, panelHeight);
		g2.drawLine(0, 10, panelWidth, 10);
		g2.drawLine(0, panelHeight-10, panelWidth, panelHeight-10);
		*/
		
		/* Normal Strings */
		g2.setColor(Color.WHITE);
		
		drawCenteredString(g2, "CPU0", skt0Rect);
		drawCenteredString(g2, "CPU1", skt1Rect);

		drawCenteredString(g2, "Channel0", skt0ch0Rect);
		drawCenteredString(g2, "Channel1", skt0ch1Rect);
		drawCenteredString(g2, "Channel2", skt0ch2Rect);
		drawCenteredString(g2, "Channel3", skt0ch3Rect);

		drawCenteredString(g2, "Channel0", skt1ch0Rect);
		drawCenteredString(g2, "Channel1", skt1ch1Rect);
		drawCenteredString(g2, "Channel2", skt1ch2Rect);
		drawCenteredString(g2, "Channel3", skt1ch3Rect);		
		
		/* Draw arrow lines */
		g2.setColor(Color.DARK_GRAY);
		g2.setStroke(new BasicStroke(2));
			
		drawArrowFromRect(g2, skt0ch0Rect);
		drawArrowFromRect(g2, skt0ch1Rect);
		drawArrowFromRect(g2, skt0ch2Rect);
		drawArrowFromRect(g2, skt0ch3Rect);
				
		drawHalfArrowToRect(g2, skt0Rect);
		drawArrowQPIRect(g2, skt0Rect);
		drawHalfArrowFromRect(g2, skt1Rect);
		
		drawArrowToRect(g2, skt1ch0Rect);
		drawArrowToRect(g2, skt1ch1Rect);
		drawArrowToRect(g2, skt1ch2Rect);
		drawArrowToRect(g2, skt1ch3Rect);
		
		
		
		
		
		/* Information Strings */
		g2.setFont(new Font(Font.SERIF, Font.PLAIN, 8));
		g2.setColor(Color.BLACK);

		//System.out.println("aa"+skt0ch0Read);
		
		drawDataString(g2, skt0ch0Read+" MB/s", skt0ch0Rect, membus0Rect,
				(int) (skt0ch0Rect.getY()+skt0ch0Rect.getHeight()/3));
		drawDataString(g2, skt0ch0Write+" MB/s", skt0ch0Rect, membus0Rect,
				(int) (skt0ch0Rect.getY()+skt0ch0Rect.getHeight()*2/3));
		
		drawDataString(g2, skt0ch1Read+" MB/s", skt0ch1Rect, membus0Rect,
				(int) (skt0ch1Rect.getY()+skt0ch1Rect.getHeight()/3));
		drawDataString(g2, skt0ch1Write+" MB/s", skt0ch1Rect, membus0Rect,
				(int) (skt0ch1Rect.getY()+skt0ch1Rect.getHeight()*2/3));
		
		drawDataString(g2, skt0ch2Read+" MB/s", skt0ch2Rect, membus0Rect,
				(int) (skt0ch2Rect.getY()+skt0ch2Rect.getHeight()/3));
		drawDataString(g2, skt0ch2Write+" MB/s", skt0ch2Rect, membus0Rect,
				(int) (skt0ch2Rect.getY()+skt0ch2Rect.getHeight()*2/3));
		
		drawDataString(g2, skt0ch3Read+" MB/s", skt0ch3Rect, membus0Rect,
				(int) (skt0ch3Rect.getY()+skt0ch3Rect.getHeight()/3));
		drawDataString(g2, skt0ch3Write+" MB/s", skt0ch3Rect, membus0Rect,
				(int) (skt0ch3Rect.getY()+skt0ch3Rect.getHeight()*2/3));
		
		
		drawDataString(g2, skt0Read+" MB/s", membus0Rect,skt0Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()/3));
		drawDataString(g2, skt0Write+" MB/s", membus0Rect,skt0Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()*2/3));	
		
		
		drawDataString(g2, skt1QPI0+" MB/s", skt0Rect,skt1Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()*3/13));		
		drawDataString(g2, skt1QPI1+" MB/s", skt0Rect,skt1Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()*8/13));		
	
		drawDataString(g2, skt0QPI0+" MB/s", skt0Rect,skt1Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()*10/13));		
		drawDataString(g2, skt0QPI1+" MB/s", skt0Rect,skt1Rect, 
				(int) (skt0Rect.getY()+skt0Rect.getHeight()*5/13));		
		
				
		drawDataString(g2, skt1Read+" MB/s", skt1Rect, membus1Rect,
				(int) (skt1Rect.getY()+skt1Rect.getHeight()/3));
		drawDataString(g2, skt1Write+" MB/s", skt1Rect, membus1Rect, 
				(int) (skt1Rect.getY()+skt1Rect.getHeight()*2/3));
		
		
		drawDataString(g2, skt1ch0Read+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch0Rect.getY()+skt1ch0Rect.getHeight()/3));
		drawDataString(g2, skt1ch0Write+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch0Rect.getY()+skt1ch0Rect.getHeight()*2/3));
		
		drawDataString(g2, skt1ch1Read+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch1Rect.getY()+skt1ch1Rect.getHeight()/3));
		drawDataString(g2, skt1ch1Write+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch1Rect.getY()+skt1ch1Rect.getHeight()*2/3));
		
		drawDataString(g2, skt1ch2Read+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch2Rect.getY()+skt1ch2Rect.getHeight()/3));
		drawDataString(g2, skt1ch2Write+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch2Rect.getY()+skt1ch2Rect.getHeight()*2/3));
		
		drawDataString(g2, skt1ch3Read+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch3Rect.getY()+skt1ch3Rect.getHeight()/3));
		drawDataString(g2, skt1ch3Write+" MB/s", membus1Rect, skt1memRect, 
				(int) (skt1ch3Rect.getY()+skt1ch3Rect.getHeight()*2/3));
							
		
		
		
		/* Rotated Strings */
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 14));
		g2.setColor(Color.WHITE);
		AffineTransform orig = g2.getTransform();
		orig.rotate(-Math.PI/2);
		g2.setTransform(orig);
		//g2.drawString("Kernel", -(margin+panelHeight/2), margin+panelWidth/28);
		g2.drawString("Memory Bus", -(margin+panelHeight*2/3), effStart + effLength*9/34);
		g2.drawString("Memory Bus", -(margin+panelHeight*2/3), effStart + effLength*25/34);
	}
	
	private void drawCenteredString(Graphics2D g2, String text, RoundRectangle2D rect) {
		
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int x = (int) (rect.getX() + (rect.getWidth() - metrics.stringWidth(text))/2);
		int y = (int) (rect.getY() + (rect.getHeight() - metrics.getHeight())/2 + metrics.getAscent());
		g2.drawString(text, x, y);
	}
	
	private void drawDataString(Graphics2D g2, String text, RoundRectangle2D Leftrect, RoundRectangle2D Rightrect, int _y) {
		
		int gap = getGap(Leftrect,Rightrect);
		int margin = 2;
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int x = (int)(Rightrect.getX()-metrics.stringWidth(text)/2-gap/2);
		int y = _y - margin;
		g2.drawString(text, x, y);
		
	}	

	private void drawDoubleArrowToRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()/3+10), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()/3+10), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawDoubleArrowFromRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()+inRect.getWidth()*3/3-10), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()+inRect.getWidth()*4/3-10), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	
	private void drawArrowToRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()*3/3+10), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()*3/3+10), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawArrowFromRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()+inRect.getWidth()*6/3-10), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()+inRect.getWidth()*6/3-10), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawHalfArrowToRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()/2), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()/2), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawHalfArrowFromRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawArrowQPIRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*3/13), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()*3/13));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*5/13), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()*5/13));
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*8/13), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()*8/13));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*10/13), 
				(int)(inRect.getX()+inRect.getWidth()*3/2), 
				(int)(inRect.getY()+inRect.getHeight()*10/13));
	}
	
	private void drawRightArrow(Graphics2D g2, int x1, int y1, int x2, int y2) {

        g2.drawLine(x1, y1, x2, y2);
        g2.drawLine(x2, y2, x2-ARR_SIZE, y2-ARR_SIZE);
        g2.drawLine(x2, y2, x2-ARR_SIZE, y2+ARR_SIZE);
	}
	
	private void drawLeftArrow(Graphics2D g2, int x1, int y1, int x2, int y2) {
		
        g2.drawLine(x1, y1, x2, y2);
        g2.drawLine(x1, y1, x1+ARR_SIZE, y1-ARR_SIZE);
        g2.drawLine(x1, y1, x1+ARR_SIZE, y1+ARR_SIZE);
	}
	
	private int getGap(RoundRectangle2D LeftRect, RoundRectangle2D RightRect) {
		return (int)( RightRect.getX() - (LeftRect.getX() + LeftRect.getWidth()));
	}
	
	private void parseData() {
		/* Parsing Memory profiling data */
		String skt0="SKT0";
		String skt1="SKT1";
		
		for(int i=0; i<aryMemory.get(0).length; i++) {
			if(skt0.equals(aryMemory.get(0)[i])) {
				switch (aryMemory.get(1)[i]) {
				
				case "Ch0Read": skt0ch0Read = aryMemory.get(2)[i]; break;
				case "Ch1Read": skt0ch1Read = aryMemory.get(2)[i]; break;
				case "Ch2Read": skt0ch2Read = aryMemory.get(2)[i]; break;
				case "Ch3Read": skt0ch3Read = aryMemory.get(2)[i]; break;
				
				case "Ch0Write": skt0ch0Write = aryMemory.get(2)[i]; break;
				case "Ch1Write": skt0ch1Write = aryMemory.get(2)[i]; break;
				case "Ch2Write": skt0ch2Write = aryMemory.get(2)[i]; break;
				case "Ch3Write": skt0ch3Write = aryMemory.get(2)[i]; break;
				
				case "Mem Read (MB/s)": skt0Read = aryMemory.get(2)[i]; break;
				case "Mem Write (MB/s)": skt0Write = aryMemory.get(2)[i]; break;				

				default: break;
				}
			}
			else if(skt1.equals(aryMemory.get(0)[i])) {
				
				switch (aryMemory.get(1)[i]) {
				
				case "Ch0Read": skt1ch0Read = aryMemory.get(2)[i]; break;
				case "Ch1Read": skt1ch1Read = aryMemory.get(2)[i]; break;
				case "Ch2Read": skt1ch2Read = aryMemory.get(2)[i]; break;
				case "Ch3Read": skt1ch3Read = aryMemory.get(2)[i]; break;
				
				case "Ch0Write": skt1ch0Write = aryMemory.get(2)[i]; break;
				case "Ch1Write": skt1ch1Write = aryMemory.get(2)[i]; break;
				case "Ch2Write": skt1ch2Write = aryMemory.get(2)[i]; break;
				case "Ch3Write": skt1ch3Write = aryMemory.get(2)[i]; break;
				
				case "Mem Read (MB/s)": skt1Read = aryMemory.get(2)[i]; break;
				case "Mem Write (MB/s)": skt1Write = aryMemory.get(2)[i]; break;				

				default: break;
				}			
			}			
			else continue;
		}
		/* Parsing CPU QPI profiling data */
		
		String skt0qpi="SKT0trafficOut";
		String skt1qpi="SKT1trafficOut";
		
		for(int i=0; i<aryCPU.get(0).length; i++) {
			
			if(skt0qpi.equals(aryCPU.get(0)[i])) {
				
				switch (aryCPU.get(1)[i]) {
				
				case "QPI0": skt0QPI0 = aryCPU.get(2)[i]; break;
				case "QPI1": skt0QPI1 = aryCPU.get(2)[i]; break;
				
				default: break;
				}
			}
			else if(skt1qpi.equals(aryCPU.get(0)[i])) {
				
				switch (aryCPU.get(1)[i]) {
				
				case "QPI0": skt1QPI0 = aryCPU.get(2)[i]; break;
				case "QPI1": skt1QPI1 = aryCPU.get(2)[i]; break;
				
				default: break;
				}			
			}			
			else continue;
		}
		
	}
	
	private String seperateNumUnit(String _inString) {
		
		String numberPart, unitPart;
		Float numberFloat;
		
		unitPart = _inString.replaceAll("\\d|\\.", ""); //remove numbers
		numberPart = _inString.replaceAll(unitPart, ""); // remove characters except '.'
		
		numberFloat = Float.parseFloat(numberPart);
		numberPart = String.format("%.3f", numberFloat);
		_inString = numberPart + unitPart;
		
		return _inString;
	}
}
