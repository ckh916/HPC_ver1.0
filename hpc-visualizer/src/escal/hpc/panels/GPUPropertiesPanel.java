package escal.hpc.panels;

import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GPUPropertiesPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1862111137546329167L;
	
	private JFrame parentFrame;
	private Properties projectProp;
	private String pathConfig;
	
	private JTextField nvccTxt = null;
	private JTextField nvprofTxt = null;

	public GPUPropertiesPanel(JFrame _parentFrame, Properties _projectProp, String _pathConfig) {
		
		parentFrame = _parentFrame;
		projectProp = _projectProp;
		pathConfig = _pathConfig;
		
		initPropertiesPanel();
	}
	
	private void initPropertiesPanel() {
		
		this.setLayout(null);
		
		JLabel nvccLabel = new JLabel("NVCC Path");
		JLabel nvprofLabel = new JLabel("NVPROF Path");
		
		nvccTxt = new JTextField();
		nvprofTxt = new JTextField();
		
		JButton nvccBrowseBtn = new JButton("Browse..");
		JButton nvprofBrowseBtn = new JButton("Browse..");
		
		JButton okBtn = new JButton("OK");
		JButton cancleBtn = new JButton("Cancel");
		
		/* Labels of configuration */
		nvccTxt.setText(projectProp.getProperty("pathNvcc"));
		nvprofTxt.setText(projectProp.getProperty("pathNvprof"));
		
		nvccLabel.setBounds(10,10,100,25);
		nvccLabel.setFont(new Font(Font.SERIF, Font.BOLD, 12));
		nvprofLabel.setBounds(10,40,100,25);
		nvprofLabel.setFont(new Font(Font.SERIF, Font.BOLD, 12));
		
		this.add(nvccLabel);
		this.add(nvprofLabel);
		
		nvccTxt.setBounds(120,10,370,25);
		nvccTxt.setEditable(false);
		nvprofTxt.setBounds(120,40,370,25);
		nvprofTxt.setEditable(false);
		
		this.add(nvccTxt);
		this.add(nvprofTxt);
		
		/* Buttons of configuration */
		nvccBrowseBtn.setBounds(495, 10, 100, 25);
		nvprofBrowseBtn.setBounds(495, 40, 100, 25);
		
		nvccBrowseBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionBrowse(nvccTxt);
			}
		});
		
		nvprofBrowseBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionBrowse(nvprofTxt);
			}
		});
		
		this.add(nvccBrowseBtn);
		this.add(nvprofBrowseBtn);
		
		/* OK and Cancel button */
		okBtn.setBounds(495, 220, 100, 25);
		cancleBtn.setBounds(390, 220, 100, 25);
		
		okBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionOK();
			}
		});
		
		cancleBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// do nothing
				parentFrame.dispose();
			}
		});
		
		this.add(okBtn);
		this.add(cancleBtn);
		
		/* Finally add to parent frame */
		parentFrame.add(this);
	}
	
	void actionOK() {
		
		if (JOptionPane.showConfirmDialog(parentFrame, "Are you sure?\nAll changes will be saved.", "Warning",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			projectProp.setProperty("pathNvcc", nvccTxt.getText());
			projectProp.setProperty("pathNvprof", nvprofTxt.getText());
			
			try {
				
				projectProp.store(new FileOutputStream(pathConfig), "ini file write done");
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			parentFrame.dispose();
		}
		else {
			// nothing
		}
	}
	
	void actionBrowse(JTextField _textField) {
		
		FileDialog fileLoad = new FileDialog(parentFrame, "Browse..", FileDialog.LOAD);
		
		fileLoad.setLocation(parentFrame.getLocation());
		fileLoad.setVisible(true);
		
		if (fileLoad.getFile() == null) {
			
			return;
		}
		
		_textField.setText(fileLoad.getDirectory() + fileLoad.getFile());
	}
}
