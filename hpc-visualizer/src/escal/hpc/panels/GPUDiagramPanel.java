package escal.hpc.panels;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class GPUDiagramPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1232628135743308137L;
	
	ArrayList<String[]> aryEvents;
	ArrayList<String[]> aryMetrics;
	ArrayList<String> kernelList;
	DrawingPanel drawingPanel;
	
	JComboBox<String> kernelSelect;
	
	public GPUDiagramPanel(ArrayList<String[]> _aryEvents, ArrayList<String[]> _aryMetrics) {
		
		aryEvents = _aryEvents;
		aryMetrics = _aryMetrics;
		
		/* Get the list of kernels and the number of kernels */
		kernelList = new ArrayList<String>();
		
		for (int i=1; i<aryMetrics.size(); i++) {
			kernelList.add(aryMetrics.get(i)[1]);
		}
		
		kernelList = new ArrayList<String>(new HashSet<String>(kernelList));
		
		setLayout(new BorderLayout());
		
		kernelSelect = new JComboBox<String>();
		
		for(int i=0; i<kernelList.size(); i++) {
			kernelSelect.addItem(kernelList.get(i));
		}
		
		drawingPanel = new DrawingPanel(aryEvents, aryMetrics, (String) kernelSelect.getSelectedItem());
		
		this.add(kernelSelect, BorderLayout.PAGE_START);
		this.add(drawingPanel, BorderLayout.CENTER);
		
		kernelSelect.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				drawingPanel.updatePaint((String) kernelSelect.getSelectedItem());
				drawingPanel.repaint();
				drawingPanel.revalidate();
			}
		});
	}
}

class DrawingPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1241684274610104942L;
	
	private final int ARR_SIZE = 4;
	
	ArrayList<String[]> aryEvents;
	ArrayList<String[]> aryMetrics;
	String selectedKernel;
	
	String localStoreTx, localLoadTx;
	String globalStoreTx, globalLoadTx;
	String sharedStoreTx, sharedLoadTx;
	
	String sharedStoreTh, sharedLoadTh;
	
	String l2TextWriteTh, l2TextReadTh;
	String l2WriteTh, l2ReadTh;
	
	String sysMemWriteTh, sysMemReadTh;
	String devMemWriteTh, devMemReadTh;
	
	public DrawingPanel(ArrayList<String[]> _aryEvents,	ArrayList<String[]> _aryMetrics, 
			String _selectedKernel) {
		
		aryEvents = _aryEvents;
		aryMetrics = _aryMetrics;
		selectedKernel = _selectedKernel;
		
		parseData();
	}
	
	public void updatePaint(String _selectedKernel) {
		
		selectedKernel = _selectedKernel;
		parseData();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		/* Some variables */
		Color lightBlue = new Color(91, 155, 213);
		int panelWidth = this.getWidth();
		int panelHeight = this.getHeight();
		int margin = 10;
		int round = 25;
		int gap = 20;
		int effLength = panelWidth*19/20 - 2*margin;
		int effHeight = panelHeight - 2*margin;
		int effStart = panelWidth/20 + margin;
		
		RoundRectangle2D kernelRect, textureRect, localRect, globalRect, sharedRect;
		RoundRectangle2D textureCacheRect, l1CacheRect, sharedMemoryRect;
		RoundRectangle2D l2CacheRect, systemMemoryRect, deviceMemoryRect;
		
		/* Configuring graphic parameters */
		super.setBackground(Color.WHITE);
		Graphics2D g2 = (Graphics2D)g;
		g2.setBackground(Color.WHITE);
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 14));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g2.setColor(lightBlue);
		
		kernelRect = new RoundRectangle2D.Float(margin, margin, panelWidth/20, panelHeight - 2*margin, round, round);
		
		textureRect = new RoundRectangle2D.Float(effStart + effLength*1/8, margin, effLength/8, (effHeight-3*gap)/4, round, round);
		localRect = new RoundRectangle2D.Float(effStart + effLength*1/8, margin + (effHeight-3*gap)/4 + gap, effLength/8, (effHeight-3*gap)/4, round, round);
		globalRect = new RoundRectangle2D.Float(effStart + effLength*1/8, margin + 2*((effHeight-3*gap)/4 + gap), effLength/8, (effHeight-3*gap)/4, round, round);
		sharedRect = new RoundRectangle2D.Float(effStart + effLength*1/8, margin + 3*((effHeight-3*gap)/4 + gap), effLength/8, (effHeight-3*gap)/4, round, round);
		
		textureCacheRect = new RoundRectangle2D.Float(effStart + effLength*3/8, margin, effLength/8, (effHeight-3*gap)/4, round, round);
		l1CacheRect = new RoundRectangle2D.Float(effStart + effLength*3/8, margin + (effHeight-3*gap)/4 + gap, effLength/8, (effHeight-3*gap)/2 + gap, round, round);
		sharedMemoryRect = new RoundRectangle2D.Float(effStart + effLength*3/8, margin + 3*((effHeight-3*gap)/4 + gap), effLength/8, (effHeight-3*gap)/4, round, round);
		
		l2CacheRect = new RoundRectangle2D.Float(effStart + effLength*5/8, margin, effLength/8, (effHeight-3*gap)*3/4 + 2*gap, round, round);
		
		systemMemoryRect = new RoundRectangle2D.Float(effStart + effLength*7/8, margin, effLength/8, ((effHeight-3*gap)*3/4 + 1*gap)/2, round, round);
		deviceMemoryRect = new RoundRectangle2D.Float(effStart + effLength*7/8, margin + ((effHeight-3*gap)*3/4 + 1*gap)/2 + gap, effLength/8, ((effHeight-3*gap)*3/4 + 1*gap)/2, round, round);
		
		g2.fill(kernelRect);
		
		g2.fill(textureRect);
		g2.fill(localRect);
		g2.fill(globalRect);
		g2.fill(sharedRect);
		
		g2.fill(textureCacheRect);
		g2.fill(l1CacheRect);
		g2.fill(sharedMemoryRect);
		
		g2.fill(l2CacheRect);
		
		g2.fill(systemMemoryRect);
		g2.fill(deviceMemoryRect);
		
		/* Normal Strings */
		g2.setColor(Color.WHITE);
		drawCenteredString(g2, "Texture", textureRect);
		drawCenteredString(g2, "Local", localRect);
		drawCenteredString(g2, "Global", globalRect);
		drawCenteredString(g2, "Shared", sharedRect);		
		drawCenteredString(g2, "T-Cache", textureCacheRect);
		drawCenteredString(g2, "L1 Cache", l1CacheRect);
		drawCenteredString(g2, "S-Memory", sharedMemoryRect);		
		drawCenteredString(g2, "L2 Cache", l2CacheRect);		
		drawCenteredString(g2, "SysMem", systemMemoryRect);
		drawCenteredString(g2, "DevMem", deviceMemoryRect);	
		
		/* Draw arrow lines */
		g2.setColor(Color.DARK_GRAY);
		g2.setStroke(new BasicStroke(2));
		
		drawArrowToRect(g2, textureRect);
		drawArrowToRect(g2, localRect);
		drawArrowToRect(g2, globalRect);
		drawArrowToRect(g2, sharedRect);	
		drawArrowToRect(g2, textureCacheRect);
		drawArrowFromRect(g2, localRect);
		drawArrowFromRect(g2, globalRect);
		drawArrowToRect(g2, sharedMemoryRect);		
		drawArrowFromRect(g2, textureCacheRect);
		drawArrowFromRect(g2, l1CacheRect);
		drawArrowToRect(g2, systemMemoryRect);
		drawArrowToRect(g2, deviceMemoryRect);
		
		/* Information Strings */
		//g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
		//g2.setColor(Color.BLACK);
		
		if(compare(localStoreTx,localLoadTx)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, localStoreTx+" Reqs", localRect, (int) (localRect.getY()+localRect.getHeight()/3-2));
		
		if(compare(localStoreTx,localLoadTx)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, localLoadTx+" Reqs", localRect, 	(int) (localRect.getY()+localRect.getHeight()*2/3-2));
		
		if(compare(globalStoreTx,globalLoadTx)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, globalStoreTx+" Reqs", globalRect, (int) (globalRect.getY()+globalRect.getHeight()/3-2));
		
		if(compare(globalStoreTx,globalLoadTx)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, globalLoadTx+" Reqs", globalRect, (int) (globalRect.getY()+globalRect.getHeight()*2/3-2));
		
		if(compare(sharedStoreTx,sharedLoadTx)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sharedStoreTx+" Reqs", sharedRect, (int) (sharedRect.getY()+sharedRect.getHeight()/3-2));
		
		if(compare(sharedStoreTx,sharedLoadTx)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sharedLoadTx+" Reqs", sharedRect, (int) (sharedRect.getY()+sharedRect.getHeight()*2/3-2));
		
		if(compare(sharedStoreTh,sharedLoadTh)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sharedStoreTh, sharedMemoryRect, (int) (sharedMemoryRect.getY()+sharedMemoryRect.getHeight()/3-2));
		
		if(compare(sharedStoreTh,sharedLoadTh)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sharedLoadTh, sharedMemoryRect, (int) (sharedMemoryRect.getY()+sharedMemoryRect.getHeight()*2/3-2));
		
		if(compare(l2TextWriteTh,l2TextReadTh)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, l2TextWriteTh, l2CacheRect, (int) (textureCacheRect.getY()+textureCacheRect.getHeight()/3-2));

		if(compare(l2TextWriteTh,l2TextReadTh)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, l2TextReadTh, l2CacheRect, (int) (textureCacheRect.getY()+textureCacheRect.getHeight()*2/3-2));
		
		if(compare(l2WriteTh,l2ReadTh)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, l2WriteTh, l2CacheRect, (int) (l1CacheRect.getY()+l1CacheRect.getHeight()/3-2));
		
		if(compare(l2WriteTh,l2ReadTh)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, l2ReadTh, l2CacheRect, (int) (l1CacheRect.getY()+l1CacheRect.getHeight()*2/3-2));
		
		if(compare(sysMemWriteTh,sysMemReadTh)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sysMemWriteTh, systemMemoryRect, (int) (systemMemoryRect.getY()+systemMemoryRect.getHeight()/3-2));
		
		if(compare(sysMemWriteTh,sysMemReadTh)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, sysMemReadTh, systemMemoryRect, (int) (systemMemoryRect.getY()+systemMemoryRect.getHeight()*2/3-2));
		
		if(compare(devMemWriteTh,devMemReadTh)=="L") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, devMemWriteTh, deviceMemoryRect, (int) (deviceMemoryRect.getY()+deviceMemoryRect.getHeight()/3-2));
		
		if(compare(devMemWriteTh,devMemReadTh)=="R") {
			g2.setFont(new Font(Font.SERIF, Font.BOLD, 12));
			g2.setColor(Color.RED);}
		else {
			g2.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
			g2.setColor(Color.BLACK);}
		drawDataString(g2, devMemReadTh, deviceMemoryRect, (int) (deviceMemoryRect.getY()+deviceMemoryRect.getHeight()*2/3-2));
		
		//g2.drawString(selectedKernel, panelWidth - g2.getFontMetrics(g2.getFont()).stringWidth(selectedKernel)-3, panelHeight - g2.getFontMetrics(g2.getFont()).getAscent());
		
		/* Rotated Strings */
		g2.setFont(new Font(Font.SERIF, Font.BOLD, 14));
		g2.setColor(Color.WHITE);
		AffineTransform orig = g2.getTransform();
		orig.rotate(-Math.PI/2);
		g2.setTransform(orig);
		g2.drawString("Kernel", -(margin+panelHeight/2), margin+panelWidth/28);
	}
	
	private void drawCenteredString(Graphics2D g2, String text, RoundRectangle2D rect) {
		
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int x = (int) (rect.getX() + (rect.getWidth() - metrics.stringWidth(text))/2);
		int y = (int) (rect.getY() + (rect.getHeight() - metrics.getHeight())/2 + metrics.getAscent());
		g2.drawString(text, x, y);
	}
	
	private void drawDataString(Graphics2D g2, String text, RoundRectangle2D rect, int y) {
		
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int x = (int)(rect.getX()-rect.getWidth() + (rect.getWidth()-metrics.stringWidth(text))/2);
		g2.drawString(text, x, y);
	}
	
	private void drawArrowToRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()-inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawArrowFromRect(Graphics2D g2, RoundRectangle2D inRect) {
		
		drawRightArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3), 
				(int)(inRect.getX()+2*inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()/3));
		drawLeftArrow(g2, 
				(int)(inRect.getX()+inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3), 
				(int)(inRect.getX()+2*inRect.getWidth()), 
				(int)(inRect.getY()+inRect.getHeight()*2/3));
	}
	
	private void drawRightArrow(Graphics2D g2, int x1, int y1, int x2, int y2) {

        g2.drawLine(x1, y1, x2, y2);
        g2.drawLine(x2, y2, x2-ARR_SIZE, y2-ARR_SIZE);
        g2.drawLine(x2, y2, x2-ARR_SIZE, y2+ARR_SIZE);
	}
	
	private void drawLeftArrow(Graphics2D g2, int x1, int y1, int x2, int y2) {
		
        g2.drawLine(x1, y1, x2, y2);
        g2.drawLine(x1, y1, x1+ARR_SIZE, y1-ARR_SIZE);
        g2.drawLine(x1, y1, x1+ARR_SIZE, y1+ARR_SIZE);
	}
	
	private void parseData() {
		
		for(int i=0; i<aryMetrics.size(); i++) {
			
			if(selectedKernel.equals(aryMetrics.get(i)[1])) {
				
				switch (aryMetrics.get(i)[3]) {
				
				case "local_store_transactions": localStoreTx = aryMetrics.get(i)[7]; break;
				case "local_load_transactions": localLoadTx = aryMetrics.get(i)[7]; break;
				case "gst_transactions": globalStoreTx = aryMetrics.get(i)[7]; break;
				case "gld_transactions": globalLoadTx = aryMetrics.get(i)[7]; break;
				case "shared_store_transactions": sharedStoreTx = aryMetrics.get(i)[7]; break;
				case "shared_load_transactions": sharedLoadTx = aryMetrics.get(i)[7]; break;
				
				case "shared_store_throughput": sharedStoreTh = aryMetrics.get(i)[7]; break;
				case "shared_load_throughput": sharedLoadTh = aryMetrics.get(i)[7]; break;
				
				case "l2_tex_write_throughput": l2TextWriteTh = aryMetrics.get(i)[7]; break;
				case "l2_tex_read_throughput": l2TextReadTh = aryMetrics.get(i)[7]; break;
				case "l2_write_throughput": l2WriteTh = aryMetrics.get(i)[7]; break;
				case "l2_read_throughput": l2ReadTh = aryMetrics.get(i)[7]; break;
				
				case "sysmem_write_throughput": sysMemWriteTh = aryMetrics.get(i)[7]; break;
				case "sysmem_read_throughput": sysMemReadTh = aryMetrics.get(i)[7]; break;
				case "dram_write_throughput": devMemWriteTh = aryMetrics.get(i)[7]; break;
				case "dram_read_throughput": devMemReadTh = aryMetrics.get(i)[7]; break;

				default: break;
				}
			}
			else continue;
		}
		
		sharedStoreTh = seperateNumUnit(sharedStoreTh);
		sharedLoadTh = seperateNumUnit(sharedLoadTh);
		
		l2TextWriteTh = seperateNumUnit(l2TextWriteTh);
		l2TextReadTh = seperateNumUnit(l2TextReadTh);
		l2WriteTh = seperateNumUnit(l2WriteTh);
		l2ReadTh = seperateNumUnit(l2ReadTh);
		
		sysMemWriteTh = seperateNumUnit(sysMemWriteTh);
		sysMemReadTh = seperateNumUnit(sysMemReadTh);
		devMemWriteTh = seperateNumUnit(devMemWriteTh);
		devMemReadTh = seperateNumUnit(devMemReadTh);

	}
	
	private String seperateNumUnit(String _inString) {
		
		String numberPart, unitPart;
		Float numberFloat;
		
		unitPart = _inString.replaceAll("\\d|\\.", ""); //remove numbers
		numberPart = _inString.replaceAll(unitPart, ""); // remove characters except '.'
		
		numberFloat = Float.parseFloat(numberPart);
		numberPart = String.format("%.3f", numberFloat);
		_inString = numberPart + unitPart;
		
		return _inString;
	}
	private String compare(String _inString1,String _inString2) {
		String unitPart1,unitPart2;
		String numberPart1,numberPart2;
		double in1, in2;
		
		//initialize
		unitPart1 = _inString1.replaceAll("\\d|\\.", ""); 
		numberPart1 = _inString1.replaceAll(unitPart1, "");
		in1 = Float.parseFloat(numberPart1);
		unitPart2 = _inString2.replaceAll("\\d|\\.", ""); 
		numberPart2 = _inString2.replaceAll(unitPart2, "");
		in2 = Float.parseFloat(numberPart2);
		
		//considering unit part
		if(unitPart1.contains("GB/s")) in1 *= Math.pow(10, 9);
		else if(unitPart1.contains("MB/s")) in1 *= Math.pow(10, 6);
		else if(unitPart1.contains("MB/s")) in1 *= Math.pow(10, 3);

		if(unitPart2.contains("GB/s")) in2 *= Math.pow(10, 9);
		else if(unitPart2.contains("MB/s")) in2 *= Math.pow(10, 6);
		else if(unitPart2.contains("MB/s")) in2 *= Math.pow(10, 3);
		
		//compare 
		int ratio = 10; //how difference between data to be highlighted
		if(in1 == 0 && in2 == 0) return "";
		else if(in1 >= ratio * in2) return "L";
		else if(in2 >= ratio * in1) return "R";
		else return "";		
	}
	
}
