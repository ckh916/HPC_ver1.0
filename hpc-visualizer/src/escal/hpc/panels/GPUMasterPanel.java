package escal.hpc.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.SimpleTimePeriod;

import escal.hpc.utilities.CheckBoxNodeEditor;
import escal.hpc.utilities.CheckBoxNodeRenderer;
import escal.hpc.utilities.LineNumTextArea;
import escal.hpc.utilities.Miscellaneous;
import jsyntaxpane.DefaultSyntaxKit;

import org.jfree.chart.plot.PiePlot; 

public class GPUMasterPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8219085059463328556L;
	
	private JFrame parentFrame;
	private JFrame ptxFrame;
	private JFrame SuggestFrame;
	private GPUPTXPanel ptxPanel;
	private GPUsuggestCodePanel2 suggestcodePanel;
	
	JTabbedPane tabbedCodePane = new JTabbedPane();
	JTabbedPane tabbedcompareGraphPane = new JTabbedPane();
	JTabbedPane tabbedcomparestallGraphPane = new JTabbedPane(); //issue stall reason
	JTabbedPane tabbedcompareL2GraphPane = new JTabbedPane(); //L2
	JTabbedPane tabbedcompareglobalGraphPane = new JTabbedPane(); //Global 
	JTabbedPane tabbedcomparesharedmemoryGraphPane = new JTabbedPane(); //sharedmemory
	JTabbedPane tabbedCPUPane = new JTabbedPane();
	
	JEditorPane syntaxedCodeArea = new JEditorPane();
	JScrollPane codeScrollPane = new JScrollPane(syntaxedCodeArea);
	
	
	JPanel analyzerPanel = new JPanel(); // Use JSplitPane to enhance UX
	JTabbedPane tabbedGraphPane = new JTabbedPane();
	JPanel resultShowPanel = new JPanel();
	JTabbedPane tabbedShowPanel  = new JTabbedPane();
	JPanel mGPUPanel = new JPanel();
	JPanel summarymetricPanel = new JPanel();
	JPanel summarystallPanel = new JPanel(); //issue stall reason
	JPanel summaryL2Panel = new JPanel(); //L2 cahce
	JPanel summaryglobalPanel = new JPanel(); //global 
	JPanel summarysharedmemoryPanel = new JPanel(); //sharedmemory
	JPanel resultPanel;
	FileDialog codeFileLoad = new FileDialog(parentFrame, "Load source file", FileDialog.LOAD);
	
	String pathRoot = System.getProperty("user.dir");
	String pathConfig = pathRoot + "/config.ini";
	String pathNvcc = null;
	String pathNvprof = null;
	String pathCodeDirectory = null;
	String pathCodeFile = null;
	String pathCode = null;
	String pathBin = null;
	String pathPTX = null;
	String pathExport = null;
	String pathLineInfo = null;
	String pathpcm = null;
	String pathpcmMemory = null;
	String projectName = null;
	
	ArrayList<String> getline = new ArrayList<String>();
	int line; // number of code line
	
	ArrayList<String[]> arySummary = new ArrayList<String[]>();
	ArrayList<String[]> arySummaryAPI = new ArrayList<String[]>();
	ArrayList<String[]> aryTrace = new ArrayList<String[]>();
	ArrayList<String[]> aryEvents = new ArrayList<String[]>();
	ArrayList<String[]> aryMetrics = new ArrayList<String[]>();
	
	String version = "Version v1.0.0 (20161215)";
	
	int numberOfKernel = 0;
	long cmdExportElapsedTime, cmdEventsMetricsElapsedTime;
	long cmdAdditionalElapsedTime;
	
	Miscellaneous misc = new Miscellaneous();
	
	Properties projectProp = new Properties();
	
	
	public GPUMasterPanel(JFrame _parentFrame) {
		
		parentFrame = _parentFrame;
		parentFrame.getContentPane().add(this);
		
		initGPUPanel();
	}
	
	private void initGPUPanel() {
		
		// TODO enhance layout with JSplitPane
		this.setLayout(new GridLayout(1, 2));
					
		/* Initializing graph area */
		analyzerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c0 = new GridBagConstraints();
		c0.weightx=1.0; c0.weighty=1.0;
		c0.fill = GridBagConstraints.BOTH;

		JPanel graphPanel = new JPanel();
		resultPanel = new JPanel();
		JPanel cpuPanel = new JPanel(); 

		graphPanel.setLayout(new BorderLayout());
		resultPanel.setLayout(new BorderLayout());
		cpuPanel.setLayout(new BorderLayout());
				
		JLabel graphLabel = new JLabel("Graphs & Tables");
		JLabel resultLabel = new JLabel("Analysis Results: Low Utilized Resources");
		JLabel cpuLabel = new JLabel("CPU Profiling");
		
		graphLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		graphLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		graphPanel.add(graphLabel, BorderLayout.PAGE_START);
		graphPanel.add(mGPUPanel, BorderLayout.CENTER);
		mGPUPanel.setLayout(new BorderLayout());
		mGPUPanel.add(tabbedGraphPane,BorderLayout.CENTER);
		
		cpuLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		cpuLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		cpuPanel.add(cpuLabel, BorderLayout.PAGE_START);
		cpuPanel.add(tabbedCPUPane, BorderLayout.CENTER);	
		
		resultLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		resultLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		resultPanel.add(resultLabel, BorderLayout.PAGE_START);
		
		c0.gridx=0; c0.gridy=0; c0.gridwidth=1; c0.gridheight=2;
		analyzerPanel.add(cpuPanel, c0);
		c0.gridx=0; c0.gridy=2; c0.gridwidth=1; c0.gridheight=2;
		analyzerPanel.add(graphPanel, c0);
		c0.gridx=0; c0.gridy=4; c0.gridwidth=1; c0.gridheight=1;
		analyzerPanel.add(resultPanel, c0);
		
		DefaultSyntaxKit.initKit();
		
		try {
			projectProp.load(new FileInputStream(pathConfig));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pathNvcc = projectProp.getProperty("pathNvcc");
		pathNvprof = projectProp.getProperty("pathNvprof");
		pathpcm = projectProp.getProperty("pathpcm");
		pathpcmMemory = projectProp.getProperty("pathpcmMemory");

		
		 this.add(tabbedCodePane);
		this.add(analyzerPanel);
		
		parentFrame.add(this);
		addMenuBarToGPUFrame();
	}
	
	private void addMenuBarToGPUFrame() {
		
		JMenuBar menuBar = new JMenuBar();
		parentFrame.setJMenuBar(menuBar);
		
		/* File menu */
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		
		JMenuItem openItem = new JMenuItem("Open File");
		openItem.setMnemonic('O');
		openItem.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.ALT_MASK));
		openItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionOpen();
			}
		});
		/* End of file opening */
		
		JMenuItem cleanItem = new JMenuItem("Clean Project");
		cleanItem.setMnemonic('C');
		cleanItem.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.ALT_MASK));
		cleanItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (JOptionPane.showConfirmDialog(parentFrame, 
						"Are you sure?\n"
						+ "Every 'csv' file, 'ptx' file, 'bin' file, and analyze result will be deleted.\n"
						+ "This action cannot be undone.", 
						"Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					actionClean();
				}
				else {
					// nothing
				}
				
			}
		});
		
		JMenuItem propItem = new JMenuItem("Properties");
		propItem.setMnemonic('P');
		propItem.setAccelerator(KeyStroke.getKeyStroke('P', InputEvent.ALT_MASK));
		propItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionProp();				
			}
		});
		

		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.setMnemonic('E');
		exitItem.setAccelerator(KeyStroke.getKeyStroke('E', InputEvent.ALT_MASK));
		exitItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (JOptionPane.showConfirmDialog(parentFrame, "Are you sure?", "Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					misc.destroy();					
				}
				else {
					// nothing
				}
			}
		});
		
		fileMenu.add(openItem);
		fileMenu.addSeparator();
		fileMenu.add(cleanItem);
		fileMenu.addSeparator();
		fileMenu.add(propItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		/* End of file menu */
		
		/* Run menu */
		JMenu runMenu = new JMenu("Run");
		runMenu.setMnemonic('R');
		
		JMenuItem compileItem = new JMenuItem("Compile");
		compileItem.setMnemonic('C');
		compileItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		JMenuItem executionItem = new JMenuItem("Execution");
		executionItem.setMnemonic('E');
		executionItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
		
		JMenuItem mGPUItem = new JMenuItem("Muli-GPU");
		mGPUItem.setMnemonic('M');
		mGPUItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
		
		compileItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionComp();
			}
		});
		
		executionItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionExec();
			}
		});
		

		
		
		/* Get the list of kernels and the number of kernels */
		
		
		mGPUItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				actionGPU();
			}
		});
		
		runMenu.add(compileItem);
		runMenu.addSeparator();
		runMenu.add(executionItem);
		runMenu.addSeparator();
		runMenu.add(mGPUItem);
		/* End of run menu */
		
		/* Help menu */
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');

		/* Tool menu */
		JMenu toolsMenu = new JMenu("Tools");
		toolsMenu.setMnemonic('T');
		
		JMenuItem ptxItem = new JMenuItem("PTX code");
		ptxItem.setMnemonic('P');
		ptxItem.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.ALT_MASK));
		
		ptxItem.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				actionPTX();
			}
		});
		toolsMenu.add(ptxItem);
		
		JMenuItem detailItem = new JMenuItem("Detail analysis");
		detailItem.setMnemonic('D');
		detailItem.setAccelerator(KeyStroke.getKeyStroke('D', InputEvent.ALT_MASK));
		
		detailItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				actionSuggest();
			}
		});

		toolsMenu.add(detailItem);


		/* End of tool menu */
		
		/* Item declarations for Help Menu */
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.setMnemonic('A');
		aboutItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		aboutItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(parentFrame, 
						"HPC-Visualizer, GPU Analyzer\n"
						+ version
						+ "\n\nMinistry of Science, ICT and Future Planning, Korea\n"
						+ "Embedded Systems and Computer Architecture Lab.\n"
						+ "Yonsei University, Korea\n"
						+ "\nContact:\n"
						+ "Minsik Kim (minsik.kim@yonsei.ac.kr)\n"
						+ "Yoonsoo Kim (yoonsoo.kim@yonsei.ac.kr)\n"
						+ "Won Jeon (jeonwon@yonsei.ac.kr)", 
						"About", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		helpMenu.add(aboutItem);
		/* End of help menu */
		
		
		menuBar.add(fileMenu);
		menuBar.add(runMenu);
		menuBar.add(toolsMenu);
		menuBar.add(helpMenu);
		
	}
	
	private void actionSuggest()
	{
		SuggestFrame = new JFrame("Suggest Code");
		SuggestFrame.setPreferredSize(new Dimension(1500, 800));
		SuggestFrame.setSize(new Dimension(1500, 800));
		SuggestFrame.setResizable(false);
		misc.alignWindow(SuggestFrame);
		
		String pathsuggestCode = pathRoot + "/optimizedcode/";
		suggestcodePanel = new GPUsuggestCodePanel2(SuggestFrame, pathsuggestCode, aryMetrics);
		
		SuggestFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SuggestFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				
				suggestcodePanel.setVisible(false);
			}
		});
		SuggestFrame.pack();
		SuggestFrame.setVisible(true);
	} 
	  
    private void actionGPU() { // press F7
	
		JOptionPane.showMessageDialog(parentFrame, "Profiling multi-GPU", "Info", JOptionPane.INFORMATION_MESSAGE);
	
		pathBin = pathRoot + "/projects/" + codeFileLoad.getFile() + ".out";
		pathNvprof = projectProp.getProperty("pathNvprof");
		pathpcm = projectProp.getProperty("pathpcm");
		pathpcmMemory = projectProp.getProperty("pathpcmMemory");
		pathExport = pathBin + "_export";	
		
		// argument parsing
		String[] cmdExportOri = {pathNvprof,"--unified-memory-profiling", "off","--force-overwrite","--export-profile",pathExport,pathBin};
		String[] cmdSummaryOri = {pathNvprof,"--import-profile",pathExport,
				"--csv","--log-file",pathBin + "_summary.csv"};
		String[] cmdTraceOri = {pathNvprof,"--import-profile",pathExport,"--print-gpu-trace","--print-api-trace",
				"--csv","--log-file",pathBin + "_trace.csv"};
		String[] cmdEventsMetricsOri = {pathNvprof,"--events","all","--metrics","all",
				"--csv","--log-file",pathBin + "_eventsmetrics.csv",pathBin};
		
		try {
			long startTime = System.nanoTime();
			Process p = Runtime.getRuntime().exec(cmdExportOri);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			long endTime = System.nanoTime();
			cmdExportElapsedTime = endTime-startTime;
			
			p = Runtime.getRuntime().exec(cmdSummaryOri);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			
			p = Runtime.getRuntime().exec(cmdTraceOri);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
	
			startTime = System.nanoTime();
			p = Runtime.getRuntime().exec(cmdEventsMetricsOri);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			endTime = System.nanoTime();
			cmdEventsMetricsElapsedTime = endTime - startTime;
	
			startTime = System.nanoTime();
			readCSV(pathBin+"_summary.csv");
			readCSV(pathBin+"_trace.csv");
			readCSV(pathBin+"_eventsmetrics.csv");
			
			/* add list box */
			ArrayList<String> kernelList;
			JComboBox<String> kernelSelect;
			kernelList = new ArrayList<String>();
					
			// read type of GPU
			for (int i=1; i<aryMetrics.size(); i++) kernelList.add(aryMetrics.get(i)[0]);
	
			kernelList = new ArrayList<String>(new HashSet<String>(kernelList));
			kernelSelect = new JComboBox<String>();
			
			if(kernelList.size()>1) kernelSelect.addItem("Summary");
			for(int i=0; i<kernelList.size(); i++) kernelSelect.addItem(kernelList.get(i));
	
			mGPUPanel.add(kernelSelect, BorderLayout.PAGE_START);
			
			kernelSelect.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if((String) kernelSelect.getSelectedItem()=="Summary") {
						tabbedGraphPane.removeAll();
						
						actioncomparecriticmetric(); //Compare Calls
						//actionstallSummary(); //Issue Stall Reason
						actionstalllistSummary(); // Issue Stall Reason
						actionL2listSummary(); // L2 cache
						actiongloballistSummary(); // global
						actionsharedmemorylistSummary(); // sharedmemory
						actionmetricSummary(); // etc
						
						resultShowPanel.removeAll();
						showAnalResult();
			
					} //compare between GPUs
					else { // analyze chosen GPU
						tabbedGraphPane.removeAll();			
						drawmGPUGraphs();	
						
						tabbedGraphPane.addTab("GPU Diagram", null, new GPUDiagramPanel(aryEvents, aryMetrics), "Diagram of Target GPU");
						System.out.println((String) kernelSelect.getSelectedItem());
						drawmGPUTable((String) kernelSelect.getSelectedItem());
						resultShowPanel.removeAll();
						showAnalResult();}
				}
			});
	
			JOptionPane.showMessageDialog(parentFrame, "NVPROF execution and CSV file parsing end.\n"
					+ "Output CSV: " + pathRoot, "Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Execution end");
			
			endTime = System.nanoTime();
			cmdAdditionalElapsedTime = endTime - startTime;
			
			System.out.println("Export Time: " + cmdExportElapsedTime + " ns");
			System.out.println("Events/Metrics Time: " + cmdEventsMetricsElapsedTime + " ns");
			System.out.println("Additional Time: " + cmdAdditionalElapsedTime + " ns");
			System.out.println("\nAnalysis Overhead: " + cmdAdditionalElapsedTime/(float)cmdExportElapsedTime*100 + "%");
			System.out.println("Analysis Overhead(icl e/m): " + cmdAdditionalElapsedTime/(float)(cmdExportElapsedTime+cmdEventsMetricsElapsedTime)*100 + "%\n");
		}
		catch (IOException | InterruptedException el) {
			// TODO Auto-generated catch block
			el.printStackTrace();
		}
	}
   
    private void drawmGPUTable(String selectedGPU) { // summary about GPU
		
		JPanel summaryTablePanel = new JPanel();
		JPanel summaryAPITablePanel = new JPanel();
		JPanel traceTablePanel = new JPanel();
		JPanel eventsTablePanel = new JPanel();
		JPanel metricsTablePanel = new JPanel();
		
		JScrollPane summaryTableScroll = new JScrollPane(summaryTablePanel);
		JScrollPane summaryAPITableScroll = new JScrollPane(summaryAPITablePanel);
		JScrollPane traceTableScroll = new JScrollPane(traceTablePanel);
		JScrollPane eventsTableScroll = new JScrollPane(eventsTablePanel);
		JScrollPane metricsTableScroll = new JScrollPane(metricsTablePanel);
		
		DefaultTableModel summaryTM = new DefaultTableModel();
		DefaultTableModel summaryAPITM = new DefaultTableModel();
		DefaultTableModel traceTM = new DefaultTableModel();
		DefaultTableModel eventsTM = new DefaultTableModel();
		DefaultTableModel metricsTM = new DefaultTableModel();
		
		JTable summaryTable = new JTable(summaryTM);
		JTable summaryAPITable = new JTable(summaryAPITM);
		JTable traceTable = new JTable(traceTM);
		JTable eventsTable = new JTable(eventsTM);
		JTable metricsTable = new JTable(metricsTM);
		
		/* Set Column names */
		Vector<String> summaryColumn = new Vector<String>();
		Vector<String> summaryAPIColumn = new Vector<String>();
		Vector<String> traceColumn = new Vector<String>();
		Vector<String> eventsColumn = new Vector<String>();
		Vector<String> metricsColumn = new Vector<String>();
		Vector<String> tempRow;
		
		for (int i=0; i<arySummary.get(0).length; i++) {
			summaryColumn.addElement(arySummary.get(0)[i]);
		}
		
		for (int i=0; i<arySummaryAPI.get(0).length; i++) {
			summaryAPIColumn.addElement(arySummaryAPI.get(0)[i]);
		}
		
		for (int i=0; i<aryTrace.get(0).length; i++) {
			traceColumn.addElement(aryTrace.get(0)[i]);
		}
		
		for (int i=0; i<aryEvents.get(0).length; i++) {
			eventsColumn.addElement(aryEvents.get(0)[i]);
		}
		
		for (int i=0; i<aryMetrics.get(0).length; i++) {
			metricsColumn.addElement(aryMetrics.get(0)[i]);
		}
		
		summaryTM = new DefaultTableModel(null, summaryColumn);
		summaryTable.setModel(summaryTM);
		summaryTable.setEnabled(false);
		
		summaryAPITM = new DefaultTableModel(null, summaryAPIColumn);
		summaryAPITable.setModel(summaryAPITM);
		summaryAPITable.setEnabled(false);
		
		traceTM = new DefaultTableModel(null, traceColumn);
		traceTable.setModel(traceTM);
		traceTable.setEnabled(false);
		
		eventsTM = new DefaultTableModel(null, eventsColumn);
		eventsTable.setModel(eventsTM);
		eventsTable.setEnabled(false);
		
		metricsTM = new DefaultTableModel(null, metricsColumn);
		metricsTable.setModel(metricsTM);
		metricsTable.setEnabled(false);
		
		/* Add data for each row */
		for (int i=1; i<arySummary.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummary.get(0).length; j++) {
				tempRow.addElement(arySummary.get(i)[j]);
			}
			summaryTM.addRow(tempRow);
		}
		
		for (int i=1; i<arySummaryAPI.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummaryAPI.get(0).length; j++) {
				tempRow.addElement(arySummaryAPI.get(i)[j]);
			}
			summaryAPITM.addRow(tempRow);
		}
		
		for (int i=1; i<aryTrace.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryTrace.get(0).length; j++) {
				tempRow.addElement(aryTrace.get(i)[j]);
			}
			traceTM.addRow(tempRow);
		}
		
		for (int i=1; i<aryEvents.size(); i++) {
			tempRow = new Vector<String>();

			if(aryEvents.get(i)[0].equals(selectedGPU) ) {
				for (int j=0; j<aryEvents.get(0).length; j++) {
					tempRow.addElement(aryEvents.get(i)[j]);
				}
				eventsTM.addRow(tempRow);
			}
		}
		
		for (int i=1; i<aryMetrics.size(); i++) {
			tempRow = new Vector<String>();

			if(aryMetrics.get(i)[0].equals(selectedGPU)) {
				for (int j=0; j<aryMetrics.get(0).length; j++) {
					tempRow.addElement(aryMetrics.get(i)[j]);
				}
				metricsTM.addRow(tempRow);
			}
		}
		
		/* Add table to panel */
		summaryTablePanel.setLayout(new BorderLayout());
		summaryAPITablePanel.setLayout(new BorderLayout());
		traceTablePanel.setLayout(new BorderLayout());
		eventsTablePanel.setLayout(new BorderLayout());
		metricsTablePanel.setLayout(new BorderLayout());
		
		summaryTablePanel.add(summaryTable.getTableHeader(), BorderLayout.PAGE_START);
		summaryTablePanel.add(summaryTable, BorderLayout.CENTER);
		
		summaryAPITablePanel.add(summaryAPITable.getTableHeader(), BorderLayout.PAGE_START);
		summaryAPITablePanel.add(summaryAPITable, BorderLayout.CENTER);
		
		traceTablePanel.add(traceTable.getTableHeader(), BorderLayout.PAGE_START);
		traceTablePanel.add(traceTable, BorderLayout.CENTER);
		
		eventsTablePanel.add(eventsTable.getTableHeader(), BorderLayout.PAGE_START);
		eventsTablePanel.add(eventsTable, BorderLayout.CENTER);
		
		metricsTablePanel.add(metricsTable.getTableHeader(), BorderLayout.PAGE_START);
		metricsTablePanel.add(metricsTable, BorderLayout.CENTER);
		
		summaryTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		summaryAPITableScroll.getVerticalScrollBar().setUnitIncrement(16);
		traceTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		eventsTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		metricsTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		tabbedGraphPane.addTab("Summary Table", null, summaryTableScroll, "Table for summary");
		tabbedGraphPane.addTab("API Calls Table", null, summaryAPITableScroll, "Table for API calls");
		tabbedGraphPane.addTab("Trace Table", null, traceTableScroll, "Table for CPU/GPU Trace");
		tabbedGraphPane.addTab("Events Table", null, eventsTableScroll, "Table for events");
		tabbedGraphPane.addTab("Metrics Table", null, metricsTableScroll, "Table for metrics");
	}
	
	private void drawmGPUGraphs() {//summary about GPU at summary, api call, cpu/gpu trace 
		
		JPanel summaryPanel = new JPanel();
		JPanel summaryAPIPanel = new JPanel();
		//summaryPanel.setFont(new Font("",0,10));
//		IntervalCategoryDataset traceData = new TaskSeriesCollection();
		TaskSeriesCollection traceCollection = new TaskSeriesCollection();
		TaskSeries traceTS = new TaskSeries("Duration");
				
		DefaultPieDataset summaryPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryBarData = new DefaultCategoryDataset();
		
		DefaultPieDataset summaryAPIPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryAPIBarData = new DefaultCategoryDataset();
		
		JFreeChart summaryPieChart = null;
		JFreeChart summaryBarChart = null;
		JFreeChart summaryAPIPieChart = null;
		JFreeChart summaryAPIBarChart = null;
		JFreeChart traceChart = null;
		
		ChartPanel summaryPieChartPanel = null;
		ChartPanel summaryBarChartPanel = null;
		ChartPanel summaryAPIPieChartPanel = null;
		ChartPanel summaryAPIBarChartPanel = null;
		ChartPanel traceChartPanel = null;
		
		ArrayList<String> traceTaskList = new ArrayList<String>();
		
		summaryPanel.setLayout(new GridLayout(1,2));
		summaryAPIPanel.setLayout(new GridLayout(1,2));
	
		/* Draw gantt chart */
		long axisStart = (long) (Double.parseDouble(aryTrace.get(2)[1])*1000);
		long axisEnd = (long) (((Double.parseDouble(aryTrace.get(aryTrace.size()-1)[1]) + 
				(Double.parseDouble(aryTrace.get(aryTrace.size()-1)[2]))) + 1)*1000);
		for (int i=2; i<aryTrace.size(); i++) {

			traceTaskList.add(aryTrace.get(i)[0]);
		}
		
		traceTaskList = new ArrayList<String>(new HashSet<String>(traceTaskList));
		
		for (int i=0; i<traceTaskList.size(); i++) {
			
			traceTS.add(new Task(traceTaskList.get(i), new SimpleTimePeriod(axisStart, axisEnd)));
		}
		
		for (int i=2; i<aryTrace.size(); i++) {
			
			long start_time = (long) (Double.parseDouble(aryTrace.get(i)[1])*1000);
			long end_time = start_time + (long) (Double.parseDouble(aryTrace.get(i)[2])*1000);
			int index = 0;
			if (start_time == end_time)
				end_time = start_time + 1;
			
			Task tempTask = new Task(aryTrace.get(i)[0], new SimpleTimePeriod(start_time, end_time));
			
			for (int j=0; j<traceTaskList.size(); j++) {
				
				if (traceTaskList.get(j).equals(aryTrace.get(i)[0])) {
					index = j;
					break;
				}
			}
			traceTS.get(index).addSubtask(tempTask);
		}
		
		traceCollection.add(traceTS);
//		traceData = traceCollection;
		
		/* Draw summary pie and bar chart */
		for (int i=2; i<arySummary.size(); i++) {
			
			summaryPieData.setValue(arySummary.get(i)[6], Double.parseDouble(arySummary.get(i)[0]));
			
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[3]), "Avg", arySummary.get(i)[6]);
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[4]), "Min", arySummary.get(i)[6]);
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[5]), "Max", arySummary.get(i)[6]);
		}
		
		/* Draw summary API pie and bar chart */
		for (int i=2; i<arySummaryAPI.size(); i++) {
			
			summaryAPIPieData.setValue(arySummaryAPI.get(i)[6], Double.parseDouble(arySummaryAPI.get(i)[0]));
			
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[3]), "Avg", arySummaryAPI.get(i)[6]);
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[4])/1000, "Min", arySummaryAPI.get(i)[6]);
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[5]), "Max", arySummaryAPI.get(i)[6]);
		}
		
		/* Create charts */
		summaryPieChart = ChartFactory.createPieChart("Kernel Execution Ratio", 
				summaryPieData, false, true, true);
		
		summaryBarChart = ChartFactory.createBarChart("Kernel Execution Time", "Kernel", "Time (us)", 
				summaryBarData, PlotOrientation.HORIZONTAL, true, true, false);
		
		summaryAPIPieChart = ChartFactory.createPieChart("API Calls Ratio", summaryAPIPieData, 
				false, true, true);
		summaryAPIBarChart = ChartFactory.createBarChart("API Execution Time", "API Call", "Time (ms)", 
				summaryAPIBarData, PlotOrientation.HORIZONTAL, true, true, false);
		
		traceChart = ChartFactory.createGanttChart("CPU/GPU Trace", "Name", "Time (ns)", 
				traceCollection, false, false, false);
		
		/* remove block on pie graph */
		PiePlot plot = (PiePlot) summaryPieChart.getPlot();
		plot.setInteriorGap(0.0);
		plot.setLabelGenerator(null);
		
		PiePlot plotAPI = (PiePlot) summaryAPIPieChart.getPlot();
		plotAPI.setInteriorGap(0.0);
		plotAPI.setLabelGenerator(null);
		
		/* Configure charts */
		summaryPieChart.setBackgroundPaint(Color.WHITE);
		summaryBarChart.setBackgroundPaint(Color.WHITE);
		summaryAPIPieChart.setBackgroundPaint(Color.WHITE);
		summaryAPIBarChart.setBackgroundPaint(Color.WHITE);
		traceChart.setBackgroundPaint(Color.WHITE);
		
		summaryPieChart.setBorderVisible(true);
		summaryBarChart.setBorderVisible(true);
		summaryAPIPieChart.setBorderVisible(true);
		summaryAPIBarChart.setBorderVisible(true);
		
		Plot summaryPiePlot = summaryPieChart.getPlot();
		CategoryPlot summaryBarPlot = summaryBarChart.getCategoryPlot();
		Plot summaryAPIPiePlot = summaryAPIPieChart.getPlot();
		CategoryPlot summaryAPIBarPlot = summaryAPIBarChart.getCategoryPlot();
		CategoryPlot tracePlot = traceChart.getCategoryPlot();
		
		summaryPiePlot.setBackgroundPaint(Color.WHITE);
		summaryBarPlot.setBackgroundPaint(Color.WHITE);
		summaryAPIPiePlot.setBackgroundPaint(Color.WHITE);
		summaryAPIBarPlot.setBackgroundPaint(Color.WHITE);
		tracePlot.setBackgroundPaint(Color.WHITE);
		
		summaryBarPlot.setRangeGridlinePaint(Color.GRAY);
		summaryAPIBarPlot.setRangeGridlinePaint(Color.GRAY);
		tracePlot.setRangeGridlinePaint(Color.GRAY);
		
		CategoryAxis summaryBarAxis = summaryBarPlot.getDomainAxis();
		CategoryAxis summaryAPIBarAxis = summaryAPIBarPlot.getDomainAxis();
		
		summaryBarAxis.setLowerMargin(0.02);
		summaryBarAxis.setUpperMargin(0.02);
		summaryBarAxis.setCategoryMargin(0.1);

		summaryAPIBarAxis.setLowerMargin(0.02);
		summaryAPIBarAxis.setUpperMargin(0.02);
		summaryAPIBarAxis.setCategoryMargin(0.1);

		BarRenderer summaryBarRenderer = (BarRenderer) summaryBarPlot.getRenderer();
		BarRenderer summaryAPIBarRenderer = (BarRenderer) summaryAPIBarPlot.getRenderer();
		BarRenderer traceRenderer = (BarRenderer) tracePlot.getRenderer();
		
		summaryBarRenderer.setBarPainter(new StandardBarPainter());
		summaryAPIBarRenderer.setBarPainter(new StandardBarPainter());
		traceRenderer.setBarPainter(new StandardBarPainter());
		
		summaryBarRenderer.setItemMargin(0.05);
		summaryBarRenderer.setDrawBarOutline(false);
		summaryAPIBarRenderer.setItemMargin(0.05);
		summaryAPIBarRenderer.setDrawBarOutline(false);
		
		
		traceRenderer.setSeriesPaint(0, new Color(153,0,0));

		NumberAxis traceAxis = new NumberAxis("Time (ns)");
		traceAxis.setAutoRangeIncludesZero(false);
		
		tracePlot.setRangeAxis(traceAxis);
		/* Configure end */
		
		summaryPieChartPanel = new ChartPanel(summaryPieChart);
		summaryBarChartPanel = new ChartPanel(summaryBarChart);
		summaryAPIPieChartPanel = new ChartPanel(summaryAPIPieChart);
		summaryAPIBarChartPanel = new ChartPanel(summaryAPIBarChart);
		traceChartPanel = new ChartPanel(traceChart);
		
		summaryPanel.add(summaryPieChartPanel);
		summaryPanel.add(summaryBarChartPanel);
		
		summaryAPIPanel.add(summaryAPIPieChartPanel);
		summaryAPIPanel.add(summaryAPIBarChartPanel);
		
		tabbedGraphPane.addTab("Summary", null, summaryPanel, "Graph for summary.csv");
		tabbedGraphPane.addTab("API Calls", null, summaryAPIPanel, "Graph for API call summary.csv");
		tabbedGraphPane.addTab("CPU/GPU Trace", null, traceChartPanel, "Graph for trace.csv");
	}

    @SuppressWarnings("null")
	private void actioncomparecriticmetric() //Critic Compare
    {
		JPanel summarycriticPanel = new JPanel();
		
		// create the dataset
		summarycriticPanel.setLayout(new GridLayout(2,3));
		
		//measure the number of kernel of GPU
		int numkernel=0; 
		for (int i=2; i<aryMetrics.size(); i++) if(aryMetrics.get(i)[4].equals("Issued IPC")) numkernel++;
		
		double tmpissuedipc[] = new double[numkernel], tmpinstructionsissued[]= new double[numkernel];
		double tmpexecutedipc[] = new double[numkernel], tmpinstructionsexecuted[] = new double[numkernel];		
		int tmpissuedipcindex=0,tmpexecutedipcindex=0,tmpinstructionsissuedindex=0,tmpinstructionsexecutedindex=0;
		double totalipc=0, totalcycle=0; 

		double tmpisseslotutilization[] = new double[numkernel];
		int tmpisseslotutilizationindex =0;
		double tmpmutliprocessoractivity[] = new double[numkernel];
		int tmpmutliprocessoractivityindex =0;
		double tmpachievedoccupancy[] = new double[numkernel];
		int tmpachievedoccupancyindex =0;
		double tmpwarpexecutionefficiency[] = new double[numkernel];
		int tmpwarpexecutionefficiencyindex =0;
		
		String[] list = {"Issued IPC","Executed IPC","Warp Execution Efficiency","Multiprocessor Activity","Issue Slot Utilization","Achieved Occupancy"};
		
		int listindex=0;
		int numGPU =2; //have to change!!!!
		while(listindex<6)
		{
			DefaultCategoryDataset summaryCompareBarData = new DefaultCategoryDataset();
			JFreeChart summaryCompareBarChart = null;
			ChartPanel summaryCompareBarChartPanel = null;
			
			if(listindex==0) 
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					
					if(aryMetrics.get(i)[4].equals("Instructions Issued"))
					{
						tmpinstructionsissued[tmpinstructionsissuedindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpinstructionsissuedindex++;
					}
					if(aryMetrics.get(i)[4].equals("Issued IPC"))
					{
						tmpissuedipc[tmpissuedipcindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpissuedipcindex++;
						//execute average
					}
					if(tmpissuedipcindex==numkernel/numGPU && tmpinstructionsissuedindex==numkernel/numGPU)
					{
						//profiler profiles GPU step by step
						tmpinstructionsissuedindex=tmpissuedipcindex=0;
						totalipc=0;
						totalcycle=0;
						
						for(int j=0; j<numkernel/numGPU; j++) 
						{
							totalipc+=tmpinstructionsissued[j];
							totalcycle +=tmpinstructionsissued[j]/tmpissuedipc[j];
						}
						totalipc/=totalcycle;
						summaryCompareBarData.addValue(totalipc, "Instructions Issued", aryMetrics.get(i)[0]);
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device", "IPC", 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);
			}
			else if(listindex==1)
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					if(aryMetrics.get(i)[4].equals("Instructions Executed"))
					{
						tmpinstructionsexecuted[tmpinstructionsexecutedindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpinstructionsexecutedindex++;
					}
					if(aryMetrics.get(i)[4].equals("Executed IPC"))
					{
						tmpexecutedipc[tmpexecutedipcindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpexecutedipcindex++;
						//execute average
					}
					if(tmpexecutedipcindex==numkernel/numGPU && tmpinstructionsexecutedindex==numkernel/numGPU)
					{
						//profiler profiles GPU step by step
						tmpinstructionsexecutedindex=tmpexecutedipcindex=0;
						totalipc=0;
						totalcycle=0;
						
						for(int j=0; j<numkernel/numGPU; j++) 
						{
							totalipc+=tmpinstructionsexecuted[j];
							totalcycle +=tmpinstructionsexecuted[j]/tmpexecutedipc[j];
						}
						totalipc/=totalcycle;
						summaryCompareBarData.addValue(totalipc, "Instructions Executued", aryMetrics.get(i)[0]);
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device","IPC", 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);
			}
			else if(listindex ==2)
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					if(aryMetrics.get(i)[4].equals("Warp Execution Efficiency"))
					{
						aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("%")[0];
						tmpwarpexecutionefficiency[tmpwarpexecutionefficiencyindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpwarpexecutionefficiencyindex++;
					}	
					if(tmpwarpexecutionefficiencyindex==numkernel/numGPU)
					{
						tmpwarpexecutionefficiencyindex=0;
						double total =0;
						for(int j=0; j<numkernel/numGPU; j++) total +=tmpwarpexecutionefficiency[j] / (numkernel/numGPU); //average
						summaryCompareBarData.addValue(total, "Warp Execution Efficiency", aryMetrics.get(i)[0]);
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device", "Percentage (%)", 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);
				
			}
			
			else if(listindex ==3)
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					if(aryMetrics.get(i)[4].equals("Multiprocessor Activity"))
					{
						aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("%")[0];
						tmpmutliprocessoractivity[tmpmutliprocessoractivityindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpmutliprocessoractivityindex++;
					}	
					if(tmpmutliprocessoractivityindex==numkernel/numGPU)
					{
						tmpmutliprocessoractivityindex=0;
						double total =0;
						for(int j=0; j<numkernel/numGPU; j++) total +=tmpmutliprocessoractivity[j]; //sum
						summaryCompareBarData.addValue(total, "Multiprocessor Activity", aryMetrics.get(i)[0]);
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device", "Percentage (%)", 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);
				
			}
	
			else if(listindex ==4)
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					if(aryMetrics.get(i)[4].equals("Issue Slot Utilization"))
					{
						aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("%")[0];
						tmpisseslotutilization[tmpisseslotutilizationindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpisseslotutilizationindex++;
						//summaryCompareBarData.addValue(Double.parseDouble(aryMetrics.get(i)[7]), aryMetrics.get(i)[1], aryMetrics.get(i)[0]);
					}	
					if(tmpisseslotutilizationindex==numkernel/numGPU)
					{
						tmpisseslotutilizationindex=0;
						double total =0;
						for(int j=0; j<numkernel/numGPU; j++) total +=tmpisseslotutilization[j]; //sum
						summaryCompareBarData.addValue(total, "Issue Slot Utilization", aryMetrics.get(i)[0]);
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device", null, 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);	
			}
			else if(listindex ==5)
			{
				for (int i=2; i<aryMetrics.size(); i++) {
					if(aryMetrics.get(i)[4].equals("Achieved Occupancy"))
					{
						aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("%")[0];
						tmpachievedoccupancy[tmpachievedoccupancyindex]=Double.parseDouble(aryMetrics.get(i)[7]);
						tmpachievedoccupancyindex++;
					}	
					if(tmpachievedoccupancyindex==numkernel/numGPU)
					{
						tmpachievedoccupancyindex=0;
						double total =0;
						for(int j=0; j<numkernel/numGPU; j++) total +=tmpachievedoccupancy[j] / (numkernel/numGPU); //average
						summaryCompareBarData.addValue(total, "Achieved Occupancy", aryMetrics.get(i)[0]);
						
					}
				}
				summaryCompareBarChart = ChartFactory.createBarChart(list[listindex], "Device", "Occupancy (%)", 
						summaryCompareBarData, PlotOrientation.VERTICAL, false, true, false);
				
				
			}
			// set the background color for the chart
			summaryCompareBarChart.setBackgroundPaint(Color.WHITE);
			
			
			// get a reference to the plot for further customisation...
			summaryCompareBarChart.setBorderVisible(true);
			CategoryPlot summaryCompareBarPlot = summaryCompareBarChart.getCategoryPlot();	
			summaryCompareBarPlot.setBackgroundPaint(Color.WHITE);	
			summaryCompareBarPlot.setRangeGridlinePaint(Color.GRAY);
			
			// set the range axis 
			CategoryAxis summaryCompareBarAxis = summaryCompareBarPlot.getDomainAxis();
			
			summaryCompareBarAxis.setLowerMargin(0.02);
			summaryCompareBarAxis.setUpperMargin(0.02);
			summaryCompareBarAxis.setCategoryMargin(0.1);
			
			// disable bar outline
			BarRenderer summaryCompareBarRenderer = (BarRenderer) summaryCompareBarPlot.getRenderer();

			summaryCompareBarRenderer.setBarPainter(new StandardBarPainter());
			summaryCompareBarRenderer.setItemMargin(0.05);
			summaryCompareBarRenderer.setDrawBarOutline(false);
			summaryCompareBarRenderer.setMaximumBarWidth(0.1); // set the width of bar chart
     
			summaryCompareBarChartPanel = new ChartPanel(summaryCompareBarChart);			
			
			summarycriticPanel.add(summaryCompareBarChartPanel);
			
			listindex++;
		}	
		tabbedGraphPane.addTab("Critic Performance", null, summarycriticPanel, BorderLayout.CENTER);
    }

    private void actionstalllistSummary() {
		tabbedGraphPane.addTab("Issue Stall Reason Performance", null,summarystallPanel,BorderLayout.CENTER);
		summarystallPanel.setLayout(new BorderLayout());
		summarystallPanel.add(tabbedcomparestallGraphPane,BorderLayout.CENTER); 
		tabbedcomparestallGraphPane.setLayout(new BorderLayout());
		
		ArrayList<String> metricstallList;
		JComboBox<String> metricstallSelect;
		metricstallList = new ArrayList<String>();
				
		for (int i=1; i<aryMetrics.size(); i++) 
		{
			if (aryMetrics.get(i)[4].contains("Issue Stall")&&!aryMetrics.get(i)[4].contains("Utilization")) metricstallList.add(aryMetrics.get(i)[4]);
		}
		
		metricstallList = new ArrayList<String>(new HashSet<String>(metricstallList));
		metricstallSelect = new JComboBox<String>();
		

		for(int i=0; i<metricstallList.size(); i++) metricstallSelect.addItem(metricstallList.get(i));

		summarystallPanel.add(metricstallSelect, BorderLayout.PAGE_START);
		
		actioncomparemetric("Issue Stall Reasons (Texture)","Issue Stall Reason"); // show initial slide
		
		metricstallSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedcomparestallGraphPane.removeAll();
				actioncomparemetric((String) metricstallSelect.getSelectedItem(),"Issue Stall Reason");
			}
		});
	}

    private void actionL2listSummary() {
		tabbedGraphPane.addTab("L2 Cache Performance", null,summaryL2Panel,BorderLayout.CENTER);
		summaryL2Panel.setLayout(new BorderLayout());
		summaryL2Panel.add(tabbedcompareL2GraphPane,BorderLayout.CENTER); 
		tabbedcompareL2GraphPane.setLayout(new BorderLayout());
		
		ArrayList<String> metricstallList;
		JComboBox<String> metricstallSelect;
		metricstallList = new ArrayList<String>();
				
		for (int i=1; i<aryMetrics.size(); i++) 
		{
			if (aryMetrics.get(i)[4].contains("L2")&&!aryMetrics.get(i)[4].contains("Utilization")) metricstallList.add(aryMetrics.get(i)[4]);
		}

		metricstallList = new ArrayList<String>(new HashSet<String>(metricstallList));
		metricstallSelect = new JComboBox<String>();
		
		for(int i=0; i<metricstallList.size(); i++) metricstallSelect.addItem(metricstallList.get(i));

		summaryL2Panel.add(metricstallSelect, BorderLayout.PAGE_START);
		
		actioncomparemetric("L2 Throughput (Texture Writes)","L2 Cache");
		
		metricstallSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedcompareL2GraphPane.removeAll();
				actioncomparemetric((String) metricstallSelect.getSelectedItem(),"L2 Cache");
			}
		});
	}
	
    private void actiongloballistSummary() {
		tabbedGraphPane.addTab("Global Memory Performance", null,summaryglobalPanel,BorderLayout.CENTER);
		summaryglobalPanel.setLayout(new BorderLayout());
		summaryglobalPanel.add(tabbedcompareglobalGraphPane,BorderLayout.CENTER); 
		tabbedcompareglobalGraphPane.setLayout(new BorderLayout());
		
		ArrayList<String> metricstallList;
		JComboBox<String> metricstallSelect;
		metricstallList = new ArrayList<String>();
				
		for (int i=1; i<aryMetrics.size(); i++) 
		{
			if (aryMetrics.get(i)[4].contains("Global")&&!aryMetrics.get(i)[4].contains("Utilization")) metricstallList.add(aryMetrics.get(i)[4]);
		}

		metricstallList = new ArrayList<String>(new HashSet<String>(metricstallList));
		metricstallSelect = new JComboBox<String>();
		
		for(int i=0; i<metricstallList.size(); i++) metricstallSelect.addItem(metricstallList.get(i));

		summaryglobalPanel.add(metricstallSelect, BorderLayout.PAGE_START);
		
		actioncomparemetric("Requested Global Store Throughput","Global Memory");
		
		metricstallSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedcompareglobalGraphPane.removeAll();
				actioncomparemetric((String) metricstallSelect.getSelectedItem(),"Global Memory");
			}
		});
	}
	
	private void actionsharedmemorylistSummary() {
		tabbedGraphPane.addTab("Shared Memory Performance", null,summarysharedmemoryPanel,BorderLayout.CENTER);
		summarysharedmemoryPanel.setLayout(new BorderLayout());
		summarysharedmemoryPanel.add(tabbedcomparesharedmemoryGraphPane,BorderLayout.CENTER); 
		tabbedcomparesharedmemoryGraphPane.setLayout(new BorderLayout());
		
		ArrayList<String> metricstallList;
		JComboBox<String> metricstallSelect;
		metricstallList = new ArrayList<String>();
				
		for (int i=1; i<aryMetrics.size(); i++) 
		{
			if (aryMetrics.get(i)[4].contains("Shared Memory")&&!aryMetrics.get(i)[4].contains("Utilization")) metricstallList.add(aryMetrics.get(i)[4]);
		}

		metricstallList = new ArrayList<String>(new HashSet<String>(metricstallList));
		metricstallSelect = new JComboBox<String>();
		
		for(int i=0; i<metricstallList.size(); i++) metricstallSelect.addItem(metricstallList.get(i));

		summarysharedmemoryPanel.add(metricstallSelect, BorderLayout.PAGE_START);
		
		actioncomparemetric(metricstallList.get(1),"Shared Memory");
		
		metricstallSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedcomparesharedmemoryGraphPane.removeAll();
				actioncomparemetric((String) metricstallSelect.getSelectedItem(),"Shared Memory");
			}
		});
	}
	
	private void actionmetricSummary() { // etc
		tabbedGraphPane.addTab("The Others Performance", null,summarymetricPanel,BorderLayout.CENTER);
		summarymetricPanel.setLayout(new BorderLayout());
		summarymetricPanel.add(tabbedcompareGraphPane,BorderLayout.CENTER); 
		tabbedcompareGraphPane.setLayout(new BorderLayout());
		
		ArrayList<String> metricList;
		JComboBox<String> metricSelect;
		metricList = new ArrayList<String>();
				
		for (int i=1; i<aryMetrics.size(); i++) 
		{
			if (!aryMetrics.get(i)[4].contains("Utilization") 
					&& !aryMetrics.get(i)[4].contains("Issue Stall")
					&& !aryMetrics.get(i)[4].contains("L2")
					&& !aryMetrics.get(i)[4].contains("Global")
					&& !aryMetrics.get(i)[4].contains("Shared Memory")) metricList.add(aryMetrics.get(i)[4]);
		}

		metricList = new ArrayList<String>(new HashSet<String>(metricList));
		metricSelect = new JComboBox<String>();
		
		for(int i=0; i<metricList.size(); i++) metricSelect.addItem(metricList.get(i));

		summarymetricPanel.add(metricSelect, BorderLayout.PAGE_START);
		
		actioncomparemetric("Instructions Executed","default");
		
		metricSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tabbedcompareGraphPane.removeAll();
				actioncomparemetric((String) metricSelect.getSelectedItem(),"etc");
			}
		});
		
	}
	
	private void actioncomparemetric(String selectmetric, String panel)
	{
		JPanel summaryComparePanel = new JPanel();
		DefaultCategoryDataset summaryCompareBarData = new DefaultCategoryDataset();
		JFreeChart summaryCompareBarChart = null;
		ChartPanel summaryCompareBarChartPanel = null;

		// create the dataset

		for (int i=2; i<aryMetrics.size(); i++) {
			if(aryMetrics.get(i)[4].equals(selectmetric))
			{
				// Considering specific output	
				if(aryMetrics.get(i)[7].contains("GB/s")) aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("G")[0];
				else if(aryMetrics.get(i)[7].contains("MB/s")) aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("M")[0];
				else if(aryMetrics.get(i)[7].contains("B/s")) aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("B")[0];
				else if(aryMetrics.get(i)[7].contains("%")) aryMetrics.get(i)[7] = aryMetrics.get(i)[7].split("%")[0];
				summaryCompareBarData.addValue(Double.parseDouble(aryMetrics.get(i)[7]), aryMetrics.get(i)[1], aryMetrics.get(i)[0]);
			}
		}

		// create the chart
		switch(panel) {
			case("Issue Stall Reason"): 
				summaryCompareBarChart = ChartFactory.createBarChart(selectmetric, "Device", "percentage (%)", 
						summaryCompareBarData, PlotOrientation.VERTICAL, true, true, false);
				break;
			case("L2 Cache"):
				summaryCompareBarChart = ChartFactory.createBarChart(selectmetric, "Device", "", 
						summaryCompareBarData, PlotOrientation.VERTICAL, true, true, false);
				break;
			case("Global Memory"):
				summaryCompareBarChart = ChartFactory.createBarChart(selectmetric, "Device", "",
						summaryCompareBarData, PlotOrientation.VERTICAL, true, true, false);
				break;
			case("Shared Memory"):
				summaryCompareBarChart = ChartFactory.createBarChart(selectmetric, "Device", "", 
						summaryCompareBarData, PlotOrientation.VERTICAL, true, true, false);
			default://etc
				summaryCompareBarChart = ChartFactory.createBarChart(selectmetric, "Device", "", 
						summaryCompareBarData, PlotOrientation.VERTICAL, true, true, false);
				break;
		}
		// set the background color for the chart
		summaryCompareBarChart.setBackgroundPaint(Color.WHITE);
		
		// get a reference to the plot for further customisation...
		summaryCompareBarChart.setBorderVisible(true);
		CategoryPlot summaryCompareBarPlot = summaryCompareBarChart.getCategoryPlot();	
		summaryCompareBarPlot.setBackgroundPaint(Color.WHITE);	
		summaryCompareBarPlot.setRangeGridlinePaint(Color.GRAY);
		
		// set the range axis 
		CategoryAxis summaryCompareBarAxis = summaryCompareBarPlot.getDomainAxis();
		
		summaryCompareBarAxis.setLowerMargin(0.02);
		summaryCompareBarAxis.setUpperMargin(0.02);
		summaryCompareBarAxis.setCategoryMargin(0.1);

		// disable bar outline
		BarRenderer summaryCompareBarRenderer = (BarRenderer) summaryCompareBarPlot.getRenderer();

		summaryCompareBarRenderer.setBarPainter(new StandardBarPainter());
		summaryCompareBarRenderer.setItemMargin(0.05);
		summaryCompareBarRenderer.setDrawBarOutline(false);
		
		summaryCompareBarChartPanel = new ChartPanel(summaryCompareBarChart);
		
		summaryComparePanel.setLayout(new BorderLayout());
		summaryComparePanel.add(summaryCompareBarChartPanel);

		switch(panel) {
			case("Issue Stall Reason"):
				tabbedcomparestallGraphPane.addTab("L2 Cache", null, summaryComparePanel, "Graph for analyzing L2 cache.csv");
				break;
			case("L2 Cache"):
				tabbedcompareL2GraphPane.addTab("L2 Cache", null, summaryComparePanel, "Graph for analyzing L2 cache.csv");
				break;
			case("Global Memory"):
				tabbedcompareglobalGraphPane.addTab("Global Memory", null, summaryComparePanel, "Graph for analyzing global memory.csv");
				break;
			case("Shared Memory"):
				tabbedcomparesharedmemoryGraphPane.addTab("Global Memory", null, summaryComparePanel, "Graph for analyzing global memory.csv");
				break;
			default:
				tabbedcompareGraphPane.addTab("etc", null, summaryComparePanel, "Graph for analyzing the others");
				break;
		}
	}

	
private void actionOpen() {
		
		codeFileLoad.setLocation(parentFrame.getLocation());
		codeFileLoad.setVisible(true);

		syntaxedCodeArea.setEditable(false);
				
		pathCodeDirectory = codeFileLoad.getDirectory();
		pathCodeFile = codeFileLoad.getFile();
		
		if(pathCodeFile != null) {
			
			/* Get extension of selected file */
			String ext = "";
			int idx = pathCodeFile.lastIndexOf('.');
			if(idx > 0) {
				ext = pathCodeFile.substring(idx+1);
			}
		
			/* Only CUDA file is accepted */
			if(!ext.equalsIgnoreCase("cu")) 
			{
				JOptionPane.showMessageDialog(parentFrame,
						pathCodeFile + " is not a CUDA file.\n"
						+ "Only '.cu' file (CUDA extension) is supported.\n", 
						"Warning", JOptionPane.WARNING_MESSAGE);
				
				codeFileLoad.setFile(null);
				return;
			}
		}
		
		if(pathCodeFile != null) {
			
			FileReader fr = null;
			BufferedReader br = null;
			
			syntaxedCodeArea.setText(null);
			syntaxedCodeArea.setContentType("text/cpp");
			
			String syntaxedTextAll = new String();
			
			
			
			
			try {
				line =0;
				fr = new FileReader(pathCodeDirectory + pathCodeFile);
				br = new BufferedReader(fr);
				String string = new String();
				
				do {
					string = br.readLine();
			
					if(string != null) {
						getline.add(string);
						syntaxedTextAll += string + "\n";
					}
					line ++;
				} while(string != null);
			}
			catch (Exception fileReadError) {
				System.out.println("Error while opening file" + fileReadError);
			}
			finally {
				try {
					br.close();
				}
				catch (Exception fileCloseError) {
					System.out.println("Error while closing file" + fileCloseError);
				}
			}		
			
			syntaxedCodeArea.setText(syntaxedTextAll);
			syntaxedCodeArea.setCaretPosition(0);
			
			tabbedCodePane.addTab(pathCodeFile, codeScrollPane);
		}
	}
	
	private void actionComp() {
		
		String additionalCompOption = null;
		
		String[] aryAddCompOption = null;
		String[] cmdCompile = null;
		String[] cmdPTX = null;
		String[] cmdLineInfo = null;
		
		boolean noAddCompOption = false;
		
		if (pathCodeFile == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target CUDA source file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		// update nvcc path
		pathNvcc = projectProp.getProperty("pathNvcc");
		
		if(projectName == null)
			projectName = JOptionPane.showInputDialog(parentFrame, "Set project name", "Project Name",
					JOptionPane.QUESTION_MESSAGE);
		
		if(projectName == null)
			return;
		
		parentFrame.setTitle("HPCVisualizer - GPU Analyzer - " + projectName);
		 
		additionalCompOption = JOptionPane.showInputDialog(parentFrame, 
				"Additional Compile Options.\n"
				+ "Press OK to proceed.", 
				"Compile", JOptionPane.QUESTION_MESSAGE);
				
		// return on cancel
		if(additionalCompOption == null)
			return;
		
		System.out.println("Project Name: " + projectName);
		
		pathCode = codeFileLoad.getDirectory() + codeFileLoad.getFile();
		pathBin = pathRoot + "/projects/" + projectName + "/" + codeFileLoad.getFile() + ".out";
		pathPTX = pathRoot + "/projects/" + projectName + "/" + codeFileLoad.getFile() + ".ptx";
		pathLineInfo = pathRoot + "/projects/" + projectName + "/" + codeFileLoad.getFile() + ".lnf";
		
		aryAddCompOption = additionalCompOption.split("\\s+");
		
		if(aryAddCompOption.length==1 && aryAddCompOption[0].equals("")) {
			noAddCompOption = true;
		}
		
		String[] cmdMKDIR = {
				"mkdir",
				"projects",
				pathRoot + "/projects/" + projectName
		};		
		String[] cmdCompileOri = {
				pathNvcc,
				pathCode,
				"--output-file",
				pathBin
		};
		String[] cmdPTXOri = {
				pathNvcc,
				"--ptx",
				pathCode,
				"--output-file",
				pathPTX
		};
		String[] cmdLineInfoOri = {
				pathNvcc,
				"--ptx",
				"-lineinfo",
				"--source-in-ptx",
				pathCode,
				"--output-file",
				pathLineInfo
		};
		
		if (!noAddCompOption) {
			// if there are additional compile options 
			cmdCompile = new String[cmdCompileOri.length + aryAddCompOption.length];
			cmdPTX = new String[cmdPTXOri.length + aryAddCompOption.length];
			cmdLineInfo = new String[cmdLineInfoOri.length + aryAddCompOption.length];
		
			System.arraycopy(cmdCompileOri, 0, cmdCompile, 0, cmdCompileOri.length);
			System.arraycopy(cmdPTXOri, 0, cmdPTX, 0, cmdPTXOri.length);
			System.arraycopy(cmdLineInfoOri, 0, cmdLineInfo, 0, cmdLineInfoOri.length);
		
			System.arraycopy(aryAddCompOption, 0, cmdCompile, cmdCompileOri.length, aryAddCompOption.length);
			System.arraycopy(aryAddCompOption, 0, cmdPTX, cmdPTXOri.length, aryAddCompOption.length);
			System.arraycopy(aryAddCompOption, 0, cmdLineInfo, cmdLineInfoOri.length, aryAddCompOption.length);
		}
		else {
			// if there are no additional compile options
			cmdCompile = cmdCompileOri;
			cmdPTX = cmdPTXOri;
			cmdLineInfo = cmdLineInfoOri;
		}
		/*
		for(int i=0; i<cmdCompile.length; i++) {
			System.out.println(cmdCompile[i]);
		}
		*/
		try {
			Process p = Runtime.getRuntime().exec(cmdMKDIR);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmdCompile);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmdPTX);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmdLineInfo);
			p.waitFor();
			
			JOptionPane.showMessageDialog(parentFrame, 
					"Compilation end.\n"
					+ "Output binary: " + pathBin,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Compilation end");
		} 
		catch (IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private void actionExec() {
		
		String additionalExecArguments = null;
		
		String[] aryAddExecArguments = null;
		String[] cmdExport = null;
		String[] cmdSummary = null;
		String[] cmdTrace = null;
		//String[] cmdEvents = null;
		//String[] cmdMetrics = null;
		String[] cmdEventsMetrics = null;
		
		boolean noAddExecOption = false;
		
		if (pathBin == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target binary file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		// update nvprof path
		pathNvprof = projectProp.getProperty("pathNvprof");
		
		additionalExecArguments = JOptionPane.showInputDialog(parentFrame, 
				"Additional Execution Arguments.\n"
				+ "Press OK to proceed.", 
				"Execution", JOptionPane.QUESTION_MESSAGE);
				
		// return on cancel
		if(additionalExecArguments == null)
			return;
		
		aryAddExecArguments = additionalExecArguments.split("\\s+");
		
		if(aryAddExecArguments.length==1 && aryAddExecArguments[0].equals("")) {
			noAddExecOption = true;
		}
		
		pathExport = pathBin + "_export";
		
		String[] cmdExportOri = {
				pathNvprof,
				"--unified-memory-profiling", "off", //add for working on GPU3 server 
				"--force-overwrite",
				"--export-profile",
				pathExport,
				pathBin
		};
		String[] cmdSummaryOri = {
				pathNvprof,
				"--import-profile",
				pathExport,
				"--csv",
				"--log-file",
				pathBin + "_summary.csv"//,
				//pathBin
		};
		String[] cmdTraceOri = {
				pathNvprof,
				"--import-profile",
				pathExport,
				"--print-gpu-trace",
				"--print-api-trace",
				"--csv",
				"--log-file",
				pathBin + "_trace.csv"//,
				//pathBin
		};
		String[] cmdEventsMetricsOri = {
				pathNvprof,
				"--events",
				"all",
				"--metrics",
				"all",
				"--csv",
				"--log-file",
				pathBin + "_eventsmetrics.csv",
				pathBin
		};
		
		if (!noAddExecOption) {
			cmdExport = new String[cmdExportOri.length + aryAddExecArguments.length];
			cmdEventsMetrics = new String[cmdEventsMetricsOri.length + aryAddExecArguments.length];
		
			System.arraycopy(cmdExportOri, 0, cmdExport, 0, cmdExportOri.length);
			System.arraycopy(cmdEventsMetricsOri, 0, cmdEventsMetrics, 0, cmdEventsMetricsOri.length);
			System.arraycopy(aryAddExecArguments, 0, cmdExport, cmdExportOri.length, aryAddExecArguments.length);
			System.arraycopy(aryAddExecArguments, 0, cmdEventsMetrics, cmdEventsMetricsOri.length, aryAddExecArguments.length);
			
			cmdSummary = cmdSummaryOri;
			cmdTrace = cmdTraceOri;
		}
		else {
			
			cmdExport = cmdExportOri;
			cmdSummary = cmdSummaryOri;
			cmdTrace = cmdTraceOri;
			cmdEventsMetrics = cmdEventsMetricsOri;
		}

		
		try {

			// return on cancel
			if (JOptionPane.showConfirmDialog(parentFrame, 
					"Executing NVPROF and draw graphs & tables...\n"
					+ "This work might takes a long time.\n"
					+ "Press OK button to proceed.", 
					"Info.", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE)
					== JOptionPane.CANCEL_OPTION) {
				return;
			}
			
			/* Serializing NVPROF execution */
			long startTime = System.nanoTime();
			Process p = Runtime.getRuntime().exec(cmdExport);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			long endTime = System.nanoTime();
			cmdExportElapsedTime = endTime-startTime;
			
			p = Runtime.getRuntime().exec(cmdSummary);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			
			p = Runtime.getRuntime().exec(cmdTrace);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			
			startTime = System.nanoTime();
			p = Runtime.getRuntime().exec(cmdEventsMetrics);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			endTime = System.nanoTime();
			cmdEventsMetricsElapsedTime = endTime - startTime;

			//////////////////////////////////////////////////////
			startTime = System.nanoTime();
			readCSV(pathBin+"_summary.csv");
			readCSV(pathBin+"_trace.csv");
			readCSV(pathBin+"_eventsmetrics.csv");
			
			tabbedGraphPane.removeAll();
			
			drawGraphs();
			
			tabbedGraphPane.addTab("GPU Diagram", null, 
					new GPUDiagramPanel(aryEvents, aryMetrics), 
					"Diagram of Target GPU");
			
			drawTable();
			
			showAnalResult();
			
			JOptionPane.showMessageDialog(parentFrame, 
					"NVPROF execution and CSV file parsing end.\n"
					+ "Output CSV: " + pathRoot,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Execution end");
			
			endTime = System.nanoTime();
			cmdAdditionalElapsedTime = endTime - startTime;
			
			System.out.println("Export Time: " + cmdExportElapsedTime + " ns");
			System.out.println("Events/Metrics Time: " + cmdEventsMetricsElapsedTime + " ns");
			System.out.println("Additional Time: " + cmdAdditionalElapsedTime + " ns");
			System.out.println("\nAnalysis Overhead: " + cmdAdditionalElapsedTime/(float)cmdExportElapsedTime*100 + "%");
			System.out.println("Analysis Overhead(icl e/m): " + cmdAdditionalElapsedTime/(float)(cmdExportElapsedTime+cmdEventsMetricsElapsedTime)*100 + "%\n");
		} 
		catch (IOException | InterruptedException el) {
			// TODO Auto-generated catch block
			el.printStackTrace();
		}
	}
	
	private void actionPTX() {		
		if (pathCodeFile == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target CUDA source file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (pathPTX == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target CUDA source file is not compiled", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		ptxFrame = new JFrame("CUDA Source View");
		ptxFrame.setPreferredSize(new Dimension(1500, 800));
		ptxFrame.setSize(new Dimension(1500, 800));
		ptxFrame.setResizable(false);
		misc.alignWindow(ptxFrame);
		
		ptxPanel = new GPUPTXPanel(ptxFrame, pathCode, pathPTX, pathLineInfo);
		
		ptxFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ptxFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				
				ptxPanel.setVisible(false);
			}
		});
		ptxFrame.pack();
		ptxFrame.setVisible(true);
	}
	
	private void actionClean() {
		
		if(pathBin == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target binary file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		String[] cmd0 = {
				"rm",
				pathBin,
				pathPTX,
				pathBin+"_events.csv",
				pathBin+"_metrics.csv",
				pathBin+"_summary.csv",
				pathBin+"_trace.csv"
		};
		
		try {
			Process p = Runtime.getRuntime().exec(cmd0);
			p.waitFor();
		}
		catch (IOException | InterruptedException el) {
			// TODO Auto-generated catch block
			el.printStackTrace();
		}
		
		tabbedCodePane.removeAll();
		tabbedGraphPane.removeAll();
		resultShowPanel.removeAll();
		
		aryEvents.clear();
		aryMetrics.clear();
		arySummary.clear();
		arySummaryAPI.clear();
		aryTrace.clear();
		
		pathCodeDirectory = null;
		pathCodeFile = null;
		pathCode = null;
		pathBin = null;
		pathPTX = null;
		projectName = null;
		
		JOptionPane.showMessageDialog(parentFrame, "All files and results are deleted", "Info", JOptionPane.INFORMATION_MESSAGE);
		parentFrame.setTitle("HPCVisualizer - GPU Analyzer");
	}
	
	private void actionProp() {
		
		//GPUPropertiesPanel gpuProperPanel = null;
		JFrame gpuProperFrame = new JFrame("Properties");
		gpuProperFrame.setPreferredSize(new Dimension(600, 250));
		gpuProperFrame.setSize(new Dimension(600, 250));
		gpuProperFrame.setResizable(false);
		gpuProperFrame.setLocation(parentFrame.getLocation());
		
		//gpuProperPanel = new GPUPropertiesPanel(gpuProperFrame, projectProp);
		new GPUPropertiesPanel(gpuProperFrame, projectProp, pathConfig);
		
		gpuProperFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		gpuProperFrame.pack();
		gpuProperFrame.setVisible(true);
	}

	private void readCSV(String pathCSV) {
		
		FileReader fr = null;
		BufferedReader br = null;
		
		Pattern fileRex = null;
		Pattern csvRex0 = null;
		Pattern csvRex1 = null;
		Pattern csvRex2 = null;
		
		Matcher fileMat = null;
		Matcher csvMat0 = null;
		Matcher csvMat1 = null;
		Matcher csvMat2 = null;
		
		int aryidx0 = 0;
		int aryidx1 = 0;
		
		ArrayList<String[]> aryTemp = new ArrayList<String[]>();
		
		String csvType = "";
		int idx = pathCSV.lastIndexOf('_');
		if(idx > 0) {
			csvType = pathCSV.substring(idx+1);
		}		
		
		switch(csvType) {
		case "summary.csv"			: aryTemp = arySummary; break;
		case "trace.csv"			: aryTemp = aryTrace; break;
		//case "events.csv"	: aryTemp = aryEvents; break;
		//case "metrics.csv"	: aryTemp = aryMetrics; break;
		case "eventsmetrics.csv"	: aryTemp = aryEvents; break;
		default						: System.out.println("Wrong csvType: " + csvType); break;	
		}
		
		try {
			fr = new FileReader(pathCSV);
			br = new BufferedReader(fr);
			fileRex = Pattern.compile("^=(\\W|\\w)*");			// Start with '='
			csvRex0 = Pattern.compile("^\"(\\W|\\w)*\"$");		// Start with '"' end with '"' //ex: "*****"
			csvRex1 = Pattern.compile("^\"(\\W|\\w)*[^\"]$");	// Start with '"' end without '"' //ex: "*****[without "]
			csvRex2 = Pattern.compile("^[^\"](\\W|\\w)*\"$");	// Start without '"' end with '"' //ex: [without "]*****"
			int flag =0;
			
			String str = new String();
			String[] tmp;
			aryTemp.clear();

			do {
				str = br.readLine();
				if(str != null) {
					fileMat = fileRex.matcher(str); // remove unnecessary string such as "===Profiling result:" 
					
					/* Exception for arySummary, arySummaryAPI */
					if(str.isEmpty() && csvType.equals("summary.csv")) {
						arySummaryAPI.clear();
						aryTemp = arySummaryAPI;
						continue;
					}
					/* Exception for aryEvents, aryMetrics */
					if(str.isEmpty() && csvType.equals("eventsmetrics.csv")) {
						aryMetrics.clear();
						aryTemp = aryMetrics;
						continue;
					}
					
					if (!fileMat.matches()) {
						
						tmp = str.split(",", -1); // classify empty space to become array
						aryidx0 = 0;
						aryidx1 = 0; 
						
						for(int i=0; i<tmp.length; i++) {
							csvMat1 = csvRex1.matcher(tmp[i]);
							csvMat2 = csvRex2.matcher(tmp[i]);
							
							if(csvMat1.matches()) aryidx0 = i;
							if(csvMat2.matches()) aryidx1 = i;
						}
						
						if(aryidx0!=0 && aryidx1!=0) {
							for (int i=aryidx0+1; i<=aryidx1; i++)
								tmp[aryidx0] += tmp[i];
							
							for (int i=1; i<tmp.length-aryidx1; i++)
								tmp[aryidx0+i] = tmp[aryidx1+i];
						}
						
						for(int i=0; i<tmp.length; i++) {
							csvMat0 = csvRex0.matcher(tmp[i]);
							if(csvMat0.matches()) tmp[i] = tmp[i].replaceAll("\"", ""); //remove " 
							
						}
						
						/* Exception for aryTrace */
						if(csvType.equals("trace.csv")) 
						{
								String[] tmp2 = new String[tmp.length];
								
								if(flag >1) tmp2[0] = tmp[aryTemp.get(0).length-1]; // fix for problem of printing out kernel at trace table
								else tmp2[0] = tmp[tmp.length-1];

								for(int i=1; i<tmp.length; i++) tmp2[i] = tmp[i-1];
								tmp = tmp2;
								
								String[] tmp3;
								if (tmp[0].length() > 20) // if kernel
								{
									tmp3 = tmp[0].split("\\s",-1);
									tmp[0] = "";
									for(int j=0; j<tmp3.length-1; j++) tmp[0] += tmp3[j] +" ";
								}
						}
						aryTemp.add(tmp);
					}
				}
				flag++;
			} while(str != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
	}
	
	private void drawGraphs() {
		
		JPanel summaryPanel = new JPanel();
		JPanel summaryAPIPanel = new JPanel();

//		IntervalCategoryDataset traceData = new TaskSeriesCollection();
		TaskSeriesCollection traceCollection = new TaskSeriesCollection();
		TaskSeries traceTS = new TaskSeries("Duration");
		
		DefaultPieDataset summaryPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryBarData = new DefaultCategoryDataset();
		
		DefaultPieDataset summaryAPIPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryAPIBarData = new DefaultCategoryDataset();
		
		JFreeChart summaryPieChart = null;
		JFreeChart summaryBarChart = null;
		JFreeChart summaryAPIPieChart = null;
		JFreeChart summaryAPIBarChart = null;
		JFreeChart traceChart = null;
		
		ChartPanel summaryPieChartPanel = null;
		ChartPanel summaryBarChartPanel = null;
		ChartPanel summaryAPIPieChartPanel = null;
		ChartPanel summaryAPIBarChartPanel = null;
		ChartPanel traceChartPanel = null;
		
		ArrayList<String> traceTaskList = new ArrayList<String>();
		
		summaryPanel.setLayout(new GridLayout(1,2));
		summaryAPIPanel.setLayout(new GridLayout(1,2));
		
		/* Draw gantt chart */
		long axisStart = (long) (Double.parseDouble(aryTrace.get(2)[1])*1000);
		long axisEnd = (long) (((Double.parseDouble(aryTrace.get(aryTrace.size()-1)[1]) + 
				(Double.parseDouble(aryTrace.get(aryTrace.size()-1)[2]))) + 1)*1000);
		for (int i=2; i<aryTrace.size(); i++) {

			traceTaskList.add(aryTrace.get(i)[0]);
		}
		
		traceTaskList = new ArrayList<String>(new HashSet<String>(traceTaskList));
		
		for (int i=0; i<traceTaskList.size(); i++) {
			
			traceTS.add(new Task(traceTaskList.get(i), new SimpleTimePeriod(axisStart, axisEnd)));
		}
		
		for (int i=2; i<aryTrace.size(); i++) {
			
			long start_time = (long) (Double.parseDouble(aryTrace.get(i)[1])*1000);
			long end_time = start_time + (long) (Double.parseDouble(aryTrace.get(i)[2])*1000);
			int index = 0;
			if (start_time == end_time)
				end_time = start_time + 1;
			
			Task tempTask = new Task(aryTrace.get(i)[0], new SimpleTimePeriod(start_time, end_time));
			
			for (int j=0; j<traceTaskList.size(); j++) {
				
				if (traceTaskList.get(j).equals(aryTrace.get(i)[0])) {
					index = j;
					break;
				}
			}
			traceTS.get(index).addSubtask(tempTask);
		}
		
		traceCollection.add(traceTS);
//		traceData = traceCollection;
		
		/* Draw summary pie and bar chart */
		for (int i=2; i<arySummary.size(); i++) {
			
			summaryPieData.setValue(arySummary.get(i)[6], Double.parseDouble(arySummary.get(i)[0]));
			
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[3]), "Avg", arySummary.get(i)[6]);
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[4]), "Min", arySummary.get(i)[6]);
			summaryBarData.addValue(Double.parseDouble(arySummary.get(i)[5]), "Max", arySummary.get(i)[6]);
		}
		
		/* Draw summary API pie and bar chart */
		for (int i=2; i<arySummaryAPI.size(); i++) {
			
			summaryAPIPieData.setValue(arySummaryAPI.get(i)[6], Double.parseDouble(arySummaryAPI.get(i)[0]));
			
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[3]), "Avg", arySummaryAPI.get(i)[6]);
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[4]), "Min", arySummaryAPI.get(i)[6]);
			summaryAPIBarData.addValue(Double.parseDouble(arySummaryAPI.get(i)[5]), "Max", arySummaryAPI.get(i)[6]);
		}

		/* Create charts */
		summaryPieChart = ChartFactory.createPieChart("Kernel Execution Ratio", summaryPieData,
				true, true, false);
		summaryBarChart = ChartFactory.createBarChart("Kernel Execution Time", "Kernel", "Time (us)", 
				summaryBarData, PlotOrientation.HORIZONTAL, true, true, false);
		
		summaryAPIPieChart = ChartFactory.createPieChart("API Calls Ratio", summaryAPIPieData, 
				true, true, false);
		summaryAPIBarChart = ChartFactory.createBarChart("API Execution Time", "API Call", "Time (ms)", 
				summaryAPIBarData, PlotOrientation.HORIZONTAL, true, true, false);
		
		traceChart = ChartFactory.createGanttChart("CPU/GPU Trace", "Name", "Time (ns)", 
				traceCollection, false, false, false);
		
		/* Configure charts */
		summaryPieChart.setBackgroundPaint(Color.WHITE);
		summaryBarChart.setBackgroundPaint(Color.WHITE);
		summaryAPIPieChart.setBackgroundPaint(Color.WHITE);
		summaryAPIBarChart.setBackgroundPaint(Color.WHITE);
		traceChart.setBackgroundPaint(Color.WHITE);
		
		summaryPieChart.setBorderVisible(true);
		summaryBarChart.setBorderVisible(true);
		summaryAPIPieChart.setBorderVisible(true);
		summaryAPIBarChart.setBorderVisible(true);
		
		Plot summaryPiePlot = summaryPieChart.getPlot();
		CategoryPlot summaryBarPlot = summaryBarChart.getCategoryPlot();
		Plot summaryAPIPiePlot = summaryAPIPieChart.getPlot();
		CategoryPlot summaryAPIBarPlot = summaryAPIBarChart.getCategoryPlot();
		CategoryPlot tracePlot = traceChart.getCategoryPlot();
		
		summaryPiePlot.setBackgroundPaint(Color.WHITE);
		summaryBarPlot.setBackgroundPaint(Color.WHITE);
		summaryAPIPiePlot.setBackgroundPaint(Color.WHITE);
		summaryAPIBarPlot.setBackgroundPaint(Color.WHITE);
		tracePlot.setBackgroundPaint(Color.WHITE);
		
		summaryBarPlot.setRangeGridlinePaint(Color.GRAY);
		summaryAPIBarPlot.setRangeGridlinePaint(Color.GRAY);
		tracePlot.setRangeGridlinePaint(Color.GRAY);
		
		CategoryAxis summaryBarAxis = summaryBarPlot.getDomainAxis();
		CategoryAxis summaryAPIBarAxis = summaryAPIBarPlot.getDomainAxis();
		
		summaryBarAxis.setLowerMargin(0.02);
		summaryBarAxis.setUpperMargin(0.02);
		summaryBarAxis.setCategoryMargin(0.1);
		//summaryBarAxis.setCategoryLabelPositions(
			//	CategoryLabelPositions.createUpRotationLabelPositions(Math.PI/6.0));
		summaryAPIBarAxis.setLowerMargin(0.02);
		summaryAPIBarAxis.setUpperMargin(0.02);
		summaryAPIBarAxis.setCategoryMargin(0.1);
		//summaryAPIBarAxis.setCategoryLabelPositions(
			//	CategoryLabelPositions.createUpRotationLabelPositions(Math.PI/6.0));
		
		BarRenderer summaryBarRenderer = (BarRenderer) summaryBarPlot.getRenderer();
		BarRenderer summaryAPIBarRenderer = (BarRenderer) summaryAPIBarPlot.getRenderer();
		BarRenderer traceRenderer = (BarRenderer) tracePlot.getRenderer();
		
		summaryBarRenderer.setBarPainter(new StandardBarPainter());
		summaryAPIBarRenderer.setBarPainter(new StandardBarPainter());
		traceRenderer.setBarPainter(new StandardBarPainter());
		
		summaryBarRenderer.setItemMargin(0.05);
		summaryBarRenderer.setDrawBarOutline(false);
		summaryAPIBarRenderer.setItemMargin(0.05);
		summaryAPIBarRenderer.setDrawBarOutline(false);
		
		/*
		summaryBarRenderer.setSeriesPaint(0, new Color(0,0,153));
		summaryBarRenderer.setSeriesPaint(1, new Color(255,97,56));
		summaryBarRenderer.setSeriesPaint(2, new Color(174,238,0));
		
		summaryAPIBarRenderer.setSeriesPaint(0, new Color(0,0,153));
		summaryAPIBarRenderer.setSeriesPaint(1, new Color(255,97,56));
		summaryAPIBarRenderer.setSeriesPaint(2, new Color(174,238,0));
		*/
		traceRenderer.setSeriesPaint(0, new Color(153,0,0));
		/*
		traceRenderer.setBaseToolTipGenerator(
				(CategoryToolTipGenerator) new IntervalCategoryToolTipGenerator(
						"{1} Running Time: {3}ns ~ {4}ns", NumberFormat.getNumberInstance()));
		*/
		NumberAxis traceAxis = new NumberAxis("Time (ns)");
		traceAxis.setAutoRangeIncludesZero(false);
		
		tracePlot.setRangeAxis(traceAxis);
		/* Configure end */
		
		summaryPieChartPanel = new ChartPanel(summaryPieChart);
		summaryBarChartPanel = new ChartPanel(summaryBarChart);
		summaryAPIPieChartPanel = new ChartPanel(summaryAPIPieChart);
		summaryAPIBarChartPanel = new ChartPanel(summaryAPIBarChart);
		traceChartPanel = new ChartPanel(traceChart);
		
		summaryPanel.add(summaryPieChartPanel);
		summaryPanel.add(summaryBarChartPanel);
		
		summaryAPIPanel.add(summaryAPIPieChartPanel);
		summaryAPIPanel.add(summaryAPIBarChartPanel);
		
		tabbedGraphPane.addTab("Summary", null, summaryPanel, "Graph for summary.csv");
		tabbedGraphPane.addTab("API Calls", null, summaryAPIPanel, "Graph for API call summary.csv");
		tabbedGraphPane.addTab("CPU/GPU Trace", null, traceChartPanel, "Graph for trace.csv");
	}
	
	private void drawTable() {
		
		JPanel summaryTablePanel = new JPanel();
		JPanel summaryAPITablePanel = new JPanel();
		JPanel traceTablePanel = new JPanel();
		JPanel eventsTablePanel = new JPanel();
		JPanel metricsTablePanel = new JPanel();
		
		JScrollPane summaryTableScroll = new JScrollPane(summaryTablePanel);
		JScrollPane summaryAPITableScroll = new JScrollPane(summaryAPITablePanel);
		JScrollPane traceTableScroll = new JScrollPane(traceTablePanel);
		JScrollPane eventsTableScroll = new JScrollPane(eventsTablePanel);
		JScrollPane metricsTableScroll = new JScrollPane(metricsTablePanel);
		
		DefaultTableModel summaryTM = new DefaultTableModel();
		DefaultTableModel summaryAPITM = new DefaultTableModel();
		DefaultTableModel traceTM = new DefaultTableModel();
		DefaultTableModel eventsTM = new DefaultTableModel();
		DefaultTableModel metricsTM = new DefaultTableModel();
		
		JTable summaryTable = new JTable(summaryTM);
		JTable summaryAPITable = new JTable(summaryAPITM);
		JTable traceTable = new JTable(traceTM);
		JTable eventsTable = new JTable(eventsTM);
		JTable metricsTable = new JTable(metricsTM);
		
		/* Set Column names */
		Vector<String> summaryColumn = new Vector<String>();
		Vector<String> summaryAPIColumn = new Vector<String>();
		Vector<String> traceColumn = new Vector<String>();
		Vector<String> eventsColumn = new Vector<String>();
		Vector<String> metricsColumn = new Vector<String>();
		Vector<String> tempRow;
		
		for (int i=0; i<arySummary.get(0).length; i++) {
			summaryColumn.addElement(arySummary.get(0)[i]);
		}
		
		for (int i=0; i<arySummaryAPI.get(0).length; i++) {
			summaryAPIColumn.addElement(arySummaryAPI.get(0)[i]);
		}
		
		for (int i=0; i<aryTrace.get(0).length; i++) {
			traceColumn.addElement(aryTrace.get(0)[i]);
		}
		
		for (int i=0; i<aryEvents.get(0).length; i++) {
			eventsColumn.addElement(aryEvents.get(0)[i]);
		}
		
		for (int i=0; i<aryMetrics.get(0).length; i++) {
			metricsColumn.addElement(aryMetrics.get(0)[i]);
		}
		
		summaryTM = new DefaultTableModel(null, summaryColumn);
		summaryTable.setModel(summaryTM);
		summaryTable.setEnabled(false);
		
		summaryAPITM = new DefaultTableModel(null, summaryAPIColumn);
		summaryAPITable.setModel(summaryAPITM);
		summaryAPITable.setEnabled(false);
		
		traceTM = new DefaultTableModel(null, traceColumn);
		traceTable.setModel(traceTM);
		traceTable.setEnabled(false);
		
		eventsTM = new DefaultTableModel(null, eventsColumn);
		eventsTable.setModel(eventsTM);
		eventsTable.setEnabled(false);
		
		metricsTM = new DefaultTableModel(null, metricsColumn);
		metricsTable.setModel(metricsTM);
		metricsTable.setEnabled(false);
		
		/* Add data for each row */
		for (int i=1; i<arySummary.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummary.get(0).length; j++) {
				tempRow.addElement(arySummary.get(i)[j]);
			}
			summaryTM.addRow(tempRow);
		}
		
		for (int i=1; i<arySummaryAPI.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummaryAPI.get(0).length; j++) {
				tempRow.addElement(arySummaryAPI.get(i)[j]);
			}
			summaryAPITM.addRow(tempRow);
		}
		
		for (int i=1; i<aryTrace.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryTrace.get(0).length; j++) {
				tempRow.addElement(aryTrace.get(i)[j]);
			}
			traceTM.addRow(tempRow);
		}
		
		for (int i=1; i<aryEvents.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryEvents.get(0).length; j++) {
				tempRow.addElement(aryEvents.get(i)[j]);
			}
			eventsTM.addRow(tempRow);
		}
		
		for (int i=1; i<aryMetrics.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryMetrics.get(0).length; j++) {
				tempRow.addElement(aryMetrics.get(i)[j]);
			}
			metricsTM.addRow(tempRow);
		}
		
		/* Add table to panel */
		summaryTablePanel.setLayout(new BorderLayout());
		summaryAPITablePanel.setLayout(new BorderLayout());
		traceTablePanel.setLayout(new BorderLayout());
		eventsTablePanel.setLayout(new BorderLayout());
		metricsTablePanel.setLayout(new BorderLayout());
		
		summaryTablePanel.add(summaryTable.getTableHeader(), BorderLayout.PAGE_START);
		summaryTablePanel.add(summaryTable, BorderLayout.CENTER);
		
		summaryAPITablePanel.add(summaryAPITable.getTableHeader(), BorderLayout.PAGE_START);
		summaryAPITablePanel.add(summaryAPITable, BorderLayout.CENTER);
		
		traceTablePanel.add(traceTable.getTableHeader(), BorderLayout.PAGE_START);
		traceTablePanel.add(traceTable, BorderLayout.CENTER);
		
		eventsTablePanel.add(eventsTable.getTableHeader(), BorderLayout.PAGE_START);
		eventsTablePanel.add(eventsTable, BorderLayout.CENTER);
		
		metricsTablePanel.add(metricsTable.getTableHeader(), BorderLayout.PAGE_START);
		metricsTablePanel.add(metricsTable, BorderLayout.CENTER);
		
		summaryTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		summaryAPITableScroll.getVerticalScrollBar().setUnitIncrement(16);
		traceTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		eventsTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		metricsTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		tabbedGraphPane.addTab("Summary Table", null, summaryTableScroll, "Table for summary");
		tabbedGraphPane.addTab("API Calls Table", null, summaryAPITableScroll, "Table for API calls");
		tabbedGraphPane.addTab("Trace Table", null, traceTableScroll, "Table for CPU/GPU Trace");
		tabbedGraphPane.addTab("Events Table", null, eventsTableScroll, "Table for events");
		tabbedGraphPane.addTab("Metrics Table", null, metricsTableScroll, "Table for metrics");
	}
	
	private void showAnalResult() {
		tabbedShowPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
				
		/* Get the list of kernels and the number of kernels */
		ArrayList<String> kernelList = new ArrayList<String>();
		for (int i=1; i<aryMetrics.size(); i++) kernelList.add(aryMetrics.get(i)[1]);
		kernelList = new ArrayList<String>(new HashSet<String>(kernelList));
		numberOfKernel = kernelList.size();
		resultShowPanel.setLayout(new GridLayout(1, numberOfKernel));
		
		ArrayList<DefaultMutableTreeNode> kernelNodeList = new ArrayList<DefaultMutableTreeNode>(); 
		
		for (int i=0; i<numberOfKernel; i++) {
			DefaultMutableTreeNode tempKernelNode = new DefaultMutableTreeNode(kernelList.get(i));
			kernelNodeList.add(tempKernelNode);
		}
		
		for (int i=1; i<aryMetrics.size(); i++) {
			int kernelIndex = -1;
			
			for (int j=0; j<numberOfKernel; j++) {
				if (kernelList.get(j).equals(aryMetrics.get(i)[1])) {
					kernelIndex = j;
					break;
				}
			}
			
			if (aryMetrics.get(i)[4].contains("Utilization")/* || aryMetrics.get(i)[4].contains("Efficiency")*/) {
				String kernelString = aryMetrics.get(i)[4]; 
				String valueString = aryMetrics.get(i)[7];
				DefaultMutableTreeNode tempLeafNode = new DefaultMutableTreeNode(kernelString + ": " + valueString);
				
				if (valueString.equals("Low (1)") || valueString.equals("Idle (0)"))
					kernelNodeList.get(kernelIndex).add(tempLeafNode);
			}
		}
		
		for (int i=0; i<numberOfKernel; i++) {
			JTree tempTree = new JTree(kernelNodeList.get(i));

			CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer(syntaxedCodeArea);
			tempTree.setCellRenderer(renderer);
			tempTree.setCellEditor(new CheckBoxNodeEditor(tempTree, syntaxedCodeArea)); //process while click box
			tempTree.setEditable(true);
			
			//set the font of child of tree
			tempTree.setFont(new Font(Font.SERIF, Font.TRUETYPE_FONT, 12)); 
			resultShowPanel.add(tempTree);
		}
		
		//
		JPanel solShowPanel = new JPanel();
		solShowPanel.setLayout(new GridLayout(1, numberOfKernel));
		
		kernelNodeList = new ArrayList<DefaultMutableTreeNode>(); 
		for (int i=0; i<numberOfKernel; i++) {
			DefaultMutableTreeNode tempKernelNode = new DefaultMutableTreeNode(kernelList.get(i));
			kernelNodeList.add(tempKernelNode);
		}
		int kernelIndex =-1;;
		for (int i=1; i<aryMetrics.size(); i++) {
			kernelIndex = -1;
			
			for (int j=0; j<numberOfKernel; j++) {
				if (kernelList.get(j).equals(aryMetrics.get(i)[1])) {
					kernelIndex = j;
					break;
				}
			}
		}
		
		
		for(int j =0; j<line-1 ; j++)
		{
			String linedata= getline.get(j);
			if(linedata.contains("dimGrid")&&!linedata.contains("printf"))
			{
				DefaultMutableTreeNode tempLeafNode = new DefaultMutableTreeNode("Line "+ (getline.indexOf(linedata)+1) +" : " +linedata.replaceAll("^\\s+",""));
				kernelNodeList.get(kernelIndex).add(tempLeafNode);
			}	
			else if(linedata.contains("dimBlock")&&!linedata.contains("printf"))
			{
				DefaultMutableTreeNode tempLeafNode = new DefaultMutableTreeNode("Line "+ (getline.indexOf(linedata)+1) +" : " +linedata.replaceAll("^\\s+",""));
				kernelNodeList.get(kernelIndex).add(tempLeafNode);
			}
		}
		

		for (int i=0; i<numberOfKernel; i++) {
			JTree tempTree = new JTree(kernelNodeList.get(i));

			CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer(syntaxedCodeArea);
			tempTree.setCellRenderer(renderer);
			tempTree.setCellEditor(new CheckBoxNodeEditor(tempTree, syntaxedCodeArea)); //process while click box
			tempTree.setEditable(true);
			
			//set the font of child of tree
			tempTree.setFont(new Font(Font.SERIF, Font.TRUETYPE_FONT, 12)); 
			solShowPanel.add(tempTree);
		}
		

		
		tabbedShowPanel.add("Utilization of kernel", new JScrollPane(resultShowPanel));
		tabbedShowPanel.add("Solution of kernel", new JScrollPane(solShowPanel));
		resultPanel.add(tabbedShowPanel);
	}

}
