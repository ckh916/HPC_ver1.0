package escal.hpc.panels;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import javax.swing.text.Highlighter.HighlightPainter;

import escal.hpc.utilities.LineNumTextArea;
import escal.hpc.utilities.PTXPainter;
import escal.hpc.utilities.TextLineNumber;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
// for highlight
import javax.swing.text.*;

@SuppressWarnings("serial")
public class GPUsuggestCodePanel2 extends JPanel{

	private JFrame parentFrame;
	private JTextPane tPane;
	private JTextPane aftertPane;
	//private JPanel topPanel;
	//private JPanel aftertopPanel;
	private JPanel suggestcodePanel;
	int[][] CodeArray;
	int numOfFiles = 1;
	int numPTXLine = 1;
	
	String pathCode;

	JTextArea codeArea = new JTextArea("", 0, 0);
	LineNumTextArea codeNumArea = new LineNumTextArea(codeArea);
	JPanel codePanel = new JPanel();
	JScrollPane codeScrollPane = new JScrollPane(codeArea);
	
	JTextArea aftercodeArea = new JTextArea("", 0, 0);
	LineNumTextArea aftercodeNumArea = new LineNumTextArea(aftercodeArea);
	JPanel aftercodePanel = new JPanel();
	JScrollPane aftercodeScrollPane = new JScrollPane(aftercodeArea);
	
	//for suggest code analysis
	ArrayList<String[]> aryMetrics = new ArrayList<String[]>();
	
	public GPUsuggestCodePanel2(JFrame _parentFrame, String _pathCode, ArrayList<String[]> _aryMetrics) {

		parentFrame = _parentFrame;
		pathCode = _pathCode;
		aryMetrics = _aryMetrics;
		parentFrame.getContentPane().add(this);
		
		initGPUPanel();

		//CodeArray = new int[numOfFiles][numPTXLine];
		//PTXPainter ptxPainter = new PTXPainter(codeArea, aftercodeArea, CodeArray);

	}
	
	
	private void initGPUPanel() {

		this.setLayout(new BorderLayout());
		
		JLabel suggestLanel = new JLabel("Analysis Results");
		suggestLanel.setLayout(new BorderLayout());
		suggestLanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		suggestLanel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		this.add(suggestLanel, BorderLayout.PAGE_START);
		
		JPanel listkernelPanel = new JPanel();
		listkernelPanel.setLayout(new BorderLayout());	
		this.add(listkernelPanel, BorderLayout.CENTER);

		// add list of Analysis Result
		ArrayList<String> kernelList;
		JComboBox<String> kernelSelect;
		kernelList = new ArrayList<String>();
				
		// read type of GPU
		for (int i=1; i<aryMetrics.size(); i++) kernelList.add(aryMetrics.get(i)[1]);
			kernelSelect = new JComboBox<String>();
		for (int i=1; i<aryMetrics.size(); i++) {
			if (aryMetrics.get(i)[4].contains("Utilization")&&(aryMetrics.get(i)[7].equals("Low (1)") || aryMetrics.get(i)[7].equals("Idle (0)"))) { 	
					kernelSelect.addItem(aryMetrics.get(i)[4]+": "+aryMetrics.get(i)[7]);
			}
		}
		listkernelPanel.add(kernelSelect, BorderLayout.PAGE_START);
		
		suggestcodePanel = new JPanel();
		suggestcodePanel.setLayout(new GridLayout(1, 2));	
		listkernelPanel.add(suggestcodePanel, BorderLayout.CENTER);
		
		codePanel.setLayout(new BorderLayout());
		aftercodePanel.setLayout(new BorderLayout());
		
		//set label
		JLabel codeLabel = new JLabel("Original Code");
		codeLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		codeLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		codePanel.add(codeLabel, BorderLayout.PAGE_START);
		
		JLabel aftercodeLabel = new JLabel("Optimized Code");
		aftercodeLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		aftercodeLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		aftercodePanel.add(aftercodeLabel, BorderLayout.PAGE_START);
		
		tPane= new JTextPane();     
		aftertPane = new JTextPane();               

        setVisible(true);   
        
        codeScrollPane = new JScrollPane(tPane);
        TextLineNumber linenum = new TextLineNumber(tPane);
        codeScrollPane.setRowHeaderView( linenum );
        codePanel.add(codeScrollPane, BorderLayout.CENTER);
        
        aftercodeScrollPane = new JScrollPane(aftertPane);
        TextLineNumber afterlinenum = new TextLineNumber(aftertPane);
        aftercodeScrollPane.setRowHeaderView( afterlinenum );
        aftercodePanel.add(aftercodeScrollPane, BorderLayout.CENTER);
                
        suggestcodePanel.add(codePanel);
		suggestcodePanel.add(aftercodePanel);
		
		kernelSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				actionOpen("gaussian","gaussian_optimized");
			}
		});
	}

	private void appendToPane(JTextPane tp, String msg, Color c, Color bc)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.Background, bc);
        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
	 
	private void actionOpen(String name,String aftername) {
		String pathsuggestCode = pathCode +name+".cu";
		String pathsuggestafterCode = pathCode +aftername+".cu";
		
		String findword = "a_cuda[Size*(xidx+1+t)+(yidx+t)] -= m_cuda[Size*(xidx+1+t)+t] * a_cuda[Size*t+(yidx+t)];";

		int Minfind = 320, Maxfind = 333;
		FileReader fr = null;
		BufferedReader br = null;
		
		//reset the JtextPanel
		tPane.setText(null); 
		aftertPane.setText(null);

        try {
			fr = new FileReader(pathsuggestCode);
			br = new BufferedReader(fr);
						
			String string = new String();
		
			do {
				string = br.readLine();
				
				if(string != null) {
					
					if(string.contains(findword)) appendToPane(tPane, string + "\n", Color.WHITE, Color.RED);
					else appendToPane(tPane, string + "\n", Color.BLACK, Color.WHITE);
					tPane.setBackground(Color.BLUE);
				}
			} while(string != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
        
		try {
			fr = new FileReader(pathsuggestafterCode);
			br = new BufferedReader(fr);

			String string = new String();
			int n = 0;
			do {
				string = br.readLine();
				
				if(string != null) {		
					n += 1;	
					if(n > Minfind && n < Maxfind) appendToPane(aftertPane, string + "\n", Color.WHITE, Color.RED);
					else appendToPane(aftertPane, string + "\n", Color.BLACK, Color.WHITE);
					numPTXLine += 1;	
				}
			} while(string != null);
			
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}

	}
	
}
