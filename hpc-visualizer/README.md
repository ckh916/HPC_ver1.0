A visual-based analyzer for HPC (High Performance Computing) system.

Open source project of 'Ministry of Science, ICT and Future Planning', Korea


SW Developing Environment:

CPU: Intel Xeon E5-2680V3 (2.5GHz)

GPU: NVIDIA GeForce GTX 1080

Memory: 128GB

OS: Ubuntu 16.04.1 LTS

Compiler: GCC/G++ v5.4.0, OpenJDK v1.8.0_91 (OpenJDK v1.7 or above required)

Intel' PCM: Version 2.0

NVIDIA' NVPROF: CUDA Version 8.0.44


Initial Contributors:

Minsik Kim (minsik.kim@yonsei.ac.kr)

Yoonsoo Kim (yoonsoo.kim@yonsei.ac.kr)

Won Jeon (jeonwon@yonsei.ac.kr)
