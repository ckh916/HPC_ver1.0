import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

public class CheckBoxNode {
	
	String text;
	boolean selected;
	
	public CheckBoxNode(String text, boolean selected) {
		this.text = text;
		this.selected = selected;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelcted(boolean newValue) {
		selected = newValue;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String newValue) {
		text = newValue;
	}
	
	public String toString() {
		return getClass().getName() + "[" + text + "/" + selected + "]";
	}
	/*
	public NamedVector accNamedVector(String name) {
		NamedVector ret = new NamedVector(name);
		
		return ret;
	}
	
	public NamedVector accNamedVector(String name, Object elements[]) {
		NamedVector ret = new NamedVector(name, elements);
		
		return ret;
	}
	*/

}

@SuppressWarnings({ "serial", "rawtypes" })
class NamedVector extends Vector {
	
	String name;
	
	public NamedVector(String name) {
		this.name = name;
	}
	
	@SuppressWarnings("unchecked")
	public NamedVector(String name, Object elements[]) {
		this.name = name;
		for (int i=0, n=elements.length; i<n; i++) {
			add(elements[i]);
		}
	}
	
	public String toString() {
		return "[" + name + "]";
	}
}

class CheckBoxNodeRenderer implements TreeCellRenderer {
	
	private JCheckBox nonLeafRenderer = new JCheckBox();
	
	private DefaultTreeCellRenderer leafRenderer = new DefaultTreeCellRenderer();

	Color selectionBorderColor, selectionForeground, selectionBackground,
		textForeground, textBackground;

	protected JCheckBox getLeafRenderer() {
		return nonLeafRenderer;
	}

	public CheckBoxNodeRenderer() {
		Font fontValue;
		fontValue = UIManager.getFont("Tree.font");
		if (fontValue != null) {
			nonLeafRenderer.setFont(fontValue);
		}
		Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");
		nonLeafRenderer.setFocusPainted((booleanValue != null)&& (booleanValue.booleanValue()));

		selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
		selectionForeground = UIManager.getColor("Tree.selectionForeground");
		selectionBackground = UIManager.getColor("Tree.selectionBackground");
		textForeground = UIManager.getColor("Tree.textForeground");
		textBackground = UIManager.getColor("Tree.textBackground");
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		
		Component returnValue;
		if (!leaf) {
			
			String stringValue = tree.convertValueToText(value, selected,
					expanded, leaf, row, false);
			nonLeafRenderer.setText(stringValue);
			nonLeafRenderer.setSelected(false);
			nonLeafRenderer.setEnabled(tree.isEnabled());

			if (selected) {
				nonLeafRenderer.setForeground(selectionForeground);
				nonLeafRenderer.setBackground(selectionBackground);
			} 
			else {
				nonLeafRenderer.setForeground(textForeground);
				nonLeafRenderer.setBackground(textBackground);
			}

			if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
				Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
				if (userObject instanceof CheckBoxNode) {
					CheckBoxNode node = (CheckBoxNode) userObject;
					nonLeafRenderer.setText(node.getText());
					nonLeafRenderer.setSelected(node.isSelected());
				}
			}
			returnValue = nonLeafRenderer;
		} 
		else {
			returnValue = leafRenderer.getTreeCellRendererComponent(tree,
					value, selected, expanded, leaf, row, hasFocus);
		}
	    return returnValue;
	  }
	}


@SuppressWarnings("serial")
class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {

	CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
	ChangeEvent changeEvent = null;

	JTree tree;

	public CheckBoxNodeEditor(JTree tree) {
		this.tree = tree;
	}

	public Object getCellEditorValue() {
		JCheckBox checkbox = renderer.getLeafRenderer();
		CheckBoxNode checkBoxNode = new CheckBoxNode(checkbox.getText(),checkbox.isSelected());
		return checkBoxNode;
	}

	public boolean isCellEditable(EventObject event) {
		boolean returnValue = false;
		if (event instanceof MouseEvent) {
			MouseEvent mouseEvent = (MouseEvent) event;
			TreePath path = tree.getPathForLocation(mouseEvent.getX(),mouseEvent.getY());
			if (path != null) {
				Object node = path.getLastPathComponent();
				if ((node != null) && (node instanceof DefaultMutableTreeNode)) {
					DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
					Object userObject = treeNode.getUserObject();
					returnValue = ((!treeNode.isLeaf()) && (!(userObject instanceof CheckBoxNode)));
				}
			}
		}
		return returnValue;
	}

	public Component getTreeCellEditorComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row) {

		Component editor = renderer.getTreeCellRendererComponent(tree, value,
				true, expanded, leaf, row, true);

		// editor always selected / focused
		ItemListener itemListener = new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
				if (stopCellEditing()) {
					fireEditingStopped();
				}
			}
		};
		if (editor instanceof JCheckBox) {
			((JCheckBox) editor).addItemListener(itemListener);
		}

		return editor;
	}
}
