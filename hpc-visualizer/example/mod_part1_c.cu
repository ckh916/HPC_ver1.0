#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <sys/time.h>
#include <cuda_runtime.h>

#define blksize 10

__global__ void test0(int *result, float n)
{
	// bidx = k, tidx = m
	int tidx, bidx;
	int i=0;
	float temp = 0;
	tidx = threadIdx.x;
	bidx = blockIdx.x;
	
	for(i=0; i<=tidx; i++)
		temp = temp + pow(bidx/n, i);

	result[bidx*blksize + tidx] = temp;

	if(result[bidx*blksize + tidx] - temp != 0)
		result[bidx*blksize + tidx]++;

	temp = 0;
}

__global__ void test1(int *result, float n)
{
	// bidx = k, tidx = m
	int tidx, bidx;
	int i=0;
	float temp = 0;
	tidx = threadIdx.x;
	bidx = blockIdx.x;

	for(i=0; i<=tidx; i++)
		temp = temp + pow(bidx/n, i);

	result[bidx*blksize + tidx] = temp;

	if(result[bidx*blksize + tidx] - temp != 0)
		result[bidx*blksize + tidx]++;

	temp = 0;
}
__global__ void test2(int *result, float n)
{
	// bidx = k, tidx = m
	int tidx, bidx;
	int i=0;
	float temp = 0;
	tidx = threadIdx.x;
	bidx = blockIdx.x;

	for(i=0; i<=tidx; i++)
		temp = temp + pow(bidx/n, i);

	result[bidx*blksize + tidx] = temp;

	if(result[bidx*blksize + tidx] - temp != 0)
		result[bidx*blksize + tidx]++;

	temp = 0;
}

__global__ void test3(int *result, float n)
{
	// bidx = k, tidx = m
	int tidx, bidx;
	int i=0;
	float temp = 0;
	tidx = threadIdx.x;
	bidx = blockIdx.x;

	for(i=0; i<=tidx; i++)
		temp = temp + pow(bidx/n, i);

	result[bidx*blksize + tidx] = temp;

	if(result[bidx*blksize + tidx] - temp != 0)
		result[bidx*blksize + tidx]++;

	temp = 0;
}


int main(int argc, char *argv[])
{
	// host variables
	int *host_arrZ;
	int X, Y;
	float n;
	unsigned long long sum = 0;
	int i=0, j=0, k=0;
	timeval start, end;
	double time;

	// device variables
	int *device_arrZ;
	X = atoi(argv[1]);
	Y = atoi(argv[2]);
	n = atof(argv[3]);

	printf("X=%d, Y=%d, n=%f\n", X, Y, n);

	host_arrZ = (int*)malloc(sizeof(int)*blksize*blksize);
	cudaMalloc((void**)&device_arrZ, sizeof(int)*blksize*blksize);
	
	gettimeofday(&start, NULL);
	test0<<<X+1, Y+1>>>(device_arrZ, n);
	test1<<<X+3, Y+7>>>(device_arrZ, n);
	test2<<<X+2, Y+5>>>(device_arrZ, n);
	test3<<<X+9, Y+4>>>(device_arrZ, n);

	test0<<<X+1, Y+1>>>(device_arrZ, n);
	test1<<<X+1, Y+1>>>(device_arrZ, n);
	test2<<<X+3, Y+4>>>(device_arrZ, n);
	test3<<<X+4, Y+5>>>(device_arrZ, n);

	cudaMemcpy(host_arrZ, device_arrZ, sizeof(int)*blksize*blksize, cudaMemcpyDeviceToHost);

	for(i=0; i<=X; i++)
	{
		for(j=0; j<=Y; j++)
		{	
			sum = sum + (unsigned long long)host_arrZ[i*blksize + j];
			for(k=0; k<=host_arrZ[i*blksize + j]; k++)
			{
				sum = sum + (unsigned long long)pow(k, i);
			}
		}
	}

	gettimeofday(&end, NULL);

	time = (double)(end.tv_sec)+(double)(end.tv_usec)/1000000.0-(double)(start.tv_sec)-(double)(start.tv_usec)/1000000.0;


	printf("Result : %llu\n", sum);
	printf("Time : %fs\n", time);
	free(host_arrZ);
	cudaFree(device_arrZ);
	
	return 0;
}
