#include <stdio.h>
#include <math.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>

#define TN 16

struct timevall
{
		long tv_sec;
		long tv_usec;
};

void *func(void* arg);
sem_t sem1;
int kk = 0, mm = 0;

int X, Y, Z;
int i, j, k, m;
int arrZ[100][100] = {0};

double n;
double zz=0;

unsigned long long sum=0;

int main()
{
	struct timevall start, end;
	double time;
	
	pthread_t tid[TN];
	sem_init(&sem1, 0, 1);
	
	printf("X : ");
	scanf("%d", &X);
	__fpurge(stdin);
	printf("Y : ");
	scanf("%d", &Y);
	__fpurge(stdin);
	printf("n(0<n<1) : ");
	scanf("%lf", &n);
	__fpurge(stdin);

	gettimeofday(&start, NULL);
	
	for(i=0; i<TN; i++)
	{
		pthread_create(&tid[i], NULL, func, NULL);
	}
	
	for(i=0; i<TN; i++)
	{
		pthread_join(tid[i], NULL);
	}
	
	for(k=0; k<=X; k++)
	{
		for(m=0; m<=Y; m++)
		{	
			for(j=0; j<=arrZ[k][m]; j++)
			{
				sum = sum + (unsigned long long)pow(j, k);
			}
		}
	}

	// Time measue and print result
	gettimeofday(&end, NULL);
	time = (double)(end.tv_sec)+(double)(end.tv_usec)/1000000.0-(double)(start.tv_sec)-(double)(start.tv_usec)/1000000.0;

	printf("Result : %llu\n", sum);
	printf("Time : %lfs\n", time);

	sem_destroy(&sem1);
	
	return 0;
}

void *func(void* arg)
{
	int a,b,c;
	double temp = 0;
	
	while(kk<=X)
	{
		sem_wait(&sem1);
		b = kk;
		c = mm;
		mm++;
		
		if(mm>Y)
		{
			mm=0;
			kk++;
		}
		sem_post(&sem1);
		
		//printf("this is %u thread\n", (unsigned int)pthread_self());
		for(a=0; a<=c; a++)
			temp = temp + pow(b/n, a);

		arrZ[b][c] = temp;
		if(arrZ[b][c] - temp != 0)
			arrZ[b][c]++;
		temp = 0;
	}	
}
