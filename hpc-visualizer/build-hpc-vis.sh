#!/bin/bash

mkdir bin

if [ -z "$1" ]
then
    echo "No argument supplied. (Release mode by default)"
    javac -g:none -d ./bin/ -cp lib/*:src src/escal/hpc/FrontendMain.java
    echo "Built in release mode."
else
    if [ "$1" == "--enable-debug" ]
    then
        javac -g -d ./bin/ -cp lib/*:src ./src/escal/hpc/FrontendMain.java
        echo "Built in debug mode."
    else
        echo "Unknown parameter: $1"
        echo "If you build the project in debug mode, type as follows:"
        echo "  $0 --enable-debug"
        javac -g:none -d ./bin/ -cp lib/*:src ./src/escal/hpc/FrontendMain.java
        echo "Built in release mode."
    fi
fi
