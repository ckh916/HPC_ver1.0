import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FileDialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.ZoomableChart;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.traces.painters.TracePainterVerticalBar;
import info.monitorenter.gui.chart.views.ChartPanel;


@SuppressWarnings("serial")
public class GPUMasterPanel extends JPanel{
	
	private JFrame parentFrame;
	
	JTextArea codeArea = new JTextArea("", 0, 0);
	JScrollPane codeScrollPane = new JScrollPane(codeArea);
	
	JPanel analyzerPanel = new JPanel();
	JTabbedPane tabbedGraphPane = new JTabbedPane();
	
	ZoomableChart summaryChart = new ZoomableChart();
	ZoomableChart traceChart = new ZoomableChart();
	ZoomableChart eventsChart = new ZoomableChart();
	ZoomableChart metricsChart = new ZoomableChart();
	
	DefaultCategoryDataset tempdata = new DefaultCategoryDataset();
	
	JFreeChart tempchart = ChartFactory.createBarChart("bar", "name", "value", 
			tempdata, PlotOrientation.VERTICAL, true, true, false);
	
	final BarRenderer renderer = new BarRenderer();
	final CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator();
	final ItemLabelPosition p_center = new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER);
	final ItemLabelPosition p_below = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE6, TextAnchor.TOP_LEFT);
	
	ITrace2D summaryTrace = new Trace2DSimple();
	ITrace2D traceTrace = new Trace2DSimple();
	ITrace2D eventsTrace = new Trace2DSimple();
	ITrace2D metricsTrace = new Trace2DSimple();
	
	FileDialog load = new FileDialog(parentFrame, "Load source file");
	
	String pathRoot = GPUMasterPanel.class.getResource("").getPath();
	String pathNvcc = "/usr/local/cuda-8.0/bin/nvcc";
	String pathNvprof = "/usr/local/cuda-8.0/bin/nvprof";
	String pathCode = null;
	String pathBin = null;
	
	ArrayList<String[]> arySummary = new ArrayList<String[]>();
	ArrayList<String[]> arySummaryAPI = new ArrayList<String[]>();
	ArrayList<String[]> aryTrace = new ArrayList<String[]>();
	ArrayList<String[]> aryEvents = new ArrayList<String[]>();
	ArrayList<String[]> aryMetrics = new ArrayList<String[]>();
	
	String version = "Version v0.2.0 (20161017)";
	
	
	public GPUMasterPanel(JFrame _parentFrame) {
		
		parentFrame = _parentFrame;
		parentFrame.getContentPane().add(this);
		
		initGPUPanel();
	}
	
	private void initGPUPanel() {
		
		this.setLayout(new GridLayout(1, 2));
		
		/* Initializing graph area */
		analyzerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx=1.0;
		c.weighty=1.0;
		c.fill = GridBagConstraints.BOTH;

		JPanel graphPanel = new JPanel();
		JPanel resultPanel = new JPanel();
		
		tabbedGraphPane.addTab("Summary", null, new ChartPanel(summaryChart), "Graph for summary.csv");
		tabbedGraphPane.addTab("CPU/GPU Trace", null, new ChartPanel(traceChart), "Graph for trace.csv");
		//tabbedGraphPane.addTab("Events", null, new ChartPanel(eventsChart), "Graph for events.csv");
		tabbedGraphPane.addTab("Events", null, new org.jfree.chart.ChartPanel(tempchart), "Graph for events.csv");
		tabbedGraphPane.addTab("Metrics", null, new ChartPanel(metricsChart), "Graph for metrics.csv");

		graphPanel.setLayout(new BorderLayout());
		resultPanel.setLayout(new BorderLayout());
		
		JLabel graphLabel = new JLabel("Analysis Graph");
		JLabel resultLabel = new JLabel("Analysis Result");
		
		graphPanel.add(graphLabel, BorderLayout.PAGE_START);
		graphPanel.add(tabbedGraphPane, BorderLayout.CENTER);
		
		resultPanel.add(resultLabel, BorderLayout.PAGE_START);
		resultPanel.add(new JPanel(), BorderLayout.CENTER);
		
		c.gridx=0; c.gridy=0; c.gridwidth=1; c.gridheight=3;
		analyzerPanel.add(graphPanel, c);
		c.gridx=0; c.gridy=3; c.gridwidth=1; c.gridheight=1;
		analyzerPanel.add(resultPanel, c);
		
		GPUMasterPanel.this.codeArea.setEditable(false);
		
		this.add(codeScrollPane);
		this.add(analyzerPanel);
		
		parentFrame.add(this);
		addMenuBarToGPUFrame();
	}
	
	private void addMenuBarToGPUFrame() {
		
		JMenuBar menuBar = new JMenuBar();
		parentFrame.setJMenuBar(menuBar);
		
		/* File menu */
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		
		JMenuItem openItem = new JMenuItem("Open File");
		openItem.setMnemonic('O');
		openItem.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.ALT_MASK));
		
		/* File Opening Action */
		openItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionOpen();
			}
		});
		/* End of file opening */
		

		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.setMnemonic('E');
		exitItem.setAccelerator(KeyStroke.getKeyStroke('E', InputEvent.ALT_MASK));
		exitItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (JOptionPane.showConfirmDialog(parentFrame, "Are you sure?", "Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					destroy();					
				}
				else {
					// nothing
				}
			}
		});
		
		fileMenu.add(openItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		/* End of file menu */
		
		/* Run menu */
		JMenu runMenu = new JMenu("Run");
		runMenu.setMnemonic('R');
		
		JMenuItem compileItem = new JMenuItem("Compile");
		compileItem.setMnemonic('C');
		compileItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		JMenuItem executionItem = new JMenuItem("Execution");
		executionItem.setMnemonic('E');
		executionItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
		
		compileItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionComp();
			}
		});
		
		executionItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionExec();
			}
		});
		
		runMenu.add(compileItem);
		runMenu.addSeparator();
		runMenu.add(executionItem);
		
		/* End of run menu */
		
		/* Help menu */
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');
		
		/* Item declarations for Help Menu */
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.setMnemonic('A');
		aboutItem.setAccelerator(KeyStroke.getKeyStroke('A', InputEvent.ALT_MASK));
		aboutItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(parentFrame, 
						"HPC-Visualizer, GPU Analyzer\n"
						+ version
						+ "\n\nMinistry of Science, ICT and Future Planning, Korea\n"
						+ "Embedded Systems and Computer Architecture Lab.\n"
						+ "Yonsei University, Korea\n"
						+ "\nContact:\n"
						+ "Minsik Kim (minsik.kim@yonsei.ac.kr)\n"
						+ "Yoonsoo Kim (yoonsoo.kim@yonsei.ac.kr)\n"
						+ "Won Jeon (jeonwon@yonsei.ac.kr)", 
						"About", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		helpMenu.add(aboutItem);
		/* End of help menu */
		
		
		menuBar.add(fileMenu);
		menuBar.add(runMenu);
		menuBar.add(helpMenu);
		
	}
	
	private void actionOpen() {
		
		//FileDialog load = new FileDialog(parentFrame, "Load source file");
		load.setVisible(true);
		JTextArea codeTemp = new JTextArea("", 0, 0);
		codeTemp = GPUMasterPanel.this.codeArea;
		
		if(load.getFile() != null)
		{
			FileReader fr = null;
			BufferedReader br = null;
			
			codeTemp.setText(null);
			codeTemp.setTabSize(4);
			
			try {
				fr = new FileReader(load.getDirectory() + load.getFile());
				br = new BufferedReader(fr);
				
				String ext = "";
				int idx = load.getFile().lastIndexOf('.');
				if(idx > 0) {
					ext = load.getFile().substring(idx+1);
				}
				/*
				 String extension = "";
				 int i = fileName.lastIndexOf('.');
				 int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
				 if (i > p) {
				     extension = fileName.substring(i+1);
				     }
				 */
				if(!ext.equalsIgnoreCase("cu")) 
				{
					JOptionPane.showMessageDialog(parentFrame,
							load.getFile() + " is not a CUDA file.\n"
							+ "Only '.cu' file (CUDA extension) is supported.\n", 
							"Warning",
							JOptionPane.WARNING_MESSAGE);
					load.setFile(null);
				}
				else 
				{						
					String string = new String();
				
					do {
						string = br.readLine();
						
						if(string != null)
							codeTemp.append(string + "\n");
					} while(string != null);
				}
			}
			catch (Exception fileReadError) {
				System.out.println("Error while opening file" + fileReadError);
			}
			finally {
				try {
					br.close();
				}
				catch (Exception fileCloseError) {
					System.out.println("Error while closing file" + fileCloseError);
				}
			}
			
			parentFrame.setTitle("HPCVisualizer - GPU Analyzer - " + load.getFile());
			codeTemp.setCaretPosition(0);
			codeTemp.setTabSize(4);
		}		
	}
	
	private void actionComp() {
		
		if (load.getFile() == null)
			// TODO add code designation dialog when pathBin is null
			return;
		
		pathCode = load.getDirectory() + load.getFile();
		pathBin = pathRoot + "/" + load.getFile() + ".out";
		
		// TODO receive compile option from user
		 
		String[] cmd = {
				pathNvcc,
				pathCode,
				"--output-file",
				pathBin
		};
		try {
			JOptionPane.showMessageDialog(parentFrame, 
					"Compiling CUDA program...\n"
					+ "Press OK button to proceed.", 
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			
			JOptionPane.showMessageDialog(parentFrame, 
					"Compilation end.\n"
					+ "Output binary: " + pathBin,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Compilation end");
		} 
		catch (IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private void actionExec() {
		
		if (load.getFile() == null)
			// TODO add binary designation dialog when pathBin is null
			return;
		
		String[] cmd0 = {
				pathNvprof,
				"--csv",
				"--log-file",
				pathBin + "_summary.csv",
				pathBin
		};
		String[] cmd1 = {
				pathNvprof,
				"--print-gpu-trace",
				"--print-api-trace",
				"--csv",
				"--log-file",
				pathBin + "_trace.csv",
				pathBin
		};
		String[] cmd2 = {
				pathNvprof,
				"--events",
				"all",
				"--csv",
				"--log-file",
				pathBin + "_events.csv",
				pathBin
		};
		String[] cmd3 = {
				pathNvprof,
				"--metrics",
				"all",
				"--csv",
				"--log-file",
				pathBin + "_metrics.csv",
				pathBin
		};
		
		try {
			// TODO
			// nvprof --csv exe
			// nvprof --print-gpu-trace --print-api-trace exe
			// nvprof --events all exe
			// nvprof --metrics all exe
			// ...and more?
			
			JOptionPane.showMessageDialog(parentFrame, 
					"Executing NVPROF...\n"
					+ "This work might takes long time.\n"
					+ "Press OK button to proceed.", 
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			/* Serializing NVPROF execution */
			Process p = Runtime.getRuntime().exec(cmd0);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmd1);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmd2);
			p.waitFor();
			p = Runtime.getRuntime().exec(cmd3);
			p.waitFor();

			readCSV(pathBin+"_summary.csv");
			readCSV(pathBin+"_trace.csv");
			readCSV(pathBin+"_events.csv");
			readCSV(pathBin+"_metrics.csv");
			
			drawGraphs();
			
			JOptionPane.showMessageDialog(parentFrame, 
					"NVPROF execution and CSV file parsing end.\n"
					+ "Output CSV: " + pathRoot,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Execution end");
			
			/*
			//printArrayListString(arySummary);
			System.out.println("---------------------------");
			printArrayListString(aryEvents);
			System.out.println("---------------------------");
			printArrayListString(aryMetrics);
			System.out.println("---------------------------");
			printArrayListString(aryTrace);
			*/
		} 
		catch (IOException | InterruptedException el) {
			// TODO Auto-generated catch block
			el.printStackTrace();
		}
	}
	
	private void readCSV(String pathCSV) {
		// TODO
		FileReader fr = null;
		BufferedReader br = null;
		
		Pattern fileRex = null;
		Pattern csvRex0 = null;
		Pattern csvRex1 = null;
		Pattern csvRex2 = null;
		
		Matcher fileMat = null;
		Matcher csvMat0 = null;
		Matcher csvMat1 = null;
		Matcher csvMat2 = null;
		
		int aryidx0 = 0;
		int aryidx1 = 0;
		
		ArrayList<String[]> aryTemp = new ArrayList<String[]>();

		String csvType = "";
		int idx = pathCSV.lastIndexOf('_');
		if(idx > 0) {
			csvType = pathCSV.substring(idx+1);
		}		
		
		switch(csvType) {
		case "summary.csv"	: aryTemp = arySummary; break;
		case "trace.csv"	: aryTemp = aryTrace; break;
		case "events.csv"	: aryTemp = aryEvents; break;
		case "metrics.csv"	: aryTemp = aryMetrics; break;
		default				: System.out.println("Wrong csvType: " + csvType); break;	
		}
		
		try {
			fr = new FileReader(pathCSV);
			br = new BufferedReader(fr);
			fileRex = Pattern.compile("^=(\\W|\\w)*");
			csvRex0 = Pattern.compile("^\"(\\W|\\w)*\"$");
			csvRex1 = Pattern.compile("^\"(\\W|\\w)*[^\"]$");
			csvRex2 = Pattern.compile("^[^\"](\\W|\\w)*\"$");
			
			String str = new String();
			String[] tmp;

			do {
				str = br.readLine();
				if(str != null) {
					fileMat = fileRex.matcher(str);
					
					if (!fileMat.matches()) {
						
						tmp = str.split(",", -1);
						aryidx0 = 0;
						aryidx1 = 0; 
						
						for(int i=0; i<tmp.length; i++) {
							csvMat1 = csvRex1.matcher(tmp[i]);
							csvMat2 = csvRex2.matcher(tmp[i]);
							
							if(csvMat1.matches()) aryidx0 = i;
							if(csvMat2.matches()) aryidx1 = i;
						}
						
						if(aryidx0!=0 && aryidx1!=0) {
							for (int i=aryidx0+1; i<=aryidx1; i++)
								tmp[aryidx0] += tmp[i];
							
							for (int i=1; i<tmp.length-aryidx1; i++)
								tmp[aryidx0+i] = tmp[aryidx1+i];
						}
						/*
						else {
							// TODO enhance ux
							System.out.println("Wrong CSV format");
							System.exit(0);
						}*/
						
						aryTemp.add(tmp);
					}
				}
			} while(str != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
	}
	
	private void drawGraphs() {
		
		int barWidth = 8;
		
		summaryTrace.setTracePainter(new TracePainterVerticalBar(barWidth, summaryChart));
		traceTrace.setTracePainter(new TracePainterVerticalBar(barWidth, traceChart));
		eventsTrace.setTracePainter(new TracePainterVerticalBar(barWidth, eventsChart));
		metricsTrace.setTracePainter(new TracePainterVerticalBar(barWidth, metricsChart));
		
		summaryTrace.setColor(new Color(0, 0, 153));
		traceTrace.setColor(new Color(0, 0, 153));
		eventsTrace.setColor(new Color(0, 0, 153));
		metricsTrace.setColor(new Color(0, 0, 153));
		
		summaryTrace.setName("Trace for summary");
		traceTrace.setName("Trace for GPU/CPU trace");
		eventsTrace.setName("Trace for events");
		metricsTrace.setName("Trace for metrics");
		
		summaryChart.addTrace(summaryTrace);
		traceChart.addTrace(traceTrace);
		eventsChart.addTrace(eventsTrace);
		metricsChart.addTrace(metricsTrace);
		
		renderer.setBaseItemLabelGenerator(generator);
		renderer.setBaseItemLabelsVisible(true);
		renderer.setBasePositiveItemLabelPosition(p_center);
		renderer.setSeriesPaint(0,  new Color(0,162,255));
		
		tempdata.addValue(1.0, "S1", "Jan");
		tempdata.addValue(5.0, "S1", "Feb");
		tempdata.addValue(4.0, "S1", "Mar");
		tempdata.addValue(8.0, "S1", "Apr");
		tempdata.addValue(6.0, "S1", "May");
		tempdata.addValue(15.0, "S1", "Jun");
		tempdata.addValue(2.0, "S1", "Jul");
		tempdata.addValue(5.0, "S1", "Aug");
		
		
		Random random = new Random();
		for (int i = 0; i<30; i++) {
			summaryTrace.addPoint(i, random.nextDouble()*10.0+i);
			traceTrace.addPoint(i, random.nextDouble()*10.0+i);
			//eventsTrace.addPoint(i, random.nextDouble()*10.0+i);
			metricsTrace.addPoint(i, random.nextDouble()*10.0+i);
		}
		
	}
	
	public void printArrayListString(ArrayList<String[]> aryIn) {
		
		for(int i=0; i<aryIn.size(); i++) {
			for(int j=0; j<aryIn.get(0).length; j++) {
				System.out.print(aryIn.get(i)[j]+" ");
			}
			System.out.print("\n");
		}
	}
	
	public void destroy() {
		
		System.exit(0);
	}

}
