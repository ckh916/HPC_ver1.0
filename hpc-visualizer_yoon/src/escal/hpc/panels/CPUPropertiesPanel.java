package escal.hpc.panels;

import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CPUPropertiesPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1862111137546329167L;
	
	private JFrame parentFrame;
	private Properties projectProp;
	private String pathConfig;
	
	private JTextField gccTxt = null;
	private JTextField gprofTxt = null;

	public CPUPropertiesPanel(JFrame _parentFrame, Properties _projectProp, String _pathConfig) {
		
		parentFrame = _parentFrame;
		projectProp = _projectProp;
		pathConfig = _pathConfig;
		
		initPropertiesPanel();
	}
	
	private void initPropertiesPanel() {
		
		this.setLayout(null);
		
		JLabel gccLabel = new JLabel("GCC Path");
		JLabel gprofLabel = new JLabel("GPROF Path");
		
		gccTxt = new JTextField();
		gprofTxt = new JTextField();
		
		JButton nvccBrowseBtn = new JButton("Browse..");
		JButton nvprofBrowseBtn = new JButton("Browse..");
		
		JButton okBtn = new JButton("OK");
		JButton cancleBtn = new JButton("Cancel");
		
		/* Labels of configuration */
		gccTxt.setText(projectProp.getProperty("pathGcc"));
		gprofTxt.setText(projectProp.getProperty("pathGprof"));
		
		gccLabel.setBounds(10,10,100,25);
		gccLabel.setFont(new Font(Font.SERIF, Font.BOLD, 12));
		gprofLabel.setBounds(10,40,100,25);
		gprofLabel.setFont(new Font(Font.SERIF, Font.BOLD, 12));
		
		this.add(gccLabel);
		this.add(gprofLabel);
		
		gccTxt.setBounds(120,10,370,25);
		gccTxt.setEditable(false);
		gprofTxt.setBounds(120,40,370,25);
		gprofTxt.setEditable(false);
		
		this.add(gccTxt);
		this.add(gprofTxt);
		
		/* Buttons of configuration */
		nvccBrowseBtn.setBounds(495, 10, 100, 25);
		nvprofBrowseBtn.setBounds(495, 40, 100, 25);
		
		nvccBrowseBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionBrowse(gccTxt);
			}
		});
		
		nvprofBrowseBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionBrowse(gprofTxt);
			}
		});
		
		this.add(nvccBrowseBtn);
		this.add(nvprofBrowseBtn);
		
		/* OK and Cancel button */
		okBtn.setBounds(495, 220, 100, 25);
		cancleBtn.setBounds(390, 220, 100, 25);
		
		okBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionOK();
			}
		});
		
		cancleBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// do nothing
				parentFrame.dispose();
			}
		});
		
		this.add(okBtn);
		this.add(cancleBtn);
		
		/* Finally add to parent frame */
		parentFrame.add(this);
	}
	
	void actionOK() {
		
		if (JOptionPane.showConfirmDialog(parentFrame, "Are you sure?\nAll changes will be saved.", "Warning",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			projectProp.setProperty("pathNvcc", gccTxt.getText());
			projectProp.setProperty("pathNvprof", gprofTxt.getText());
			
			try {
				
				projectProp.store(new FileOutputStream(pathConfig), "ini file write done");
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			parentFrame.dispose();
		}
		else {
			// nothing
		}
	}
	
	void actionBrowse(JTextField _textField) {
		
		FileDialog fileLoad = new FileDialog(parentFrame, "Browse..", FileDialog.LOAD);
		
		fileLoad.setLocation(parentFrame.getLocation());
		fileLoad.setVisible(true);
		
		if (fileLoad.getFile() == null) {
			
			return;
		}
		
		_textField.setText(fileLoad.getDirectory() + fileLoad.getFile());
	}
}
