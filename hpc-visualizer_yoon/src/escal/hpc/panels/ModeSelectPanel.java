package escal.hpc.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import escal.hpc.utilities.Miscellaneous;

@SuppressWarnings("serial")
public class ModeSelectPanel extends JPanel {
	
	private JFrame cpuFrame;
	private CPUMasterPanel cpuPanel;
	
	private JFrame gpuFrame;
	@SuppressWarnings("unused")
	private GPUMasterPanel gpuPanel;
	
	Miscellaneous misc = new Miscellaneous();
	
	public ModeSelectPanel(final JFrame parentFrame) {
		
		JPanel masterPanel = new JPanel(new GridLayout(6, 1));
		masterPanel.setPreferredSize(new Dimension(600, 250));
		
		JLabel text1 = new JLabel("HPCVisualizer: Visual-based single node analyzer");
		text1.setHorizontalAlignment(JLabel.CENTER);
		text1.setFont(new Font("Serif", Font.BOLD, 20));
		
		JLabel text2 = new JLabel("   - Open source project of <Ministry of Science ICT>, Korea");
		JLabel text3 = new JLabel("   - CPU: Intel's PCM, ompP based analyzer");
		JLabel text4 = new JLabel("   - GPU: NVIDIA's NVPROF based analyzer");
		JLabel text5 = new JLabel("   - Memory: Intel's PCM, NVIDIA's NVPROF based analyzer");
		
		masterPanel.add(text1);
		masterPanel.add(text2);
		masterPanel.add(text3);
		masterPanel.add(text4);
		masterPanel.add(text5);
		
		JPanel rbPanel = new JPanel(new FlowLayout());
		((FlowLayout) rbPanel.getLayout()).setHgap(50);
		rbPanel.setPreferredSize(new Dimension(500, 30));
		
		JRadioButton[] rb = new JRadioButton[2];
		final ButtonGroup bg = new ButtonGroup();
		
		rb[0] = new JRadioButton("CUDA");
		rb[0].setFont(new Font("Arial", Font.BOLD, 15));
		rb[0].setHorizontalAlignment(SwingConstants.CENTER);
		rb[0].setActionCommand("gpu");
		rb[0].setSelected(true);
		
		rb[1] = new JRadioButton("OpenMP");
		rb[1].setFont(new Font("Arial", Font.BOLD, 15));
		rb[1].setHorizontalAlignment(SwingConstants.CENTER);
		rb[1].setActionCommand("cpu");
		rb[1].setSelected(true);
		
		bg.add(rb[0]);
		bg.add(rb[1]);
		rbPanel.add(rb[0]);
		rbPanel.add(rb[1]);		
		
		JButton nextBtn = new JButton("Start");
		nextBtn.setFont(new Font("Arial", Font.BOLD, 15));
		nextBtn.setPreferredSize(new Dimension(80,30));
		nextBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String cmd = bg.getSelection().getActionCommand();
				
				if(cmd.equals("cpu")) {
					showCPU();
				}
				else if(cmd.equals("gpu")) {
					showGPU();
				}
				else {
					return;
				}
				
				parentFrame.setVisible(false);
				//showGPU();
			}
		});
		
		rbPanel.add(nextBtn);
		masterPanel.add(rbPanel);
		
		setLayout(new BorderLayout());
		add(masterPanel, BorderLayout.CENTER);
	}
	
	private void showCPU() {
		
		cpuFrame = new JFrame("HPCVisualizer - CPU Analyzer");
		cpuFrame.setPreferredSize(new Dimension(1500, 800));
		cpuFrame.setSize(new Dimension(1500, 800));
		cpuFrame.setResizable(false);
		misc.alignWindow(cpuFrame);
		
		cpuPanel = new CPUMasterPanel(cpuFrame);
		cpuFrame.getContentPane().add(cpuPanel);
		
		cpuFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cpuFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				
				//cpuPanel.destroy();

			}
		});
		cpuFrame.pack();
		cpuFrame.setVisible(true);
	}
	
	private void showGPU() {
		
		gpuFrame = new JFrame("HPCVisualizer - GPU Analyzer");
		gpuFrame.setPreferredSize(new Dimension(1500, 800));
		gpuFrame.setSize(new Dimension(1500, 800));
		gpuFrame.setResizable(false);
		misc.alignWindow(gpuFrame);
		
		gpuPanel = new GPUMasterPanel(gpuFrame);
		
		gpuFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		gpuFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				
				misc.destroy();

			}
		});
		gpuFrame.pack();
		gpuFrame.setVisible(true);
	}
}
