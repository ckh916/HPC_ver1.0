package escal.hpc.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.SimpleTimePeriod;

import escal.hpc.utilities.Miscellaneous;
import jsyntaxpane.DefaultSyntaxKit;

public class CPUMasterPanel extends JPanel{

	private static final long serialVersionUID = -8219085059463328554L;
	
	private JFrame parentFrame;
	
	JTabbedPane tabbedCodePane = new JTabbedPane();

	JTextArea codeArea = new JTextArea("", 0, 0);
	JEditorPane syntaxedCodeArea = new JEditorPane();
	JScrollPane codeScrollPane = new JScrollPane(syntaxedCodeArea);

	JPanel analyzerPanel = new JPanel(); // Use JSplitPane to enhance UX
	JTabbedPane tabbedGraphPane = new JTabbedPane();
	JPanel resultShowPanel = new JPanel();
	JPanel MainPanel= new JPanel();
	JPanel resultPanel= new JPanel();
	JTabbedPane tabbedCPUPane = new JTabbedPane();
	
	FileDialog codeFileLoad = new FileDialog(parentFrame, "Load source file", FileDialog.LOAD);
	
	String pathRoot = System.getProperty("user.dir");
	String pathConfig = pathRoot + "/config.ini";
	String pathGcc = null;
	String pathGprof = null;
	String pathOmpp = null;
	String pathCodeDirectory = null;
	String pathCodeFile = null;
	String pathCode = null;
	String pathBin = null;
	String pathPTX = null;
	String pathExport = null;
	String pathLineInfo = null;
	String pathpcm = null;
	String pathpcmMemory = null;
	String projectName = null;
		
	ArrayList<String[]> arySummary = new ArrayList<String[]>();
	ArrayList<String[]> arySummaryAPI = new ArrayList<String[]>();
	ArrayList<String[]> aryTrace = new ArrayList<String[]>();
	ArrayList<String[]> aryEvents = new ArrayList<String[]>();
	ArrayList<String[]> aryMetrics = new ArrayList<String[]>();
	ArrayList<String[]> aryCPUperf = new ArrayList<String[]>();
	ArrayList<String[]> aryCPU = new ArrayList<String[]>();
	ArrayList<String[]> aryMemory = new ArrayList<String[]>();
	
	String version = "Version v1.0.0 (20161215)";
	
	int numberOfKernel = 0;
	int cores = Runtime.getRuntime().availableProcessors();
	
	long cmdExportElapsedTime, cmdEventsMetricsElapsedTime;
	long cmdAdditionalElapsedTime;
	
	Miscellaneous misc = new Miscellaneous();
	
	Properties projectProp = new Properties();
	
	public CPUMasterPanel(JFrame _parentFrame) {
		
		parentFrame = _parentFrame;
		parentFrame.getContentPane().add(this);
		
		initCPUPanel();
	}
	
	private void initCPUPanel() {
		
		// TODO enhance layout with JSplitPane
		this.setLayout(new GridLayout(1, 2));
		
		tabbedCodePane.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		/* Initializing graph area */
		analyzerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c0 = new GridBagConstraints();
		c0.weightx=1.0; c0.weighty=1.0;
		c0.fill = GridBagConstraints.BOTH;

		JPanel graphPanel = new JPanel();
		JPanel resultPanel = new JPanel();

		graphPanel.setLayout(new BorderLayout());
		resultPanel.setLayout(new BorderLayout());
				
		JLabel graphLabel = new JLabel("Graphs & Tables");
		JLabel resultLabel = new JLabel("Analysis Results: Low Utilized Resources");
		
		graphLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		graphLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		graphPanel.add(graphLabel, BorderLayout.PAGE_START);
		graphPanel.add(tabbedGraphPane, BorderLayout.CENTER);
		
		resultLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		resultLabel.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		resultPanel.add(resultLabel, BorderLayout.PAGE_START);
		resultPanel.add(new JScrollPane(resultShowPanel), BorderLayout.CENTER);
		
		c0.gridx=0; c0.gridy=0; c0.gridwidth=1; c0.gridheight=3;
		analyzerPanel.add(graphPanel, c0);
		c0.gridx=0; c0.gridy=3; c0.gridwidth=1; c0.gridheight=1;
		analyzerPanel.add(resultPanel, c0);
		
		DefaultSyntaxKit.initKit();
		
		try {
			projectProp.load(new FileInputStream(pathConfig));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pathGcc = projectProp.getProperty("pathGcc");
		pathGprof = projectProp.getProperty("pathGprof");
		pathOmpp = projectProp.getProperty("pathOmpp");
		
		// this.add(codeScrollPane);
		this.add(tabbedCodePane);
		this.add(analyzerPanel);
		
		parentFrame.add(this);
		addMenuBarToCPUFrame();
	}
	
	private void addMenuBarToCPUFrame() {
		
		JMenuBar menuBar = new JMenuBar();
		parentFrame.setJMenuBar(menuBar);
		
		/* File menu */
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		
		JMenuItem openItem = new JMenuItem("Open File");
		openItem.setMnemonic('O');
		openItem.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.ALT_MASK));
		openItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionOpen();
			}
		});
		/* End of file opening */
		
		JMenuItem cleanItem = new JMenuItem("Clean Project");
		cleanItem.setMnemonic('C');
		cleanItem.setAccelerator(KeyStroke.getKeyStroke('C', InputEvent.ALT_MASK));
		cleanItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (JOptionPane.showConfirmDialog(parentFrame, 
						"Are you sure?\n"
						+ "Every 'csv' file, 'ptx' file, 'bin' file, and analyze result will be deleted.\n"
						+ "This action cannot be undone.", 
						"Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					//actionClean();
				}
				else {
					// nothing
				}
				
			}
		});
		
		JMenuItem propItem = new JMenuItem("Properties");
		propItem.setMnemonic('P');
		propItem.setAccelerator(KeyStroke.getKeyStroke('P', InputEvent.ALT_MASK));
		propItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionProp();				
			}
		});
		

		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.setMnemonic('E');
		exitItem.setAccelerator(KeyStroke.getKeyStroke('E', InputEvent.ALT_MASK));
		exitItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (JOptionPane.showConfirmDialog(parentFrame, "Are you sure?", "Warning",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					misc.destroy();					
				}
				else {
					// nothing
				}
			}
		});
		
		fileMenu.add(openItem);
		fileMenu.addSeparator();
		fileMenu.add(cleanItem);
		fileMenu.addSeparator();
		fileMenu.add(propItem);
		fileMenu.addSeparator();
		fileMenu.add(exitItem);
		/* End of file menu */
		
		/* Run menu */
		JMenu runMenu = new JMenu("Run");
		runMenu.setMnemonic('R');
		
		JMenuItem compileItem = new JMenuItem("Compile");
		compileItem.setMnemonic('C');
		compileItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		JMenuItem executionItem = new JMenuItem("Execution");
		executionItem.setMnemonic('E');
		executionItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
		
		compileItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionComp();
			}
		});
		
		executionItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				actionExec();
			}
		});
		
		runMenu.add(compileItem);
		runMenu.addSeparator();
		runMenu.add(executionItem);
		/* End of run menu */
		
		/* Help menu */
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');

		/* Tool menu */
		JMenu toolsMenu = new JMenu("Tools");
		toolsMenu.setMnemonic('T');
		
		JMenuItem ptxItem = new JMenuItem("Assembly code");
		ptxItem.setMnemonic('P');
		ptxItem.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.ALT_MASK));
		
		ptxItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//actionPTX();
			}
		});

		toolsMenu.add(ptxItem);
		
		JMenuItem optItem = new JMenuItem("Auto Vectorization");
		optItem.setMnemonic('O');
		optItem.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.ALT_MASK));
		
		optItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//actionPTX();
			}
		});

		toolsMenu.add(optItem);
		
		JMenuItem recItem = new JMenuItem("Optimization example");
		recItem.setMnemonic('R');
		recItem.setAccelerator(KeyStroke.getKeyStroke('R', InputEvent.ALT_MASK));
		
		recItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//actionPTX();
			}
		});

		toolsMenu.add(recItem);

		/* End of tool menu */
		
		/* Item declarations for Help Menu */
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.setMnemonic('A');
		aboutItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		aboutItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(parentFrame, 
						"HPC-Visualizer, OpenMP Analyzer\n"
						+ version
						+ "\n\nMinistry of Science, ICT and Future Planning, Korea\n"
						+ "Embedded Systems and Computer Architecture Lab.\n"
						+ "Yonsei University, Korea\n"
						+ "\nContact:\n"
						+ "Minsik Kim (minsik.kim@yonsei.ac.kr)\n"
						+ "Yoonsoo Kim (yoonsoo.kim@yonsei.ac.kr)\n"
						+ "Won Jeon (jeonwon@yonsei.ac.kr)", 
						"About", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		helpMenu.add(aboutItem);
		/* End of help menu */
		
		
		menuBar.add(fileMenu);
		menuBar.add(runMenu);
		menuBar.add(toolsMenu);
		menuBar.add(helpMenu);
		
	}
	
	private void actionOpen() {
		codeFileLoad.setLocation(parentFrame.getLocation());
		codeFileLoad.setVisible(true);
		
		CPUMasterPanel.this.codeArea.setEditable(false);
		syntaxedCodeArea.setEditable(false);
		//codeScrollPane.setRowHeaderView(codeLine);
		
		JTextArea codeTemp = new JTextArea("", 0, 0);
		codeTemp = CPUMasterPanel.this.codeArea;
		codeTemp.setFont(new Font(Font.MONOSPACED, Font.TRUETYPE_FONT, 12));
		
		//codeTemp.setLineWrap(true);
		
		pathCodeDirectory = codeFileLoad.getDirectory();
		pathCodeFile = codeFileLoad.getFile();
		
		if(pathCodeFile != null) {
			
			/* Get extension of selected file */
			String ext = "";
			int idx = pathCodeFile.lastIndexOf('.');
			if(idx > 0) {
				ext = pathCodeFile.substring(idx+1);
			}
		
			/* Only OpenMP file is accepted */
			if(!ext.equalsIgnoreCase("c")) 
			{
				JOptionPane.showMessageDialog(parentFrame,
						pathCodeFile + " is not a OpenMP file.\n"
						+ "Only '.c' and '.cpp' files  are supported.\n", 
						"Warning", JOptionPane.WARNING_MESSAGE);
				
				codeFileLoad.setFile(null);
				return;
			}
		}
		
		if(pathCodeFile != null) {
			
			FileReader fr = null;
			BufferedReader br = null;
			
			codeTemp.setText(null);
			codeTemp.setTabSize(4);
			
			syntaxedCodeArea.setText(null);
			syntaxedCodeArea.setContentType("text/cpp");
			
			String syntaxedTextAll = new String();
			
			try {
				fr = new FileReader(pathCodeDirectory + pathCodeFile);
				br = new BufferedReader(fr);
				/*
				String extension = "";
				int i = fileName.lastIndexOf('.');
				int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
				if (i > p) {
				    extension = fileName.substring(i+1);
				}
				*/
				String string = new String();
				
				do {
					string = br.readLine();
					
					if(string != null) {
						codeTemp.append(string + "\n");
						syntaxedTextAll += string + "\n";
					}
				} while(string != null);
			}
			catch (Exception fileReadError) {
				System.out.println("Error while opening file" + fileReadError);
			}
			finally {
				try {
					br.close();
				}
				catch (Exception fileCloseError) {
					System.out.println("Error while closing file" + fileCloseError);
				}
			}
			
			// parentFrame.setTitle("HPCVisualizer - OpenMP Analyzer - " + pathCodeFile);
			codeTemp.setCaretPosition(0);
			codeTemp.setTabSize(4);
			
			syntaxedCodeArea.setText(syntaxedTextAll);
			syntaxedCodeArea.setCaretPosition(0);
			
			//codeLine.updateLineNumbers();
			
			tabbedCodePane.addTab(pathCodeFile, codeScrollPane);
		}
	}
	
	private void actionComp() {
		
		String additionalCompOption = null;

		String[] aryAddCompOption = null;
		String[] cmdCompile = null;

		boolean noAddCompOption = false;
		
		if (pathCodeFile == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target OMPP source file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		// update gcc path
		pathGcc = projectProp.getProperty("pathGcc");
		pathOmpp = projectProp.getProperty("pathOmpp"); // /home/ckh916/ompp/bin/kinst-ompp
		
		if(projectName == null)
			projectName = JOptionPane.showInputDialog(parentFrame, "Set project name", "Project Name",
					JOptionPane.QUESTION_MESSAGE);
		
		if(projectName == null)
			return;
		
		parentFrame.setTitle("HPCVisualizer - OpenMP Analyzer - " + projectName);
		 
		additionalCompOption = JOptionPane.showInputDialog(parentFrame, 
				"Additional Compile Options.\n"
				+ "Press OK to proceed.", 
				"Compile", JOptionPane.QUESTION_MESSAGE);
				
		// return on cancel
		if(additionalCompOption == null)
			return;
		
		System.out.println("Project Name: " + projectName);
		
		pathCode = codeFileLoad.getDirectory() + codeFileLoad.getFile();
		pathBin = pathRoot + "/projects/" + projectName + "/" + codeFileLoad.getFile() + ".out";
		pathExport = pathRoot + "/projects/" + projectName + "/" + "gmon.out";

		aryAddCompOption = additionalCompOption.split("\\s+");
		
		if(aryAddCompOption.length==1 && aryAddCompOption[0].equals("")) {
			noAddCompOption = true;
		}
		
		String[] cmdMKDIR = {
				"mkdir",
				"projects",
				pathRoot + "/projects/" + projectName
		}; // /home/ckh916/ompp/bin/kinst-ompp, gcc, -fopenmp, /home/ckh916/HPC/hpc-visualizer_yoon/example_ompp/main.c, -o, /home/ckh916/HPC/hpc-visualizer_yoon/projects/ompp_3/main.c.out
		String[] cmdCompileOri = {
				pathOmpp,
				"gcc",
				"-fopenmp",
				pathCode,
				"-o",
				pathBin
				
		};
		
		if (!noAddCompOption) {
			// if there are additional compile options 
			cmdCompile = new String[cmdCompileOri.length + aryAddCompOption.length];
			System.arraycopy(cmdCompileOri, 0, cmdCompile, 0, cmdCompileOri.length);
			System.arraycopy(aryAddCompOption, 0, cmdCompile, cmdCompileOri.length, aryAddCompOption.length);
		}
		else {
			// if there are no additional compile options
			cmdCompile = cmdCompileOri;
		}	
		try {
			Process p = Runtime.getRuntime().exec(cmdMKDIR);
			p.waitFor();
			// need to fix
			//p = Runtime.getRuntime().exec(cmdCompile);
			//p.waitFor();
			System.out.println(Arrays.toString(cmdCompile));
			
			JOptionPane.showMessageDialog(parentFrame, 
					"Compilation end.\n"
					+ "Output binary: " + pathBin,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Compilation end");
		} 
		catch (IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private void actionExec() {
		
		String additionalExecArguments = null;
		
		String[] aryAddExecArguments = null;
		String[] cmdExport = null;

		boolean noAddExecOption = false;
		
		if (pathBin == null) {
			JOptionPane.showMessageDialog(parentFrame, "Target binary file is not specified", "Info", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		// update gprof path
		pathGprof = projectProp.getProperty("pathGprof");
		pathpcm = projectProp.getProperty("pathpcm");
		pathpcmMemory = projectProp.getProperty("pathpcmMemory");
		
		additionalExecArguments = JOptionPane.showInputDialog(parentFrame, 
				"Additional Execution Arguments.\n"
				+ "Press OK to proceed.", 
				"Execution", JOptionPane.QUESTION_MESSAGE);
				
		// return on cancel
		if(additionalExecArguments == null)
			return;
		
		aryAddExecArguments = additionalExecArguments.split("\\s+");
		
		if(aryAddExecArguments.length==1 && aryAddExecArguments[0].equals("")) {
			noAddExecOption = true;
		}
		
		String[] cmdExportOri = {
				pathBin
		};
		
		if (!noAddExecOption) {
			// if there are additional compile options 
			cmdExport = new String[cmdExportOri.length + aryAddExecArguments.length];
			System.arraycopy(cmdExportOri, 0, cmdExport, 0, cmdExportOri.length);	
			System.arraycopy(aryAddExecArguments, 0, cmdExport, cmdExportOri.length, aryAddExecArguments.length);
		}
		else {
			// if there are no additional compile options
			cmdExport = cmdExportOri;
		}
		
		try {
			// return on cancel
			if (JOptionPane.showConfirmDialog(parentFrame, 
					"Executing GPROF and draw graphs & tables...\n"
					+ "This work might takes a long time.\n"
					+ "Press OK button to proceed.", 
					"Info.", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE)
					== JOptionPane.CANCEL_OPTION) {
				return;
			}
			
			/* Serializing GPROF execution */
			long startTime = System.nanoTime();
			Process p = Runtime.getRuntime().exec(cmdExport);
			p.getErrorStream().close();
			p.getInputStream().close();
			p.getOutputStream().close();
			p.waitFor();
			long endTime = System.nanoTime();
			cmdExportElapsedTime = endTime-startTime;
			
			
			// only can work on sudo
			//actionExec_CPU(aryAddExecArguments,noAddExecOption);
			
			startTime = System.nanoTime();
			System.out.println("readCSV(pathRoot + \"/report.csv\");");
			
			readCSV(pathRoot + "/report.csv"); //read for ompp 
			System.out.println("readCSV(pathBin+\"_cpuperf.csv\");");
			readCSV(pathBin+"_cpuperf.csv"); // for CPU
			System.out.println("readCSV(pathBin+\"_memory.csv\"); ");
			readCSV(pathBin+"_memory.csv"); // for CPU
			
			tabbedGraphPane.removeAll();

			drawGraphs();		
			drawTable();
			drawGraphs_CPU();
			drawTable_CPU();
			
			JOptionPane.showMessageDialog(parentFrame, 
					"GPROF execution and CSV file parsing end.\n"
					+ "Output CSV: " + pathRoot,
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
			System.out.println("Execution end");
			
			endTime = System.nanoTime();
			cmdAdditionalElapsedTime = endTime - startTime;
			
			System.out.println("Export Time: " + cmdExportElapsedTime + " ns");
		}
		catch (IOException | InterruptedException el) {
			// TODO Auto-generated catch block
			el.printStackTrace();
		}
	}
		
	private void actionProp() {
		
		//CPUPropertiesPanel cpuProperPanel = null;
		JFrame cpuProperFrame = new JFrame("Properties");
		cpuProperFrame.setPreferredSize(new Dimension(600, 250));
		cpuProperFrame.setSize(new Dimension(600, 250));
		cpuProperFrame.setResizable(false);
		cpuProperFrame.setLocation(parentFrame.getLocation());
		
		//cpuProperPanel = new CPUPropertiesPanel(cpuProperFrame, projectProp);
		// TODO
		new CPUPropertiesPanel(cpuProperFrame, projectProp, pathConfig);
		
		cpuProperFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cpuProperFrame.pack();
		cpuProperFrame.setVisible(true);
	}
	
	private void readCSV(String pathCSV) {
		
		FileReader fr = null;
		BufferedReader br = null;
		
		Pattern fileRex = null;
		Pattern csvRex0 = null;
		Pattern csvRex1 = null;
		Pattern csvRex2 = null;
		
		Matcher fileMat = null;
		Matcher csvMat0 = null;
		Matcher csvMat1 = null;
		Matcher csvMat2 = null;
		
		int aryidx0 = 0;
		int aryidx1 = 0;
		
		ArrayList<String[]> aryTemp = new ArrayList<String[]>();
		
		String csvType = "";
		int idx = pathCSV.lastIndexOf('_');
		if(idx > 0) {
			csvType = pathCSV.substring(idx+1);
		}
		switch(csvType) {
		case "cpuperf.csv"			: aryTemp = aryCPU; break;
		case "memory.csv"			: aryTemp = aryMemory; break;
		//case "report.csv"			: aryTemp = arySummary; break;
		//default						: System.out.println("Wrong csvType: " + csvType); break;	
		default 					: aryTemp = arySummary; break;	
		}
		
		try {
			fr = new FileReader(pathCSV);
			br = new BufferedReader(fr);
			fileRex = Pattern.compile("^=(\\W|\\w)*");			// Start with '='
			csvRex0 = Pattern.compile("^\"(\\W|\\w)*\"$");		// Start with '"' end with '"'
			csvRex1 = Pattern.compile("^\"(\\W|\\w)*[^\"]$");	// Start with '"' end without '"'
			csvRex2 = Pattern.compile("^[^\"](\\W|\\w)*\"$");	// Start without '"' end with '"'
			
			String space = "";
			String str = new String();
			String[] tmp;
			String[] tmp3 = new String[cores];
			aryTemp.clear();
			
			int cnt_line=0;
			
			do {
				str = br.readLine();
				if(str != null) {
					fileMat = fileRex.matcher(str);
					
					if(str.isEmpty()) {
						continue;						
					}

					/* Exception for arySummary, arySummaryAPI */
					if(str.contains("##BEG header separator=") ) {
						arySummary.clear();
						aryTemp = arySummary;
						continue;
					}
					
					if(str.contains("##BEG region overview") ) {
						arySummaryAPI.clear();
						aryTemp = arySummaryAPI;
						continue;
					}

					if(str.contains("##BEG ompp callgraph") ) {
						aryTrace.clear();
						aryTemp = aryTrace;
						continue;
					}

					if(str.contains("##BEG flat region profiles") ) {
						aryEvents.clear();
						aryTemp = aryEvents;
						continue;
					}

					if(str.contains("##BEG callgraph region profiles") ) {
						aryMetrics.clear();
						aryTemp = aryMetrics;
						continue;
					}

					if(str.contains("##BEG")||str.contains("##END") ) {
						continue;
					}
					
					if (!fileMat.matches()) {
						/*          Read PCM csv         */
						if(csvType.equals("cpuperf.csv")) {
							tmp = str.split(";", -1);		 
							
							for(int i=0; i<tmp.length; i++) {								
								if(i!=0&&space.equals(tmp[i])) tmp[i] = tmp[i-1];
							}							
							aryTemp.add(tmp);							
						}		  
						else if(csvType.equals("memory.csv")) {
							tmp = str.split(";", -1);
							aryTemp.add(tmp);
						}
						else {
							tmp = str.split(",", -1);
							aryidx0 = 0;
							aryidx1 = 0; 
							
							for(int i=0; i<tmp.length; i++) {
								csvMat1 = csvRex1.matcher(tmp[i]);
								csvMat2 = csvRex2.matcher(tmp[i]);
								
								if(csvMat1.matches()) aryidx0 = i;
								if(csvMat2.matches()) aryidx1 = i;
							}
							
							if(aryidx0!=0 && aryidx1!=0) {
								for (int i=aryidx0+1; i<=aryidx1; i++)
									tmp[aryidx0] += tmp[i];
								
								for (int i=1; i<tmp.length-aryidx1; i++)
									tmp[aryidx0+i] = tmp[aryidx1+i];
							}
							
							for(int i=0; i<tmp.length; i++) {
								csvMat0 = csvRex0.matcher(tmp[i]);
								
								if(csvMat0.matches()) tmp[i] = tmp[i].replaceAll("\"", "");
							}

							aryTemp.add(tmp);							
						}
					}
				}
			} while(str != null);
		}
		catch (Exception fileReadError) {
			System.out.println("Error while opening file" + fileReadError);
		}
		finally {
			try {
				br.close();
			}
			catch (Exception fileCloseError) {
				System.out.println("Error while closing file" + fileCloseError);
			}
		}
	}
	
	private void drawGraphs() {
		
		JPanel summaryPanel = new JPanel();
		
		TaskSeriesCollection traceCollection = new TaskSeriesCollection();
		TaskSeries traceTS = new TaskSeries("Duration");

		DefaultPieDataset summaryPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryBarData = new DefaultCategoryDataset();
		
		DefaultPieDataset summaryAPIPieData = new DefaultPieDataset();
		DefaultCategoryDataset summaryAPIBarData = new DefaultCategoryDataset();
		
		JFreeChart summaryPieChart = null;
		JFreeChart summaryBarChart = null;
		JFreeChart summaryAPIPieChart = null;
		JFreeChart summaryAPIBarChart = null;
		JFreeChart traceChart = null;

		ArrayList<String> traceTaskList = new ArrayList<String>();
		
		ChartPanel summaryPieChartPanel = null;
		ChartPanel summaryBarChartPanel = null;
		ChartPanel summaryAPIPieChartPanel = null;
		ChartPanel summaryAPIBarChartPanel = null;
		ChartPanel traceChartPanel = null;
		
		ArrayList<String> execList = new ArrayList<String>();
		ArrayList<String> bodyTList = new ArrayList<String>();
		
		summaryPanel.setLayout(new GridLayout(1,2));

		summaryPieData.setValue("Start", Double.parseDouble(aryEvents.get(aryEvents.size()-1)[5]));
		summaryPieData.setValue("Execution", Double.parseDouble(aryEvents.get(aryEvents.size()-1)[3]));
		summaryPieData.setValue("Barrier", Double.parseDouble(aryEvents.get(aryEvents.size()-1)[4]));
		summaryPieData.setValue("Shutdown", Double.parseDouble(aryEvents.get(aryEvents.size()-1)[6]));
		
		//summaryBarData.addValue(Double.parseDouble(aryEvents.get(aryEvents.size()-1)[1]), "ExecT", aryEvents.get(aryEvents.size()-1)[0]);
		summaryBarData.addValue(Double.parseDouble(aryEvents.get(aryEvents.size()-1)[5]), "Start", "Overview");
		summaryBarData.addValue(Double.parseDouble(aryEvents.get(aryEvents.size()-1)[3]), "Execution", "Overview");
		summaryBarData.addValue(Double.parseDouble(aryEvents.get(aryEvents.size()-1)[4]), "Barrier", "Overview");
		summaryBarData.addValue(Double.parseDouble(aryEvents.get(aryEvents.size()-1)[6]), "Shutdown", "Overview");
		

		summaryPieChart = ChartFactory.createPieChart("Parallel Execution Ratio", summaryPieData,
				true, true, false);
		summaryBarChart = ChartFactory.createBarChart("Parallel Execution Time", "Category", "Time (us)", 
				summaryBarData, PlotOrientation.HORIZONTAL, true, true, false);
		
		summaryPieChart.setBackgroundPaint(Color.WHITE);
		summaryBarChart.setBackgroundPaint(Color.WHITE);
		
		summaryPieChart.setBorderVisible(true);
		summaryBarChart.setBorderVisible(true);

		Plot summaryPiePlot = summaryPieChart.getPlot();
		CategoryPlot summaryBarPlot = summaryBarChart.getCategoryPlot();
		
		summaryPiePlot.setBackgroundPaint(Color.WHITE);
		summaryBarPlot.setBackgroundPaint(Color.WHITE);
		
		summaryBarPlot.setRangeGridlinePaint(Color.GRAY);
		
		CategoryAxis summaryBarAxis = summaryBarPlot.getDomainAxis();
		
		summaryBarAxis.setLowerMargin(0.02);
		summaryBarAxis.setUpperMargin(0.02);
		summaryBarAxis.setCategoryMargin(0.1);
		
		BarRenderer summaryBarRenderer = (BarRenderer) summaryBarPlot.getRenderer();
		
		summaryBarRenderer.setBarPainter(new StandardBarPainter());
		
		summaryBarRenderer.setItemMargin(0.05);
		summaryBarRenderer.setDrawBarOutline(false);
		
		summaryPieChartPanel = new ChartPanel(summaryPieChart);
		summaryBarChartPanel = new ChartPanel(summaryBarChart);
		
		summaryPanel.add(summaryPieChartPanel);		
		summaryPanel.add(summaryBarChartPanel);
		
		tabbedGraphPane.addTab("Summary", null, summaryPanel, "Graph for summary.csv");
		
		
	}
	
	private void drawTable() {
		
		JPanel summaryTablePanel = new JPanel();
		JPanel summaryAPITablePanel = new JPanel();
		JPanel traceTablePanel = new JPanel();
		JPanel eventsTablePanel = new JPanel();
		
		JScrollPane summaryTableScroll = new JScrollPane(summaryTablePanel);
		JScrollPane summaryAPITableScroll = new JScrollPane(summaryAPITablePanel);
		JScrollPane traceTableScroll = new JScrollPane(traceTablePanel);
		JScrollPane eventsTableScroll = new JScrollPane(eventsTablePanel);
		JScrollPane eTableScroll = new JScrollPane(eventsTablePanel);
		JScrollPane fTableScroll = new JScrollPane(eventsTablePanel);
		
		DefaultTableModel summaryTM = new DefaultTableModel();
		DefaultTableModel summaryAPITM = new DefaultTableModel();
		DefaultTableModel traceTM = new DefaultTableModel();
		DefaultTableModel eventsTM = new DefaultTableModel();
		
		JTable summaryTable = new JTable(summaryTM);
		JTable summaryAPITable = new JTable(summaryAPITM);
		JTable traceTable = new JTable(traceTM);
		JTable eventsTable = new JTable(eventsTM);
		
		/* Set Column names */
		Vector<String> summaryColumn = new Vector<String>();
		Vector<String> summaryAPIColumn = new Vector<String>();
		Vector<String> traceColumn = new Vector<String>();
		Vector<String> eventsColumn = new Vector<String>();
		Vector<String> tempRow;
		
		summaryColumn.addElement("Parameter");
		summaryColumn.addElement("Value");
		
		for (int i=0; i<arySummaryAPI.get(0).length; i++) {
			summaryAPIColumn.addElement(arySummaryAPI.get(0)[i]);
		}
		for (int i=arySummaryAPI.get(0).length; i<6; i++) {
			summaryAPIColumn.addElement("");
		}
		
		for (int i=0; i<aryTrace.get(0).length; i++) {
			traceColumn.addElement(aryTrace.get(0)[i]);
		}
		for (int i=aryTrace.get(0).length; i<6; i++) {
			traceColumn.addElement("");
		}
		
		for (int i=0; i<aryEvents.get(1).length; i++) {
			eventsColumn.addElement(aryEvents.get(1)[i]);
		}
		for (int i=aryEvents.get(1).length; i<8; i++) {
			eventsColumn.addElement("");
		}
						
		summaryTM = new DefaultTableModel(null, summaryColumn);
		summaryTable.setModel(summaryTM);
		summaryTable.setEnabled(false);

		summaryAPITM = new DefaultTableModel(null, summaryAPIColumn);
		summaryAPITable.setModel(summaryAPITM);
		summaryAPITable.setEnabled(false);
		
		traceTM = new DefaultTableModel(null, traceColumn);
		traceTable.setModel(traceTM);
		traceTable.setEnabled(false);

		eventsTM = new DefaultTableModel(null, eventsColumn);
		eventsTable.setModel(eventsTM);
		eventsTable.setEnabled(false);
		
		/* Add data for each row */
		for (int i=0; i<4; i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummary.get(0).length; j++) {
				tempRow.addElement(arySummary.get(i)[j]);
			}
			summaryTM.addRow(tempRow);
		}

		for (int i=5; i<6; i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummary.get(0).length; j++) {
				tempRow.addElement(arySummary.get(i)[j]);
			}
			summaryTM.addRow(tempRow);
		}
		
		for (int i=1; i<arySummaryAPI.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<arySummaryAPI.get(1).length; j++) {
				tempRow.addElement(arySummaryAPI.get(i)[j]);
			}
			summaryAPITM.addRow(tempRow);
		}
		
		for (int i=1; i<aryTrace.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryTrace.get(1).length; j++) {
				tempRow.addElement(aryTrace.get(i)[j]);
			}
			traceTM.addRow(tempRow);
		}

		for (int i=2; i<aryEvents.size(); i++) {
			tempRow = new Vector<String>();
			
			for (int j=0; j<aryEvents.get(1).length; j++) {
				tempRow.addElement(aryEvents.get(i)[j]);
			}
			eventsTM.addRow(tempRow);
		}
		
		/* Add table to panel */
		summaryTablePanel.setLayout(new BorderLayout());
		summaryAPITablePanel.setLayout(new BorderLayout());
		traceTablePanel.setLayout(new BorderLayout());
		eventsTablePanel.setLayout(new BorderLayout());
		
		summaryTablePanel.add(summaryTable.getTableHeader(), BorderLayout.PAGE_START);
		summaryTablePanel.add(summaryTable, BorderLayout.CENTER);

		summaryAPITablePanel.add(summaryAPITable.getTableHeader(), BorderLayout.PAGE_START);
		summaryAPITablePanel.add(summaryAPITable, BorderLayout.CENTER);

		traceTablePanel.add(traceTable.getTableHeader(), BorderLayout.PAGE_START);
		traceTablePanel.add(traceTable, BorderLayout.CENTER);
		
		eventsTablePanel.add(eventsTable.getTableHeader(), BorderLayout.PAGE_START);
		eventsTablePanel.add(eventsTable, BorderLayout.CENTER);
		
		summaryTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		summaryAPITableScroll.getVerticalScrollBar().setUnitIncrement(16);
		traceTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		eventsTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		eTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		fTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		tabbedGraphPane.addTab("General Information", null, summaryTableScroll, "Table for general information");
		tabbedGraphPane.addTab("Parallel Region", null, summaryAPITableScroll, "Table for API calls");
		tabbedGraphPane.addTab("Call Graph", null, traceTableScroll, "Table for CPU/GPU Trace");
		tabbedGraphPane.addTab("Region Profile", null, eventsTableScroll, "Table for events");
		tabbedGraphPane.addTab("Overhead Aanysis", null, eTableScroll, "Table for events");
		tabbedGraphPane.addTab("Performance Properties", null, fTableScroll, "Table for events");
		
	}

	private void drawTable_CPU() {
		
		
		ArrayList<String> CoreList;
		ArrayList<String> SKTList;
		
		int core=0;
		String tmp = "Init";
		String init = "Core0 (Socket 0)";
		String skt0 = "SKT0";
		String system = "System";
		
		CoreList = new ArrayList<String>();
		SKTList = new ArrayList<String>();
		
		for(int i=0; i<aryCPU.get(0).length; i++) {
			
			if (tmp.equals(aryCPU.get(0)[i])) {
				continue;
			}
			
			else if (init.equals(aryCPU.get(0)[i]) | core!=0) {
				CoreList.add(aryCPU.get(0)[i]);
				tmp = aryCPU.get(0)[i];
				core++;
			}			
		}
		
		tmp = "";
		for(int i=0; i<aryMemory.get(0).length; i++) {
			
			if (tmp.equals(aryMemory.get(0)[i])) {
				continue;
			}
			
			else if (!system.equals(aryMemory.get(0)[i])) {
				SKTList.add(aryMemory.get(0)[i]);
				tmp = aryMemory.get(0)[i];
				System.out.println(tmp);
			}			
		}
		

		/* Initial define */
		JPanel summaryCPUTablePanel = new JPanel();
		JPanel summaryCPUAPITablePanel = new JPanel();
		JPanel traceCPUTablePanel = new JPanel();
		JPanel eventsCPUTablePanel = new JPanel();
		JPanel metricsCPUTablePanel = new JPanel();
		JPanel CPUCoreTablePanel = new JPanel();
		JPanel MemoryTablePanel = new JPanel();

		/* Define a scroll panel*/
		JScrollPane summaryCPUTableScroll = new JScrollPane(summaryCPUTablePanel);
		JScrollPane summaryCPUAPITableScroll = new JScrollPane(summaryCPUAPITablePanel);
		JScrollPane traceCPUTableScroll = new JScrollPane(traceCPUTablePanel);
		JScrollPane eventsCPUTableScroll = new JScrollPane(eventsCPUTablePanel);
		JScrollPane metricsCPUTableScroll = new JScrollPane(metricsCPUTablePanel);
		JScrollPane CPUCoreTableScroll = new JScrollPane(CPUCoreTablePanel);
		JScrollPane MemoryTableScroll = new JScrollPane(MemoryTablePanel);
		
		DefaultTableModel CoreTM = new DefaultTableModel();
		DefaultTableModel MemoryTM = new DefaultTableModel();
		
		JTable CoreTable = new JTable(CoreTM);
		JTable MemoryTable = new JTable(MemoryTM);
		
		/* Set Column names */
		Vector<String> coreColumn = new Vector<String>();
		Vector<String> memoryColumn = new Vector<String>();
		Vector<String> tempRow;
		
		
		coreColumn.addElement("Core");
		for (int i=0; i<aryCPU.get(0).length; i++) {
			if(init.equals(aryCPU.get(0)[i]))
			coreColumn.addElement(aryCPU.get(1)[i]);
		}
		
		memoryColumn.addElement("Node");
		memoryColumn.addElement("SKT");
		for (int i=0; i<aryMemory.get(0).length; i++) {
			if(skt0.equals(aryMemory.get(0)[i]))
			memoryColumn.addElement(aryMemory.get(1)[i]);
		}		
		
		CoreTM = new DefaultTableModel(null, coreColumn);
		CoreTable.setModel(CoreTM);
		CoreTable.setEnabled(false);	
		
		MemoryTM = new DefaultTableModel(null, memoryColumn);
		MemoryTable.setModel(MemoryTM);
		MemoryTable.setEnabled(false);	
		
	
		for (int i=0; i<CoreList.size(); i++) {
			tempRow = new Vector<String>();	
			tempRow.addElement(CoreList.get(i));
			
			for (int j=0; j<aryCPU.get(0).length; j++) {
				if(CoreList.get(i).equals(aryCPU.get(0)[j]))
					tempRow.addElement(aryCPU.get(2)[j]);
			}			
			CoreTM.addRow(tempRow);			
		}
		
		for (int i=0; i<SKTList.size(); i++) {
			tempRow = new Vector<String>();
			tempRow.addElement("Node 0");
			tempRow.addElement(SKTList.get(i));
			
			for (int j=0; j<aryMemory.get(0).length; j++) {
				if(SKTList.get(i).equals(aryMemory.get(0)[j]))
					tempRow.addElement(aryMemory.get(2)[j]);
			}			
			MemoryTM.addRow(tempRow);			
		}
		
		CPUCoreTablePanel.setLayout(new BorderLayout());
		MemoryTablePanel.setLayout(new BorderLayout());
		
		CPUCoreTablePanel.add(CoreTable.getTableHeader(), BorderLayout.PAGE_START);
		CPUCoreTablePanel.add(CoreTable, BorderLayout.CENTER);
		
		MemoryTablePanel.add(MemoryTable.getTableHeader(), BorderLayout.PAGE_START);
		MemoryTablePanel.add(MemoryTable, BorderLayout.CENTER);
						
		summaryCPUTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		summaryCPUAPITableScroll.getVerticalScrollBar().setUnitIncrement(16);
		traceCPUTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		eventsCPUTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		metricsCPUTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		CPUCoreTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		MemoryTableScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		tabbedGraphPane.addTab("Core Table", null, CPUCoreTableScroll, "Table of each cores");
		tabbedGraphPane.addTab("Memory Table", null, MemoryTableScroll, "Table of each memories");	
	}

	private void drawGraphs_CPU() {
		

		JPanel summaryCPUPanel = new JPanel();

		DefaultCategoryDataset summaryCPUBarData = new DefaultCategoryDataset();
		
		JFreeChart summaryCPUBarChart = null;
		
		ChartPanel summaryCPUBarChartPanel = null;
		
		summaryCPUPanel.setLayout(new GridLayout(1,1));
		//summaryCPUPanel.setLayout(new GridLayout(1,2));
		//summaryCPUAPIPanel.setLayout(new GridLayout(1,2));
		
		
		
		String[] temprow= new String[cores];
		
		String system = "System";
		String skt0 = "SKT0";
		String skt1 = "SKT1";
		String skt = "Socket0";
		String ipc="IPC";		
		
		int j = 0;
		
		for(int i=0; i<aryCPU.get(0).length; i++) {
			if (ipc.equals(aryCPU.get(1)[i])&!skt0.equals(aryCPU.get(0)[i])&!skt1.equals(aryCPU.get(0)[i])&!system.equals(aryCPU.get(0)[i])&!skt.equals(aryCPU.get(0)[i])) {
				//System.out.println(j);
				//System.out.println(aryCPU.get(0)[i]);
				temprow[j]=aryCPU.get(2)[i];
				j++;
			}			
		}
		
		aryCPUperf.add(temprow);
		/* Draw summary pie and bar chart for CPU */
		for (int i=0; i<cores; i++) {
			
			summaryCPUBarData.addValue(Double.parseDouble(aryCPUperf.get(0)[i]), "IPC", String.valueOf(i));
		}		
		
		/* Create charts */
		
		summaryCPUBarChart = ChartFactory.createBarChart("CPU Performance","Nth Core", "IPC", 
				summaryCPUBarData, PlotOrientation.VERTICAL, true, true, false);
		
		
		/* Configure charts */
		summaryCPUBarChart.setBackgroundPaint(Color.WHITE);
		summaryCPUBarChart.setBorderVisible(true);
		
		summaryCPUBarChart.removeLegend();
		
		CategoryPlot summaryCPUBarPlot = summaryCPUBarChart.getCategoryPlot();
		
		summaryCPUBarPlot.setBackgroundPaint(Color.WHITE);
		
		summaryCPUBarPlot.setRangeGridlinePaint(Color.GRAY);
		
		Font font = new Font("Dialog", Font.PLAIN, 8);
		
		CategoryAxis summaryCPUBarAxis = summaryCPUBarPlot.getDomainAxis();
		
		summaryCPUBarAxis.setLowerMargin(0.02);
		summaryCPUBarAxis.setUpperMargin(0.02);
		summaryCPUBarAxis.setCategoryMargin(0.1);
		summaryCPUBarAxis.setTickLabelFont(font);
		
		BarRenderer summaryCPUBarRenderer = (BarRenderer) summaryCPUBarPlot.getRenderer();
		
		summaryCPUBarRenderer.setBarPainter(new StandardBarPainter());
		
		summaryCPUBarRenderer.setItemMargin(0.05);
		summaryCPUBarRenderer.setDrawBarOutline(false);
		
		NumberAxis traceAxis = new NumberAxis("Time (ns)");
		traceAxis.setAutoRangeIncludesZero(false);
		
		/* Configure end */
		
		summaryCPUBarChartPanel = new ChartPanel(summaryCPUBarChart);
		
		//summaryCPUBarChartPanel.getChart().removeLegend();
		
		summaryCPUPanel.add(summaryCPUBarChartPanel);
		
		tabbedGraphPane.addTab("CPU Performance", null, summaryCPUPanel, "Graph for CPU summary.csv");
	}

}
