package escal.hpc.utilities;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.ArrayList;

public class Miscellaneous {
	
	public void printArrayListString(ArrayList<String[]> aryIn) {
		
		System.out.println("---------------------------");
		
		for(int i=0; i<aryIn.size(); i++) {
			for(int j=0; j<aryIn.get(0).length; j++) {
				if(aryIn.get(i).length != 0)
					System.out.print(aryIn.get(i)[j]+" ");
			}
			System.out.print("\n");
		}
		
		System.out.println("---------------------------");
	}

	public void alignWindow(Window frame) {		
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - frame.getWidth()) / 3);
	    int y = (int) ((dimension.getHeight() - frame.getHeight()) / 3);
	    frame.setLocation(x, y);
	}

	public void destroy() {

		System.exit(0);
	}
}
