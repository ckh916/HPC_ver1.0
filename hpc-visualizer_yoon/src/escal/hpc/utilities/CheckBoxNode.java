package escal.hpc.utilities;

public class CheckBoxNode {
	
	String text;
	boolean selected;
	
	public CheckBoxNode(String text, boolean selected) {
		this.text = text;
		this.selected = selected;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelcted(boolean newValue) {
		selected = newValue;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String newValue) {
		text = newValue;
	}
	
	public String toString() {
		return getClass().getName() + "[" + text + "/" + selected + "]";
	}
	/*
	public NamedVector accNamedVector(String name) {
		NamedVector ret = new NamedVector(name);
		
		return ret;
	}
	
	public NamedVector accNamedVector(String name, Object elements[]) {
		NamedVector ret = new NamedVector(name, elements);
		
		return ret;
	}
	*/

}
/*
@SuppressWarnings({ "serial", "rawtypes" })
class NamedVector extends Vector {
	
	String name;
	
	public NamedVector(String name) {
		this.name = name;
	}
	
	@SuppressWarnings("unchecked")
	public NamedVector(String name, Object elements[]) {
		this.name = name;
		for (int i=0, n=elements.length; i<n; i++) {
			add(elements[i]);
		}
	}
	
	public String toString() {
		return "[" + name + "]";
	}
}
*/