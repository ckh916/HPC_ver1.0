#ifdef _POMP
#  undef _POMP
#endif
#define _POMP 200110

#include "/home/ckh916/HPC/HPC_ver1.0/hpc-visualizer_yoon/example_ompp/main.c.opari.inc"
#line 1 "/home/ckh916/HPC/HPC_ver1.0/hpc-visualizer_yoon/example_ompp/main.c"

void subdomain(float *x, int istart, int ipoints) {

	int i;

	for (i = 0; i < ipoints; i++)
		x[istart+i] = 123.456;
}

void sub(float *x, int npoints)
{
	int iam, nt, ipoints, istart;

POMP_Parallel_fork(&omp_rd_21);
#line 15 "/home/ckh916/HPC/HPC_ver1.0/hpc-visualizer_yoon/example_ompp/main.c"
#pragma omp parallel default(shared) private(iam,nt,ipoints,istart) POMP_DLIST_00021
{ POMP_Parallel_begin(&omp_rd_21);
#line 16 "/home/ckh916/HPC/HPC_ver1.0/hpc-visualizer_yoon/example_ompp/main.c"
{
	iam = omp_get_thread_num();
	nt = omp_get_num_threads();
	ipoints = npoints / nt; /* size of partition */
	istart = iam * ipoints; /* starting array index */
	if (iam == nt-1) /* last thread may do more */
		ipoints = npoints - istart;
		subdomain(x, istart, ipoints);
	}
POMP_Barrier_enter(&omp_rd_21);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_21);
POMP_Parallel_end(&omp_rd_21); }
POMP_Parallel_join(&omp_rd_21);
#line 25 "/home/ckh916/HPC/HPC_ver1.0/hpc-visualizer_yoon/example_ompp/main.c"
}

int main()
{
	float array[10000];

	sub(array, 10000);

	return 0;
}
