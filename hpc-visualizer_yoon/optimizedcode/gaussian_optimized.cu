__global__ void Fan2(float *m_cuda, float *a_cuda, float *b_cuda,int Size, int j1, int t)
{	
	const int xidx = blockIdx.x * blockDim.x + threadIdx.x;
	const int posX = xidx + t + 1;
	const int yidx = blockIdx.y * blockDim.y + threadIdx.y;
	const int posY = yidx + t;
	const int sizePosX = Size * posX;
	const int sizeT = Size * t;

	if(xidx >= Size-1-t) return;
	if(yidx >= Size-t) return;

	__shared__ float m_shared[BLOCK_SIZE_XY];
	__shared__ float a_shared[BLOCK_SIZE_XY];

	if(threadIdx.y == 0) {
		m_shared[threadIdx.x] = m_cuda[sizePosX+t];
	}
	__syncthreads();

	if(threadIdx.y == 0) {	
		a_shared[threadIdx.y] = a_cuda[sizeT+posY];
	}
	__syncthreads();
	
	a_cuda[sizePosX+posY] -= m_shared[threadIdx.x] * a_shared[threadIdx.y];
	if(yidx == 0){
		b_cuda[posX] -= m_shared[threadIdx.x] * b_cuda[t];
	}
}
