#include "pomp_lib.h"


extern struct ompregdescr omp_rd_13;
extern struct ompregdescr omp_rd_21;

int POMP_MAX_ID = 22;

struct ompregdescr* pomp_rd_table[22] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  &omp_rd_13,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  &omp_rd_21,
};
