1. insert .bashrc

export OMPP_OUTFORMAT=csv
export OMPP_REPORTFILE=report.csv

2. config.ini

pathpcmMemory=/home/ckh916/pcm/pcm-memory.x
pathNvcc=/usr/bin/gcc
pathpcm=/home/ckh916/pcm/pcm.x
pathNvprof=/usr/bin/gprof
pathGcc=/usr/bin/gcc
pathGprof=/usr/bin/gprof
pathOmpp=/home/ckh916/ompp/bin/kinst-ompp

3. install pcm

git clone https://github.com/opcm/pcm.git
make
